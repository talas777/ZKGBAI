#include "CombatReclaimTask.h"

#include "kgbutil/KgbUtil.h"

namespace zkgbai {

CombatReclaimTask::CombatReclaimTask(springai::Unit* unit) : WorkerTask(), target(unit)
{
    setpos(position,target->GetPos());
}

}
