#include "ConstructionTask.h"

#include "kgbutil/KgbUtil.h"

namespace zkgbai {

ConstructionTask::ConstructionTask(springai::UnitDef* def, const springai::AIFloat3& pos, int h) :
    WorkerTask(), buildType(def), facing(h)
{
    setpos(position,pos);
}

ConstructionTask::ConstructionTask(springai::UnitDef* def, const springai::AIFloat3& pos, int h, bool isPlop) :
    WorkerTask(), buildType(def), facing(h)
{
    setpos(position,pos);
    facPlop = isPlop;
}

ConstructionTask::~ConstructionTask(){}

}
