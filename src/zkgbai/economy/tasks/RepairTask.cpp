#include "RepairTask.h"

#include <algorithm>

#include "military/UnitClasses.h"

#include "kgbutil/KgbUtil.h"

namespace zkgbai {

RepairTask::RepairTask(springai::Unit* target) :
    WorkerTask(), target(target)
{
    std::string defName(target->GetDef()->GetName());

    UnitClasses* unitTypes = UnitClasses::getInstance();
    if(CONTAINSS(unitTypes->shieldMobs,defName) && defName != "striderfunnelweb"){
      isShieldMob = true;
    }
    if(defName == "tankassault" || defName == "tankriot" || defName == "tankheavyassault"){
      isTank = true;
    }
}

RepairTask::~RepairTask(){}

}
