#include "WorkerTask.h"

#include <algorithm>

namespace zkgbai {

WorkerTask::WorkerTask() : taskID(rootID){
    ++rootID;
}

WorkerTask::~WorkerTask(){
    for(Worker* w : assignedWorkers) w->endTask();
}

void WorkerTask::addWorker(Worker& w){
    auto it=std::find_if(assignedWorkers.begin(), assignedWorkers.end(),
                         [w](Worker*x){return*x==w;});
    if(it == assignedWorkers.end()) assignedWorkers.push_back(&w);
}

void WorkerTask::removeWorker(Worker& w){
    auto it=std::find_if(assignedWorkers.begin(), assignedWorkers.end(),
                         [w](Worker*x){return*x==w;});
    if(it != assignedWorkers.end()) (void)assignedWorkers.erase(it);
}

std::vector<Worker*> WorkerTask::stopWorkers(){
    for(Worker* w : assignedWorkers) w->endTask();
    std::vector<Worker*> ret(assignedWorkers);
    assignedWorkers.clear();
    return ret;
}

}
