#include "ReclaimTask.h"

#include "kgbutil/KgbUtil.h"

namespace zkgbai {

ReclaimTask::ReclaimTask(springai::Feature* feat, float metal) :
    WorkerTask(), target(feat), def(feat->GetDef()), metalValue(metal)
{
    setpos(position,feat->GetPosition());
}

}
