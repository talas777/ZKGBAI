#include "BuildIDs.h"

// generated by the C++ Wrapper scripts
#include "UnitDef.h"
#include "Resource.h"

namespace zkgbai {

BuildIDs::BuildIDs(springai::OOAICallback* callback) : 
    mexID(callback->GetUnitDefByName("staticmex")->GetUnitDefId()),
    solarID(callback->GetUnitDefByName("energysolar")->GetUnitDefId()),
    windID(callback->GetUnitDefByName("energywind")->GetUnitDefId()),
    nanoID(callback->GetUnitDefByName("staticcon")->GetUnitDefId()),
    storageID(callback->GetUnitDefByName("staticstorage")->GetUnitDefId()),
    airpadID(callback->GetUnitDefByName("staticrearm")->GetUnitDefId()),
    pylonID(callback->GetUnitDefByName("energypylon")->GetUnitDefId()),
    radarID(callback->GetUnitDefByName("staticradar")->GetUnitDefId())
{
    springai::Resource* m = callback->GetResourceByName("Metal");
    for(springai::UnitDef* ud : callback->GetUnitDefs()){
      std::string defName(ud->GetName());
      if(defName.find("factory") != std::string::npos || defName.find("hub") != std::string::npos){
        facIDs.insert(ud->GetUnitDefId());
      } else if(ud->GetSpeed()==0 && ud->GetCost(m)>200.f && ud->IsAbleToAttack() && std::string::npos == std::string(ud->GetTooltip()).find("Anti-Air")){
        expensiveIDs.insert(ud->GetUnitDefId());
      }
    }
}

}
