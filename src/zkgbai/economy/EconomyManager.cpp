#include "EconomyManager.h"

#include <algorithm>

#include "kgbutil/KgbUtil.h"
#include "kgbutil/HeightMap.h"
#include "kgbutil/TerrainAnalyzer.h"

#include "ZKGBAI.h"
#include "FactoryManager.h"

#include "graph/Link.h"
#include "military/Enemy.h"

namespace zkgbai {

#define ERASEUNIT(c,u){int id=u->GetUnitId();\
    auto it=std::find_if(c.begin(),c.end(),[id](auto*v){return v->GetUnitId()==id;});\
    if(it!=c.end()) (void)c.erase(it);}

EconomyManager::EconomyManager(ZKGBAI* ai){
    this->ai = ai;
    this->callback = ai->getCallback();
    this->eco = callback->GetEconomy();

    this->m = callback->GetResourceByName("Metal");
    this->e = callback->GetResourceByName("Energy");

    this->waterDamage = (callback->GetMap()->GetWaterDamage() > 0);

    this->bigMap = ai->mapDiag > 1270.0f;

    this->unitTypes = UnitClasses::getInstance();
    this->heightMap = new HeightMap(callback);
}

EconomyManager::~EconomyManager(){
    if(!initialized) return;
    delete terrainManager;
    delete buildIDs;
    for(auto * v : repairTasks){ v->assignedWorkers.clear(); delete v; }
    for(auto * v : reclaimTasks){ v->assignedWorkers.clear(); delete v; }
    for(auto * v : combatReclaimTasks){ v->assignedWorkers.clear(); delete v; }
    for(auto * v : constructionTasks){ v->assignedWorkers.clear(); delete v; }
    for(auto &[k,v] : dporcs) delete v;
    for(auto &[k,v] : workers) delete v;
    for(auto &[k,v] : nanos) delete v;
}

int EconomyManager::init(int AIID, springai::OOAICallback* cb){
    initialized = true;
    this->warManager = ai->warManager;
    this->graphManager = ai->graphManager;
    this->losManager = ai->losManager;
    this->facManager = ai->facManager;

    this->terrainManager = new TerrainAnalyzer(ai);
    this->potentialFacList = terrainManager->getInitialFacList();
    this->spidersViable = terrainManager->areSpidersViable();
    this->botsViable = terrainManager->areBotsViable();
    this->shipsViable = terrainManager->areShipsViable();
    this->buildIDs = new BuildIDs(callback);

    nukeWepID = callback->GetWeaponDefByName("staticnuke_crblmssl")->GetWeaponDefId();
    wolvMineID = callback->GetUnitDefByName("wolverine_mine")->GetUnitDefId();

    captureMexes();
    return 0;
}

int EconomyManager::message(int player, std::string message){
    if(ai->mergedAllies == 0 && message.find("kgbfac") == 0){
      if(message.find("veh") == 6){
        userFac = "factoryveh";
      } else if(message.find("hover") == 6){
        userFac = "factoryhover";
      } else if(message.find("tank") == 6){
        userFac = "factorytank";
      } else if(message.find("cloak") == 6){
        userFac = "factorycloak";
      } else if(message.find("shield") == 6){
        userFac = "factoryshield";
      } else if(message.find("amph") == 6){
        userFac = "factoryamph";
      } else if(message.find("spider") == 6){
        userFac = "factoryspider";
      } else {
        ai->say("Invalid factory name!");
        ai->say("Valid choices: veh, hover, tank, cloak, shield, amph, spider");
      }

      if(userFac != ""){
        ai->say("Set first factory: " + std::string(callback->GetUnitDefByName(userFac.c_str())->GetHumanName()));
      }
    } else if(ai->mergedAllies > 0 && message.find("kgbfac") == 0){
      ai->say("Teams mode, cannot set first factory!");
    }
    return 0;
}

int EconomyManager::update(int frame){
    this->frame = frame;

    if(frame % 300 == ai->offset % 300) checkNanos();

    if(frame % 15 == ai->offset % 15){
      rawMexIncome = 0;
      effectiveIncomeMetal = 2.f * (1 + ai->mergedAllies);
      effectiveIncomeEnergy = 2.f * (1 + ai->mergedAllies);
      maxStorage = std::max(0.5f, (500.0f * (commanders.size() + storages.size())));

      for(springai::Unit* ms : mexes){
        rawMexIncome += ms->GetRulesParamFloat("mexIncome", 0.0f);
        effectiveIncomeMetal += ms->GetRulesParamFloat("current_metalIncome", 0.0f);
      }
      for(Worker* w : commanders){
        effectiveIncomeMetal += w->getUnit()->GetRulesParamFloat("current_metalIncome", 0.0f);
        effectiveIncomeEnergy += w->getUnit()->GetRulesParamFloat("current_energyIncome", 0.0f);
      }
      for(springai::Unit* s : solars){
        effectiveIncomeEnergy += s->GetRulesParamFloat("current_energyIncome", 0.0f);
      }
      for (springai::Unit* w : windgens){
        effectiveIncomeEnergy += w->GetRulesParamFloat("current_energyIncome", 0.0f);
      }
      for(springai::Unit* f : fusions){
        effectiveIncomeEnergy += f->GetRulesParamFloat("current_energyIncome", 0.0f);
      }

      baseIncome = std::min(effectiveIncomeMetal, effectiveIncomeEnergy);

      metal = eco->GetCurrent(m);
      energy = eco->GetCurrent(e);

      float expendMetal = eco->GetUsage(m);
      float expendEnergy = eco->GetUsage(e);

      effectiveIncome = std::min(effectiveIncomeMetal, effectiveIncomeEnergy);
      effectiveExpenditure = std::min(expendMetal, expendEnergy);

      staticIncome = effectiveIncomeMetal;

      float currentReclaim = 0.f;
      for(auto &[k,w] : workers){
        currentReclaim += w->getUnit()->GetResourceMake(m);
      }
      reclaimRecord.push_back(currentReclaim);

      // Take a 5 second poll of reclaim income, add half to static income to turn reclaim into units.
      if(reclaimRecord.size() > 10){
        reclaimRecord.erase(reclaimRecord.begin());
        reclaimIncome = 0;
        for(float f : reclaimRecord){
          reclaimIncome += f;
        }
        reclaimIncome /= 10.f;

        if(staticIncome > 20.f || graphManager->eminentTerritory){
          staticIncome += reclaimIncome;
        } else {
          staticIncome += reclaimIncome/2.f;
        }
      }

      adjustedIncome = (staticIncome/(1 + (ai->mergedAllies))) + ((1.f + ai->mergedAllies) * (frame/1800.f));

      if(fusions.size() > 3){
        std::vector<float> params;
        if(!fusionTasks.empty()){
          params.push_back(1.f);
          for(springai::Unit* u : superWeps){
            u->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
          }
        } else {
          if(energy > 100.f){
            params.push_back(2.f);
          } else {
            params.push_back(1.f);
          }
          for(springai::Unit* u : superWeps){
            u->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
          }
        }
      }

      bool eFac = false; // note: cloaky uniquely requires running energy rich.
      if(ai->mergedAllies < 2){
        for(auto &[k,f] : facManager->factories){
          std::string defName(f->getUnit()->GetDef()->GetName());
          if(defName == "striderhub"){
            hasStriders = true;
          }
          if(defName == "factoryplane"){
            hasPlanes = true;
          }
          if(defName == "factorygunship"){
            hasGunship = true;
          }
          if(defName == "factorycloak"){
            eFac = true;
          }
        }
      }
      float eratio = 2.f + fusions.size();
      //float eratio = (eFac ? 1.5f : 1.f) + (graphManager.territoryFraction + (graphManager.territoryFraction * graphManager.territoryFraction)) / 2.f;
      hardEstalled = (maxStorage > 1.f && energy < std::min(effectiveIncomeEnergy * (1.f + graphManager->territoryFraction), maxStorage/2.f)) || staticIncome > effectiveIncomeEnergy;
      estalled = hardEstalled || rawMexIncome * eratio > effectiveIncomeEnergy;
      if(maxStorage > 1.f && hardEstalled && metal/maxStorage > 0.5f) energyReclaim = true;
      if(maxStorage > 1.f && energy/maxStorage > 0.75f) energyReclaim = false;

      setEcoPriorities();
    }


    if(frame % 30 == ai->offset % 30){
      captureMexes();
      defendLinks();
      defendMexes();

      // remove finished or invalidated tasks
      cleanOrders();

      for(springai::Unit* u : screamers){
        if(u->GetStockpileQueued() < 2){
          u->Stockpile(0);
        }
      }

      if(!warManager->enemyHasAntiNuke){
        for(springai::Unit* u : superWeps){
          if(std::string(u->GetDef()->GetName()) == "staticnuke" && !u->IsBeingBuilt() && u->GetStockpileQueued() < 2){
            u->Stockpile(0);
          }
        }
      }

      // if greedy workers make up too much of the workforce, convert them.
      for(auto &[k,w] : workers){
        if(workers.size() > greed && (greed <= 2 || greed <= workers.size() * std::min((1.f - (graphManager->territoryFraction * graphManager->territoryFraction)), 1.f))) break;
        if(w->isGreedy){
          w->isGreedy = false;
          greed--;
        }
      }

      if(effectiveIncome > 30.f && sparrow == nullptr && !hasPlanes) morphSparrow();

      /*if (!morphedComs && adjustedIncome > 20.f){
        morphComs();
      }*/
    }

    assignWorkers();
    return 0;
}

int EconomyManager::unitCreated(springai::Unit* unit,  springai::Unit* builder){
    springai::UnitDef* def = unit->GetDef();
    std::string defName(def->GetName());

    if(defName == "planescout"){
      sparrow = unit;
    }

    if(def->GetSpeed() > 0 && def->GetBuildOptions().size() > 0 && def->GetBuildSpeed() <= 8.0f){
      unit->SetMoveState(0, 0);
      newWorkers.insert(unit->GetUnitId());
    }

    // Track info for construction tasks
    if(builder != nullptr && unit->IsBeingBuilt()){
      if(def->GetSpeed() == 0){
        Worker* w = nullptr;
        if(workers.find(builder->GetUnitId()) != workers.end())
          w = workers[builder->GetUnitId()];
        if(w != nullptr && w->getTask() != nullptr && w->getTask()->type() == CONSTRUCTIONTASK && dynamic_cast<ConstructionTask*>(w->getTask())->buildType->GetName() == defName){
          ConstructionTask* ct = dynamic_cast<ConstructionTask*>(w->getTask());
          ct->target = unit;
        } else {
          for(ConstructionTask* ct : constructionTasks){
            float dist = distance(ct->getPos(), unit->GetPos());
            if(dist < 25 && std::string(ct->buildType->GetName()) == defName){
              ct->target = unit;
            }
          }
        }
      }
    } else if(builder != nullptr && !unit->IsBeingBuilt()){
      // instant factory plops only call unitcreated, not unitfinished.
      for(Worker* w : commanders){
        if(w->id == builder->GetUnitId()){
          WorkerTask* task = w->getTask();
          ERASEONE(constructionTasks, task);
          ERASEONE(factoryTasks,task);
          w->clearTask();
          delete task;
          break;
        }
      }
    }

    if(defName == "staticmex"){
      newMexes.push_back(unit);
    }

    if(defName == "energysolar" || defName == "energywind"){
      newEnergy.push_back(unit);
    }

    if((defName == "factoryplane" || defName == "striderhub" || defName == "factorygunship") && defendedFac){
      defendFusion(unit->GetPos(), true);
    }

    if(defName == "energypylon"){
      defendMex(unit->GetPos());
      pylons.push_back(unit);
    }

    if(defName == "turretmissile" || defName == "turretlaser" || defName == "turretheavylaser" || defName == "turretgauss" || defName == "turretemp" || defName == "turretriot" || defName == "turretaalaser"){
      porcs.push_back(unit);
      if(defName != "turretaalaser") dporcs[unit->GetUnitId()] = new Porc(ai, unit);
    }

    if(defName == "turretaalaser" || defName == "turretaaflak" || defName == "turretaafar"){
      AAs.push_back(unit);
    }

    if(defName == "staticrearm"){
      airpads.push_back(unit);
    }

    if(defName == "energyfusion" || defName == "energysingu" || defName == "staticheavyarty" || defName == "turretaaheavy" || defName == "turretaafar"){
      defendFusion(unit->GetPos(), false);
    }

    if(defName == "energyfusion" || defName == "energysingu"){
      fusions.push_back(unit);
      if(!screamer && fusions.size() > 4){
        MetalSpot* front = graphManager->getClosestFrontLineSpot(graphManager->getAllyCenter());
        if(front != nullptr){
          springai::UnitDef* artemis = callback->GetUnitDefByName("turretaaheavy");
          springai::AIFloat3 pos(front->getPos());
          setpos(pos,getDirectionalPoint(graphManager->getAllyCenter(), graphManager->getEnemyCenter(), 0.5f * distance(graphManager->getAllyCenter(), pos)));
          setpos(pos,callback->GetMap()->FindClosestBuildSite(artemis, pos, 600, 3, 0));
          ConstructionTask* ct = new ConstructionTask(artemis, pos, 0);
          if(buildCheck(*ct) && !CONTAINSP(constructionTasks, ct)){
            screamer = true;
            constructionTasks.push_back(ct);
          } else delete ct;
        }
      }
    }

    if(ai->mergedAllies > 1 && (defName == "striderdante" || defName == "striderscorpion") && warManager->miscHandler->striders.empty()){
      auto rti = std::find_if(repairTasks.begin(),repairTasks.end(),[unit](auto*r){return r->target->GetUnitId()==unit->GetUnitId();});
      if(rti == repairTasks.end()){
        repairTasks.push_back(new RepairTask(unit));
      }
    }

    if(defName == "striderfunnelweb" && facManager->numFunnels == 0){
      auto rti = std::find_if(repairTasks.begin(),repairTasks.end(),[unit](auto*r){return r->target->GetUnitId()==unit->GetUnitId();});
      if(rti == repairTasks.end()){
        repairTasks.push_back(new RepairTask(unit));
      }
    }

    if(defName == "factoryplane"){
      havePlanes = true;
    }

    if(defName == "turretaaheavy"){
      screamers.push_back(unit);
    }

    if(defName == "staticantinuke"){
      std::vector<float> params;
      params.push_back(2.f);
      unit->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
    }

    if(defName == "staticheavyarty" || defName == "staticnuke" || defName == "zenith" || defName == "raveparty"){
      superWeps.push_back(unit);
      if(defName == "staticnuke"){
        std::vector<float> params;
        params.push_back(2.f);
        unit->ExecuteCustomCommand(CMD_MISC_PRIORITY, params, 0);
      }
    }

    return 0;
}

int EconomyManager::unitFinished(springai::Unit* unit){
    springai::UnitDef* def = unit->GetDef();
    std::string defName(def->GetName());

    if(defName == "staticmex"){
      mexes.push_back(unit);
      ERASEUNIT(newMexes,unit);
      std::vector<float> params;
      params.push_back(1.f);
      unit->ExecuteCustomCommand(CMD_PRIORITY, params, 0);

      if(!defendedFac && ((int)mexes.size() > 1 + (ai->mergedAllies * 2) || graphManager->territoryFraction > 0.2f)){
        for(auto &[k,f] : facManager->factories){
          defendFac(f->getPos());
        }
        defendedFac = true;
      }
    }

    if(defName == "energysolar"){
      solars.push_back(unit);
    }

    if(defName == "energywind"){
      windgens.push_back(unit);
    }

    if(defName == "energysolar" || defName == "energywind"){
      ERASEUNIT(newEnergy,unit);
      std::vector<float> params;
      params.push_back(1.f);
      unit->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
    }

    if(defName == "staticradar"){
      radars.push_back(unit);
    }

    if(defName == "planelightscout"){
      sparrow = unit;
    }

    if(defName == "staticstorage"){
      storages.push_back(unit);
    }

    if(defName == "energysingu" || defName == "staticnuke" || defName == "zenith" || defName == "raveparty"){
      defendSingu(unit->GetPos());
      for(auto &[k,n] : nanos){
        if(n->target->GetUnitId() == unit->GetUnitId()){
          n->unit->SelfDestruct(0);
        }
      }
      std::vector<ConstructionTask*> unneeded;
      for(ConstructionTask* c : nanoTasks){
        if(c->ctTarget->GetUnitId() == unit->GetUnitId()){
          std::vector<Worker*> idle = c->stopWorkers();
          for(Worker* w : idle){
            idlers.push_back(w);
            ERASEONE(assigned, w);
          }
          unneeded.push_back(c);
        }
      }
      for(ConstructionTask* c : unneeded){
        ERASEONE(constructionTasks, c);
        ERASEONE(nanoTasks, c);
        delete c;
      } unneeded.clear();
    }

    if(defName == "staticnuke" || defName == "zenith" || defName == "raveparty"){
      hasSuperWep = true;
    }

    if(def->GetSpeed() > 0 && def->GetBuildOptions().size() > 0 && def->GetBuildSpeed() > 8){
      Worker* w = new Worker(ai, unit);
      w->isCom = true;
      workers[w->id] = w;
      newWorkers.erase(w->id);
      commanders.push_back(w);
      generateTasks(*w);
      assignWorker(*w);
      assigned.push_back(w);
      seeders.push_back(w);
      unit->SetMoveState(0, 0);
    }

    for(ConstructionTask* ct : constructionTasks){
      if(ct->target != nullptr && ct->target->GetUnitId() == unit->GetUnitId()){
        if(defName == "staticcon"){
          Nanotower* nt = new Nanotower(unit, ct->ctTarget);
          nanos[unit->GetUnitId()] = nt;
          unit->SetRepeat(true, 0);
          unit->Guard(nt->target, 0);
          defendNano(*nt);
        }

        dropConstructionTask(*ct);
        break;
      }
    }

    return 0;
}

int EconomyManager::unitDamaged(springai::Unit* h, springai::Unit* attacker, float damage, const springai::AIFloat3& dir, springai::WeaponDef* weaponDef, bool paralyzed){
    if(h->GetDef()->GetUnitDefId() == wolvMineID) return 0;

    // add repair tasks for damaged units
    if(h->GetUnitId() != lastRepairedUnit && h->GetHealth() > 0 && std::string(h->GetDef()->GetName()).find("bomber") == std::string::npos){
      lastRepairedUnit = h->GetUnitId();
      auto rti = std::find_if(repairTasks.begin(),repairTasks.end(),[h](auto*r){return r->target->GetUnitId()==h->GetUnitId();});
      if(rti == repairTasks.end()){
        repairTasks.push_back(new RepairTask(h));
      }
    }

    if(weaponDef != nullptr && weaponDef->GetWeaponDefId() == nukeWepID && (attacker == nullptr || attacker->GetAllyTeam() != ai->allyTeamID)){
      gotNuked = true;
    }

    // fortify mexes if they die
    if(h->GetDef()->GetUnitDefId() == buildIDs->mexID && h->GetHealth() <= 0 && !h->IsBeingBuilt()){
      if(graphManager->getClosestSpot(h->GetPos())->owned) {
        // only fortify mexes that had already been completed
        fortifyMex(h->GetPos(), weaponDef);
      }
    }

    // If it was a building under construction, reset the builder's target
    if(h->GetDef()->GetSpeed() == 0 && (h->GetHealth() <= 0.f || !h->GetDef()->IsAbleToAttack()) && weaponDef != nullptr && (attacker == nullptr || attacker->GetAllyTeam() != ai->allyTeamID) && CONTAINSI(unitTypes->porcWeps,weaponDef->GetWeaponDefId())){
      for(ConstructionTask* ct : constructionTasks){
        if(ct->target != nullptr && ct->target->GetUnitId() == h->GetUnitId()){
          // if a building was killed by enemy porc, cancel it.
          dropConstructionTask(*ct);
          break;
        }
      }
    }
    return 0;
}

int EconomyManager::unitDestroyed(springai::Unit* unit, springai::Unit* attacker) {
    if(unit->GetDef()->GetSpeed() == 0.f){
      ERASEUNIT(radars,unit);
      ERASEUNIT(porcs,unit);
      dporcs.erase(unit->GetUnitId());
      nanos.erase(unit->GetUnitId());
      ERASEUNIT(airpads,unit);
      ERASEUNIT(fusions,unit);
      ERASEUNIT(mexes,unit);
      ERASEUNIT(solars,unit);
      ERASEUNIT(windgens,unit);
      ERASEUNIT(pylons,unit);
      ERASEUNIT(AAs,unit);
      ERASEUNIT(superWeps,unit);
      ERASEUNIT(storages,unit);
      ERASEUNIT(screamers,unit);
      ERASEUNIT(newMexes,unit);
      ERASEUNIT(newEnergy,unit);

      for(ConstructionTask* ct : constructionTasks){
        if(ct->target != nullptr && ct->target->GetUnitId() == unit->GetUnitId()){
          // if a building was killed while under construction, reset its target.
          ct->target = nullptr;
          break;
        }
      }
    } else if(sparrow != nullptr && unit->GetUnitId() == sparrow->GetUnitId() && sparrow->GetRulesParamFloat("wasMorphedTo", 0.f) == 0){
      sparrow = nullptr;
    }

    std::string defName(unit->GetDef()->GetName());
    if(defName == "staticnuke" || defName == "zenith" || defName == "raveparty"){
      hasSuperWep = false;
    }

    // if the unit had a repair task targeting it, remove it
    auto rti = std::find_if(repairTasks.begin(),repairTasks.end(),[unit](auto*r){return r->target->GetUnitId()==unit->GetUnitId();});
    if(rti != repairTasks.end()){
      RepairTask* const r=*rti;
      std::vector<Worker*> idle = r->stopWorkers();
      for(Worker* w : idle){
        idlers.push_back(w);
        ERASEONE(assigned, w);
      }
      delete r; (void)repairTasks.erase(rti);
    }

    if(unit->GetDef()->GetSpeed() > 0 && unit->GetDef()->GetBuildSpeed() > 0){
      newWorkers.erase(unit->GetUnitId());
      auto it = workers.find(unit->GetUnitId());
      if(it != workers.end()){
        Worker* const w = (*it).second;
        w->clearTask();
        if(w->isGreedy) greed--;
        ERASEONE(commanders,w);
        ERASEONE(seeders,w);
        ERASEONE(idlers,w);
        ERASEONE(assigned,w);
        delete w;
        workers.erase(it);
      }
    }

    return 0; // signaling: OK
}

int EconomyManager::unitCaptured(springai::Unit* unit, int oldTeamID, int newTeamID){
    (void)newTeamID;
    if(oldTeamID == ai->teamID){
      return unitDestroyed(unit, nullptr);
    }
    return 0;
}

int EconomyManager::unitGiven(springai::Unit* unit, int oldTeamID, int newTeamID){
    if(newTeamID == ai->teamID){
      if(unit->GetDef()->GetSpeed() > 0 && unit->GetDef()->GetBuildOptions().size() > 0){
        Worker* w = new Worker(ai, unit);
        workers[w->id] = w;
        newWorkers.erase(w->id);
        generateTasks(*w);
        assignWorker(*w);
        assigned.push_back(w);
        seeders.push_back(w);
        if(unit->GetDef()->GetBuildSpeed() > 8){
          commanders.push_back(w);
        }
      }

      if(unit->IsBeingBuilt()){
        auto rti = std::find_if(repairTasks.begin(), repairTasks.end(), [unit](auto*r){ return r->target->GetUnitId() == unit->GetUnitId(); });
        if(rti == repairTasks.end()){
          repairTasks.push_back(new RepairTask(unit));
        }
      }

      std::string defName = unit->GetDef()->GetName();
      if(defName == "turretmissile" || defName == "turretlaser" || defName == "turretheavylaser" || defName == "turretemp" || defName == "turretgauss" || defName == "turretriot" || defName == "turretaalaser"){
        porcs.push_back(unit);
      }

      if(defName == "turretaalaser" || defName == "turretaafar" || defName == "turretaaflak"){
        AAs.push_back(unit);
      }

      if(defName == "staticcon"){
        unit->SelfDestruct(0);
      }

      if(defName == "staticmex"){
        mexes.push_back(unit);
      }

      if(defName == "energysolar"){
        solars.push_back(unit);
      }

      if(defName == "energywind"){
        windgens.push_back(unit);
      }

      if(defName == "energypylon"){
        pylons.push_back(unit);
      }

      if(defName == "energyfusion"){
        fusions.push_back(unit);
      }

      if(defName == "energysingu"){
        fusions.push_back(unit);
      }
    }

    return 0;
}

int EconomyManager::unitIdle(springai::Unit* u){
    if(newWorkers.find(u->GetUnitId()) != newWorkers.end() && u->GetCurrentCommands().empty()){
      Worker* w = new Worker(ai, u);
      workers[w->id] = w;
      if((int)workers.size() > 1 + ai->mergedAllies && (greed < 2 || greed < (workers.size() - commanders.size()) * std::min(1.f - graphManager->territoryFraction, 1.f))){
        w->isGreedy = true;
        greed++;
      }

      generateTasks(*w);
      assignWorker(*w);
      newWorkers.erase(u->GetUnitId());

      assigned.push_back(w);
      seeders.push_back(w);
    }
    return 0;
}

int EconomyManager::enemyEnterLOS(springai::Unit* enemy){
    // capture combat reclaim for enemy building nanoframes that enter los
    if(enemy->IsBeingBuilt() && enemy->GetDef()->GetSpeed() == 0 && warManager->getPorcThreat(enemy->GetPos()) == 0){
      CombatReclaimTask* crt = new CombatReclaimTask(enemy);
      if(!CONTAINSP(combatReclaimTasks,crt)){
        combatReclaimTasks.push_back(crt);
      }
    }

    std::string defname(enemy->GetDef()->GetName());
    // porc over enemy mexes
    if(defname == "staticmex" && graphManager->isAllyTerritory(enemy->GetPos())){
      defendMex(enemy->GetPos());
    }

    // check for enemy air
    if(enemy->GetDef()->IsAbleToFly() && defname != "dronelight" && defname != "droneheavyslow" && defname != "dronecarry"){
      enemyHasAir = true;
    }
    return 0;
}

int EconomyManager::enemyCreated(springai::Unit* enemy){
    // capture combat reclaim for undefended enemy buildings that are started within los
    if(enemy->GetDef() != nullptr && enemy->GetDef()->GetSpeed() == 0 && warManager->getThreat(enemy->GetPos()) == 0){
      CombatReclaimTask* crt = new CombatReclaimTask(enemy);
      if(!CONTAINSP(combatReclaimTasks,crt)){
        combatReclaimTasks.push_back(crt);
      } else delete crt;
    }

    // porc over enemy mexes
    if(std::string(enemy->GetDef()->GetName()) == "staticmex" && graphManager->isAllyTerritory(enemy->GetPos())){
      defendMex(enemy->GetPos());
    }
    return 0;
}

void EconomyManager::checkNanos(){
    for(auto &[k,n] : nanos){
      if(n->target == nullptr || n->target->GetHealth() <= 0){
        n->unit->SelfDestruct(0);
      }
    }
}

void EconomyManager::setEcoPriorities(){
    std::vector<float> params;
    if(!hardEstalled && !graphManager->eminentTerritory && adjustedIncome < 20.f){
      params.push_back(2.f);
    } else {
      params.push_back(1.f);
    }
    for(springai::Unit* u : newMexes){
      if(u->GetHealth() > 0 && u->GetTeam() == ai->teamID && u->IsBeingBuilt()){
        u->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
      }
    }
    params.clear();

    if(hardEstalled){
      params.push_back(2.f);
    } else {
      params.push_back(1.f);
    }
    for(springai::Unit* u : newEnergy){
      if(u->GetHealth() > 0 && u->GetTeam() == ai->teamID && u->IsBeingBuilt()){
        u->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
      }
    }
    for(springai::Unit* u : fusions){
      if(u->GetHealth() > 0 && u->GetTeam() == ai->teamID && u->IsBeingBuilt()){
        u->ExecuteCustomCommand(CMD_PRIORITY, params, 0);
      }
    }
}

void EconomyManager::assignWorkers(){
    if(frame < 8) return;

    // Generate tasks and assign workers to tasks on alternating frames.
    // Also refresh retreat paths for chickening units and process them when they're done chickening.
    if(frame % 2 == 0){
      if(!idlers.empty()){
        Worker* w = idlers.front();
        idlers.erase(idlers.begin());
        if(w->getUnit()->GetHealth() <= 0 || w->getUnit()->GetTeam() != ai->teamID){
          w->clearTask();
          if(w->isGreedy) greed--;
          workers.erase(w->id);
          ERASEONE(commanders,w);
          ERASEONE(seeders,w);
          delete w;
        } else {
          if(!w->isChicken && (warManager->getEffectiveThreat(w->getPos()) > warManager->getTotalFriendlyThreat(w->getPos())
              || ((w->getUnit()->GetHealth() < w->getUnit()->GetMaxHealth() || w->getShields() < 0.5f) && warManager->getThreat(w->getPos()) > 0 && !w->isWorkingOnPorc()))){
            w->isChicken = true;
            w->chickenFrame = frame;
            warManager->addDefenseTarget(DefenseTarget(w->getPos(), w->getUnit()->GetMaxHealth(), frame));
            ERASEONE(seeders,w);
          }
          if(w->isChicken){
            if (warManager->getThreat(w->getPos()) == 0 && ((warManager->getTotalFriendlyThreat(w->getPos()) > 0 && frame - w->chickenFrame > 90) || frame - w->chickenFrame > 150)){
              w->isChicken = false;
              assignWorker(*w);
              seeders.push_back(w);
            } else if(frame - w->lastRetreatFrame > 15){
              springai::AIFloat3 pos = graphManager->getClosestHaven(w->getPos());
              if(distance(w->getPos(), pos) > 25.f){
                w->retreatTo(pos, frame);
                if(warManager->getThreat(w->getPos()) > 0) w->chickenFrame = frame;
              } else {
                w->isChicken = false;
                assignWorker(*w);
                seeders.push_back(w);
              }
            }
          } else {
            assignWorker(*w);
          }
          assigned.push_back(w);
        }
      } else if(!assigned.empty()){
        Worker* const w = assigned.front(); assigned.erase(assigned.begin());
        if(w->getUnit()->GetHealth() <= 0 || w->getUnit()->GetTeam() != ai->teamID)
        { w->clearTask();
          if(w->isGreedy) greed--;
          workers.erase(w->id);
          ERASEONE(commanders,w);
          ERASEONE(seeders,w);
          ERASEONE(idlers,w);
          delete w;
        } else {
          if(!w->isChicken && (warManager->getEffectiveThreat(w->getPos()) > warManager->getTotalFriendlyThreat(w->getPos())
              || ((w->getUnit()->GetHealth() < w->getUnit()->GetMaxHealth() || w->getShields() < 0.5f) && warManager->getThreat(w->getPos()) > 0 && !w->isWorkingOnPorc())))
          { w->isChicken = true;
            w->chickenFrame = frame;
            warManager->addDefenseTarget(DefenseTarget(w->getPos(), w->getUnit()->GetMaxHealth(), frame));
            ERASEONE(seeders,w);
          }
          if(w->isChicken){
            if(warManager->getThreat(w->getPos()) == 0 && ((warManager->getTotalFriendlyThreat(w->getPos()) > 0 && frame - w->chickenFrame > 90) || frame - w->chickenFrame > 150))
            { w->isChicken = false;
              assignWorker(*w);
              seeders.push_back(w);
            } else if(frame - w->lastRetreatFrame > 15){
              springai::AIFloat3 pos = graphManager->getClosestHaven(w->getPos());
              if(distance(w->getPos(), pos) > 25.f){
                w->retreatTo(pos, frame);
                if(warManager->getThreat(w->getPos()) > 0) w->chickenFrame = frame;
              } else {
                w->isChicken = false;
                assignWorker(*w);
                seeders.push_back(w);
              }
            }
          } else if(!w->unstick(frame, m, e)){
            assignWorker(*w);
          }
          assigned.push_back(w);
        }
      }
    } else if(!seeders.empty()){
      Worker* w = seeders.front();
      seeders.erase(seeders.begin());
      if(w->getUnit()->GetHealth() <= 0 || w->getUnit()->GetTeam() != ai->teamID){
        w->clearTask();
        if(w->isGreedy) greed--;
        workers.erase(w->id);
        ERASEONE(commanders,w);
        ERASEONE(idlers,w);
        ERASEONE(assigned,w);
        delete w;
      } else {
        generateTasks(*w);
        seeders.push_back(w);
      }
    }
}

void EconomyManager::assignWorker(Worker& w) {
    WorkerTask* task = getCheapestJob(w);

    WorkerTask* wtask = w.getTask();
    if(task != nullptr && (wtask == nullptr || task->taskID != wtask->taskID || distance(w.getPos(), task->getPos()) > w.getUnit()->GetDef()->GetBuildDistance() * 1.5f))
    {
      // remove it from its old assignment if it had one
      if(wtask == nullptr || task->taskID != wtask->taskID){
        w.clearTask();
        w.setTask(task, frame);
      }

      bool outOfRange = distance(w.getPos(), task->getPos()) > w.getUnit()->GetDef()->GetBuildDistance() * 1.5f;
      if(task->type() == CONSTRUCTIONTASK){
        ConstructionTask* ctask = (ConstructionTask*) task;
        if(ctask->target != nullptr && ctask->target->GetHealth() <= 0) ctask->target = nullptr;
        if(outOfRange){
          w.moveTo(ctask->getPos());
          if(ctask->target == nullptr){
            w.getUnit()->Build(ctask->buildType, ctask->getPos(), ctask->facing, OPTION_SHIFT_KEY);
          } else {
            w.getUnit()->Repair(ctask->target, OPTION_SHIFT_KEY);
          }
        } else {
          if(ctask->target == nullptr){
            w.getUnit()->Build(ctask->buildType, ctask->getPos(), ctask->facing, 0);
          } else {
            w.getUnit()->Repair(ctask->target, 0);
          }
        }
      } else if(task->type() == RECLAIMTASK){
        ReclaimTask* rt = (ReclaimTask*) task;
        if(outOfRange){
          w.moveTo(rt->getPos());
          w.getUnit()->ReclaimFeature(rt->target, OPTION_SHIFT_KEY);
        } else {
          w.getUnit()->ReclaimFeature(rt->target, 0);
        }
      } else if(task->type() == COMBATRECLAIMTASK){
        CombatReclaimTask* crt = (CombatReclaimTask*)task;
        // prevent workers from being unassigned to dangerous/invalid combat reclaim jobs
        if((!crt->target->IsBeingBuilt() && crt->target->GetTeam() != ai->teamID) || warManager->getThreat(crt->getPos()) > 0 || crt->target->GetHealth() <= 0)
        { std::vector<Worker*> idle = task->stopWorkers();
          for(Worker* wo : idle){
            if(&w == wo) continue;
            idlers.push_back(wo);
            ERASEONE(assigned, wo);
          }
          ERASEONE(combatReclaimTasks,crt);
          delete crt;
          return;
        }
        // else assign
        if(outOfRange){
          w.moveTo(crt->getPos());
          w.getUnit()->ReclaimUnit(crt->target, OPTION_SHIFT_KEY);
        } else {
          w.getUnit()->ReclaimUnit(crt->target, 0);
        }
      } else if(task->type() == REPAIRTASK){
        RepairTask* rt = (RepairTask*)task;
        if(outOfRange){
          w.moveTo(rt->getPos());
          w.getUnit()->Repair(rt->target, OPTION_SHIFT_KEY);
        } else {
          w.getUnit()->Repair(rt->target, 0);
        }
      }
    }
}

WorkerTask* EconomyManager::getCheapestJob(Worker& worker){
    WorkerTask* task = nullptr;
    float cost = 65536;

    if(worker.getTask() != nullptr){
      task = worker.getTask();
      cost = costOfJob(worker, *task);
      if(task->type() == CONSTRUCTIONTASK){
        ConstructionTask* ctask = (ConstructionTask*)task;
        if(ctask->target != nullptr) cost -= 1000.f * (ctask->target->GetBuildProgress() * ctask->target->GetBuildProgress());
      }

      cost += 500.f * std::max(0.f, warManager->getEffectiveThreat(task->getPos()));
    }

    for(WorkerTask* t : constructionTasks){
      float tmpcost = costOfJob(worker, *t);
      tmpcost += 500.f * std::max(0.f, warManager->getEffectiveThreat(t->getPos()));
      if(task == nullptr || tmpcost < cost){
        cost = tmpcost;
        task = t;
      }
    }

    if(maxStorage > 1.f && (metal/maxStorage < 0.9f || !hardEstalled) && (!energyReclaim || energyReclaimTasks.empty())){
      for(WorkerTask* t : reclaimTasks){
        float tmpcost = costOfJob(worker, *t);
        tmpcost += 500.f * std::max(0.f, warManager->getEffectiveThreat(t->getPos()));
        if(task == nullptr || tmpcost < cost){
          cost = tmpcost;
          task = t;
        }
      }
    }

    if(energyReclaim){
      for(WorkerTask* t : energyReclaimTasks){
        float tmpcost = costOfJob(worker, *t);
        tmpcost += 500.f * std::max(0.f, warManager->getEffectiveThreat(t->getPos()));
        if(task == nullptr || tmpcost < cost){
          cost = tmpcost;
          task = t;
        }
      }
    }

    for(WorkerTask* t : combatReclaimTasks){
      float tmpcost = costOfJob(worker, *t);
      if(task == nullptr || tmpcost < cost){
        cost = tmpcost;
        task = t;
      }
    }
    for(RepairTask* rt : repairTasks){
      // don't assign workers to repair themselves
      if(rt->target->GetUnitId() != worker.id){
        float tmpcost = costOfJob(worker, *rt);
        tmpcost += 500.f * std::max(0.f, warManager->getEffectiveThreat(rt->getPos()));
        if(task == nullptr || tmpcost < cost){
          cost = tmpcost;
          task = rt;
        }
      }
    }
    return task;
}

float EconomyManager::costOfJob(const Worker& worker, const WorkerTask& task){
    float costMod = 1.f;
    float dist = (distance(worker.getPos(),task.getPos()));

    for(Worker* w : task.assignedWorkers){
      // increment cost mod for every other worker unassigned to the given task that isn't the worker we're assigning
      // as long as they're closer or equaldist to the target.
      float idist = distance(w->getPos(),task.getPos());
      float rdist = std::max(idist, 200.f);
      float deltadist = std::abs(idist - dist);
      if(w->id != worker.id){
        if(w->isCom){
          costMod += 2;
        } else if(rdist < dist || deltadist < 100.f){
          costMod++;
        }
      }
    }

    if(task.type() == CONSTRUCTIONTASK){
      const ConstructionTask& ctask = dynamic_cast<const ConstructionTask&>(task);

      if(ctask.facPlop){
        // only assign facplop tasks to the commander who has the plop!
        if(worker.id != ctask.assignedPlop){
          return 65535;
        } else {
          return -1000.f;
        }
      }

      if(ctask.buildType->GetUnitDefId() == buildIDs->solarID || ctask.buildType->GetUnitDefId() == buildIDs->windID){
        // for small energy
        if(estalled || effectiveIncomeMetal > effectiveIncomeEnergy - 2.5f) {
          if(worker.isGreedy) return dist + (10000.f * costMod); // greedy workers shouldn't build energy at all.
          if(worker.isCom){
            if(hardEstalled) return ((dist / 2.f) - 300.f) + std::max(0.f, (1000 * (costMod - 2)));
            return ((dist / 3.f) - 200.f) + std::max(0.f, (1000 * (costMod - 2)));
          }
          return ((dist / 3.f) - 200.f) + std::max(0.f, (1000 * (costMod - 1)));
        } else {
          if(worker.isGreedy) return dist + (10000.f * costMod); // greedy workers shouldn't build energy at all.
          if(worker.isCom) return (dist/4.f) + std::max(0.f, (1000 * (costMod - 2)));
          return (dist/4.f) + std::max(0.f, (1000 * (costMod - 1)));
        }
      }

      if(ctask.buildType->GetUnitDefId() == buildIDs->mexID){
        // for mexes
        if(worker.isGreedy) return ((dist/4.f) - 100.f) + std::max(0.f, (600.f * (costMod - 1)));
        return ((dist/4.f) - 100.f) + std::max(0.f, (600.f * (costMod - 2)));
      }

      if(worker.isGreedy && !facManager->factories.empty() && ((ctask.facDef && effectiveIncome < 15.f) || (ctask.buildType->GetCost(m) > 300.f && ctask.buildType->GetUnitDefId() != buildIDs->airpadID))){
        return dist + (10000.f * costMod);
      }

      if(CONTAINSI(buildIDs->facIDs,ctask.buildType->GetUnitDefId())){
        // emergency facs get maximum priority
        if(facManager->factories.empty()){
          return -1000;
        }
        return ((dist / 2) - ctask.buildType->GetCost(m)) + (500 * (costMod - 1));
      }

      if(ctask.facDef && worker.isCom){
        Worker* f = getNearestFac(ctask.getPos());
        if(f != nullptr && f->getUnit()->GetHealth() < f->getUnit()->GetMaxHealth()){
          return -1000.f;
        }
      }

      if(CONTAINSI(buildIDs->expensiveIDs,ctask.buildType->GetUnitDefId())){
        // for expensive porc.
        if(hardEstalled) return dist + (250.f * costMod);
        if(costMod < 4){
          return (dist/2) - std::min(3500.f, ctask.buildType->GetCost(m));
        }
        return ((dist/2) - std::min(3500.f, ctask.buildType->GetCost(m))) + (500 * costMod);
      }

      if(ctask.buildType->IsAbleToAttack()){
        // for cheap porc
        if(!defendedFac || warManager->slasherSpam * 140 > warManager->enemyPorcValue){
          return dist+300;
        }
        if(worker.isCom && !estalled) return ((dist/4.f) - 100.f) + std::max(0.f, (600.f * (costMod - 2)));
        if(worker.isGreedy) return ((dist/3.f) - 200.f) + std::max(0.f, (600.f * (costMod - 1)));
        return ((dist/2.f) - 200.f) + std::max(0.f, (1500 * (costMod - 2)));
      }

      if(ctask.buildType->GetUnitDefId() == buildIDs->airpadID){
        // for airpads
        return (dist/3.f) - 350.f + std::max(0.f, (500 * (costMod - 2)));
      }

      if(ctask.buildType->GetUnitDefId() == buildIDs->nanoID || ctask.buildType->GetUnitDefId() == buildIDs->storageID){
        // for nanotowers and storages
        if(worker.isCom || (hardEstalled && !worker.isGreedy)) return dist;
        return dist - 1000 + (500 * (costMod - 2));
      }

      if(ctask.buildType->GetUnitDefId() == buildIDs->pylonID){
        // for pylons
        if(worker.isGreedy){
          if(fusions.size() > pylons.size()) return (dist/4.f) - 3000.f + (500.f * (costMod - 1));
          return dist + (10000.f * costMod);
        }
        if(fusions.size() > pylons.size()) return (dist/4.f) - 1000.f + (500.f * (costMod - 1));
        if(fusions.size() > 3) return (dist/4.f) - 600.f + (500.f * (costMod - 1));
        if(fusions.size() > 2) return (dist/4.f) - 300.f + (500.f * (costMod - 1));
        return (dist/4.f) - 100.f + (500.f * (costMod - 1));
      }

      if(ctask.buildType->GetUnitDefId() == buildIDs->radarID){
        // for radar
        if(hardEstalled && !worker.isGreedy) return dist + 300.f;
        return (dist/4.f) - 100 + (1500 * (costMod - 1));
      }

      if(ctask.buildType->GetCost(m) > 300){
        // allow at least 3 builders for each expensive task, more if the cost is high.
        return ((dist/2) - std::min(3000.f, ctask.buildType->GetCost(m))) + std::max(0.f, (500 * (costMod - 3)));
      }

      return (dist - 250) + (100 * (costMod - 1));
    }

    if(task.type() == RECLAIMTASK){
      const ReclaimTask& rtask = dynamic_cast<const ReclaimTask&>(task);
      if(rtask.target->GetDef() == nullptr) return 65535;

      if(warManager->getPorcThreat(rtask.getPos()) > 0){
        // don't reclaim under enemy porc
        return (dist * dist) + (1000.f * costMod);
      }

      float recMod = 100.f;
      if(graphManager->isAllyTerritory(rtask.getPos())){
        recMod = 100.f + (200.f * graphManager->territoryFraction * graphManager->territoryFraction);
      }

      if(rtask.metalValue > 50){
        return ((dist/4.f) - recMod) + std::max(0.f, (250 * (costMod - 3)));
      }
      return ((dist/4.f) - recMod) + (700 * (costMod - 1));
    }

    if(task.type() == COMBATRECLAIMTASK){
      // favor combat reclaim when it's possible
      return dist - 600 + std::max(0.f, (600 * (costMod - 2)));
    }

    if(task.type() == REPAIRTASK){
      const RepairTask& rptask = dynamic_cast<const RepairTask&>(task);
      if(rptask.target->GetHealth() > 0) {
        if(!worker.isGreedy && rptask.target->GetDef()->GetSpeed() > 0 && rptask.target->GetDef()->IsAbleToAttack() && !rptask.target->GetDef()->IsBuilder()){
          // for mobile combat units
          if(warManager->getPorcThreat(rptask.getPos()) > 0) return 65535;

          if(hardEstalled){
            return dist;
          }

          if(rptask.isShieldMob && worker.hasShields){
            return dist + (100 * (costMod - 1)) - 250 - rptask.target->GetMaxHealth() / (5 * costMod);
          }

          if(!graphManager->isAllyTerritory(rptask.getPos())){
            return dist + 500;
          }

          return dist + (100 * (costMod - 1)) - 250 - (rptask.target->GetMaxHealth() + rptask.target->GetHealth()) / (10 * costMod);
        } else if(!worker.isGreedy && rptask.target->GetDef()->IsAbleToAttack() && !rptask.target->GetDef()->IsBuilder() && std::string(rptask.target->GetDef()->GetName()) != "turretaalaser"){
          // for static defenses
          return dist + (100 * (costMod - 1)) - (rptask.target->GetMaxHealth() * 2) / costMod;
        } else {
          // for everything else
          return dist - 250 + (600 * (costMod - 1));
        }
      } else {
        return 65536;
      }
    }

    return 65536;
}

bool EconomyManager::buildCheck(const ConstructionTask& task){
    // stop things from being built underwater if the map water does damage or if water pathing is bad on the map
    if((waterDamage || !graphManager->isWaterMap) && callback->GetMap()->GetElevationAt(task.getPos().x, task.getPos().z) < 10.f){
      return false;
    }

    float xsize = 0;
    float zsize = 0;

    //get the new building's area based on facing
    if(task.facing == 0 || task.facing == 2){
      xsize = task.buildType->GetXSize()*4.f;
      zsize = task.buildType->GetZSize()*4.f;
    } else {
      xsize = task.buildType->GetZSize()*4.f;
      zsize = task.buildType->GetXSize()*4.f;
    }

    //check for overlap with existing queued jobs
    for(ConstructionTask* c : constructionTasks){
      float cxsize = 0;
      float czsize = 0;

      //get the queued building's area based on facing
      if(c->facing == 0 || c->facing == 2){
        cxsize = c->buildType->GetXSize()*4.f;
        czsize = c->buildType->GetZSize()*4.f;
      } else {
        cxsize = c->buildType->GetZSize()*4.f;
        czsize = c->buildType->GetXSize()*4.f;
      }
      float minTolerance = xsize+cxsize;
      float axisDist = std::abs(c->getPos().x - task.getPos().x);
      if(axisDist < minTolerance){
        // if it's too close in the x dimension
        minTolerance = zsize+czsize;
        axisDist = std::abs(c->getPos().z - task.getPos().z);
        if(axisDist < minTolerance){
          //and it's too close in the z dimension
          return false;
        }
      }
    }

    for(auto &[k,w] : facManager->factories){
      // check fac overlap
      float cxsize = 0;
      float czsize = 0;
      springai::Unit* fac = w->getUnit();
      springai::UnitDef* def = fac->GetDef();
      if(def == nullptr) continue;
      int facing = fac->GetBuildingFacing();

      if(facing == 0 || facing == 2){
        cxsize = (def->GetXSize() + 1.f) * 4.f;
        czsize = (def->GetZSize() + 1.f) * 4.f;
      } else {
        cxsize = (def->GetZSize() + 1.f) * 4.f;
        czsize = (def->GetXSize() + 1.f) * 4.f;
      }
      float minTolerance = xsize+cxsize;
      float axisDist = std::abs(fac->GetPos().x - task.getPos().x);
      if(axisDist < minTolerance){
        // if it's too close in the x dimension
        minTolerance = zsize+czsize;
        axisDist = std::abs(fac->GetPos().z - task.getPos().z);
        if(axisDist < minTolerance){
          //and it's too close in the z dimension
          return false;
        }
      }
    }
    return true;
}

void EconomyManager::addCombatReclaimTask(CombatReclaimTask&& t0){
    CombatReclaimTask* t = new CombatReclaimTask(std::move(t0));
    combatReclaimTasks.push_back(t);
}

void EconomyManager::dropConstructionTask(ConstructionTask& ct){
    std::vector<Worker*> idle = ct.stopWorkers();
    for(Worker* w : idle){
      idlers.push_back(w);
      ERASEONE(assigned, w);
    }
    ConstructionTask* t = &ct;
    ERASEONE(factoryTasks,t);
    ERASEONE(superWepTasks,t);
    ERASEONE(radarTasks,t);
    ERASEONE(constructionTasks,t);
    ERASEONE(solarTasks,t);
    ERASEONE(windTasks,t);
    ERASEONE(fusionTasks,t);
    ERASEONE(pylonTasks,t);
    ERASEONE(porcTasks,t);
    ERASEONE(nanoTasks,t);
    ERASEONE(storageTasks,t);
    ERASEONE(AATasks,t);
    ERASEONE(airpadTasks,t);
    delete t;
}

void EconomyManager::cleanOrders(){
    //remove invalid jobs from the queue
    std::set<ConstructionTask*> invalidtasks;

    for(ConstructionTask* t : constructionTasks){
      if(t->target == nullptr && !callback->GetMap()->IsPossibleToBuildAt(t->buildType, t->getPos(), t->facing)){
        //check to make sure it isn't our own nanoframe, since update is called before unitCreated
        std::vector<springai::Unit*> stuff = callback->GetFriendlyUnitsIn(t->getPos(), std::min(t->buildType->GetXSize(), t->buildType->GetZSize()) * 4.f, true);
        bool isNano = false;
        for(springai::Unit* u : stuff){
          if (u->IsBeingBuilt() && u->GetDef()->GetUnitDefId() == t->buildType->GetUnitDefId() && u->GetTeam() == ai->teamID){
            isNano = true;
            t->target = u;
            break;
          }
        }
        if(!isNano){
          // if a construction job is blocked and it isn't our own nanoframe, remove it
          invalidtasks.insert(t);
        }
      }

      if(warManager->getPorcThreat(t->getPos()) > 0 && (t->target == nullptr || !t->buildType->IsAbleToAttack())){
        invalidtasks.insert(t);
      }
    }

    for(ConstructionTask* t : superWepTasks){
      if(invalidtasks.find(t) != invalidtasks.end()) continue;
      if(frame-t->frameIssued > 900 && t->target == nullptr){
        invalidtasks.insert(t);
      }
    }
    // remove old porc push tasks, so as not to waste metal on unneeded porc.
    for(ConstructionTask* t : porcTasks){
      if(invalidtasks.find(t) != invalidtasks.end()) continue;
      if(t->frameIssued > 0 && frame - t->frameIssued > 600 && t->assignedWorkers.empty() && t->target == nullptr){
        invalidtasks.insert(t);
      }
    }

    for(ConstructionTask* t : invalidtasks){
      dropConstructionTask(*t);
    } invalidtasks.clear();

    for(auto it=reclaimTasks.begin(); it!=reclaimTasks.end(); ){
      ReclaimTask* const rt=*it;
      if(rt->target->GetDef() == nullptr){
        std::vector<Worker*> idle = rt->stopWorkers();
        for(Worker* w : idle){
          idlers.push_back(w);
          ERASEONE(assigned, w);
        }
        delete rt; it=reclaimTasks.erase(it);
      } else ++it;
    }

    for(auto it=combatReclaimTasks.begin(); it!=combatReclaimTasks.end(); ){
      CombatReclaimTask* const crt=*it;
      if((!crt->target->IsBeingBuilt() && crt->target->GetTeam() != ai->teamID) || warManager->getPorcThreat(crt->getPos()) > 0 || crt->target->GetHealth() <= 0)
      { std::vector<Worker*> idle = crt->stopWorkers();
        for(Worker* w : idle){
          idlers.push_back(w);
          ERASEONE(assigned, w);
        }
        delete crt; it=combatReclaimTasks.erase(it);
      } else ++it;
    }

    for(auto it=repairTasks.begin(); it!=repairTasks.end(); ){
      RepairTask* const rt =*it;
      if(rt->target->GetHealth() <= 0 || rt->target->GetHealth() == rt->target->GetMaxHealth()){
        std::vector<Worker*> idle = rt->stopWorkers();
        for(Worker* w : idle){
          idlers.push_back(w);
          ERASEONE(assigned, w);
        }
        delete rt; it=repairTasks.erase(it);
      } else ++it;
    }

    if(choseSupWep && sWep == 1 && warManager->enemyHasAntiNuke){
      sWep = 0;
    }
}

void EconomyManager::generateTasks(Worker& worker){
    springai::AIFloat3 position = worker.getPos();

    // do we need a factory?
    bool hasPlop = worker.hasPlop();
    if((hasPlop && !worker.assignedPlop)
        || (frame > 900 && !hasPlop && (int)facManager->factories.size() < 1 + ai->mergedAllies && factoryTasks.empty())
        || (staticIncome > 50.f && facManager->factories.size() == 1 && nanos.size() > 3 && factoryTasks.empty())
        || (staticIncome > 90.f && facManager->factories.size() == 2 && nanos.size() > 6 && graphManager->eminentTerritory && factoryTasks.empty())
        || (ai->mergedAllies > 0 && staticIncome > 65.f && !hasStriders && graphManager->eminentTerritory && factoryTasks.empty())
        || (ai->mergedAllies > 0 && staticIncome > 65.f && graphManager->territoryFraction > 0.35f && (!hasPlanes && !hasGunship) && factoryTasks.empty())
        || (ai->mergedAllies > 0 && bigMap && graphManager->territoryFraction > 0.4f && (!warManager->miscHandler->striders.empty() || !warManager->squadHandler->shieldSquads.empty()) && (!hasPlanes || !hasGunship) && factoryTasks.empty())) {
      createFactoryTask(worker, hasPlop);
    }

    if(!worker.isCom) collectReclaimables(worker);

    // 74145: TODO: lower the value to rev value?
    // do we need AA?
    if(enemyHasAir && warManager->maxEnemyAirValue > 1000.f && graphManager->territoryFraction > 0.4f && AATasks.empty()){
      createAATask(worker);
    }

    // is there sufficient energy to cover metal income?
    if(!worker.isGreedy && estalled && (int)solarTasks.size() + (int)windTasks.size() < facManager->numWorkers){
      createEnergyTask(worker);
    }
    createFusionTask(worker);

    // do we need caretakers?
    if(!facManager->factories.empty()
        && (float)(nanos.size() + nanoTasks.size() + facManager->factories.size() + factoryTasks.size()) < std::floor(staticIncome/(10.f))){
      createNanoTurretTask(worker, getCaretakerTarget());
    }

    //do we need storages?
    if(frame > 600 && !facManager->factories.empty()
        && (metal/maxStorage > 0.8f || (int)commanders.size() + (int)storages.size() < 1 + ai->mergedAllies) && storageTasks.empty()){
      createStorageTask(worker, getCaretakerTarget());
    }

    // do we need airpads for planes
    if(!worker.isGreedy && !facManager->factories.empty()
        && havePlanes && airpadTasks.empty() && staticIncome > 15.f
        && (airpads.size() < std::round(warManager->bomberHandler->getBomberSize()/8.f) || airpads.empty())){
      createAirPadTask(getCaretakerTarget());
    }

    // do we need radar?
    if(!worker.isCom && adjustedIncome > 20.f && !hardEstalled){
      createRadarTask(worker);
    }

    // porc push against nearby enemy porc and defend the front line
    if(defendedFac && porcTasks.size() < facManager->numWorkers * 1.5f){
      porcPush(worker.getPos());
      if(graphManager->eminentTerritory) defendFront(worker.getPos());
    }

    // do we need pylons?
    if(fusions.size() > 1){ // 74145: was > 3, lowered since grid requirements exist
      createGridTask(worker.getPos());
    }

    if(bigMap && superWeps.size() > 3 && !hasSuperWep && superWepTasks.empty()){
      choseSupWep = false;
    }

    // do we need superweapons?
    if(!choseSupWep && ((ai->allies.size() > 0 && hasStriders && (facManager->numStriders > 1)) || ai->allies.empty()) && fusions.size() > 4){
      chooseSuperWeapon();
    }

    if(choseSupWep){
      if((sWep == 0 || hasSuperWep) && superWepTasks.empty()){
        createBerthaTask();
      } else if(sWep > 0 && !hasSuperWep && superWepTasks.empty()){
        createSuperWepTask();
      }
    }

    // do we need a protector?
    if(!antiNuke && (warManager->enemyHasNuke || gotNuked)){
      antiNuke = true;
      springai::UnitDef* protector = callback->GetUnitDefByName("staticantinuke");
      springai::AIFloat3 pos = callback->GetMap()->FindClosestBuildSite(protector, graphManager->getAllyCenter(), 600, 3, 0);
      ConstructionTask* ct = new ConstructionTask(protector, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(constructionTasks,ct)){
        constructionTasks.push_back(ct);
      } else delete ct;
    }
}

void EconomyManager::createFactoryTask(Worker& worker, bool isPlop){
    springai::UnitDef* factory;

    if(potentialFacList.empty() && (int)facManager->factories.size() < 1 + ai->mergedAllies){
      potentialFacList = terrainManager->getInitialFacList();
      addedFacs = false;
    }

    if(facManager->factories.size() > 0 && !isPlop && !addedFacs){
      potentialFacList.insert("factorygunship");

      potentialFacList.insert("factoryplane");
      if(!pylons.empty()) potentialFacList.insert("striderhub");

      if(facManager->factories.size() > 0 && (facManager->factories.size() < 3 || ai->mergedAllies > 0)){
        // only build air or striders as second and third facs.
        potentialFacList.erase("factoryspider");
        potentialFacList.erase("factorycloak");
        potentialFacList.erase("factoryveh");
        potentialFacList.erase("factoryshield");
        potentialFacList.erase("factoryamph");
        potentialFacList.erase("factoryhover");
        potentialFacList.erase("factoryship");
        potentialFacList.erase("factoryjump");
        potentialFacList.erase("factorytank");
      }
      addedFacs = true;
    }

    hasStriders = false;
    for(auto &[k,f] : facManager->factories){
      std::string defName(f->getUnit()->GetDef()->GetName());
      if(defName == "striderhub"){
        hasStriders = true;
      }
      // don't build the same fac twice.
      potentialFacList.erase(defName);
    }

    if(potentialFacList.empty()){
      addedFacs = false;
      return;
    }

    std::string facName = "";

    // Uncomment this to set the intial fac for debugging purposes.
    /*if(facManager.factories.size() == 0){
      facName = "factoryveh";
      factory = callback->GetUnitDefByName(facName);
      ERASEONE(potentialFacList, facName);
    } else*/ if(userFac != "" && facManager->factories.size() == 0){
      factory = callback->GetUnitDefByName(userFac.c_str());
      ERASEONE(potentialFacList, userFac);
    } else if(potentialFacList.find("factoryship") != potentialFacList.end() && kgb_rand() < 0.5){
      facName = "factoryship";
      factory = callback->GetUnitDefByName(facName.c_str());
      potentialFacList.erase(facName);
    } else if(isPlop && potentialFacList.find("factorygunship") != potentialFacList.end()){
      facName = "factorygunship";
      factory = callback->GetUnitDefByName(facName.c_str());
      potentialFacList.erase(facName);
    } else if((int)facManager->factories.size() < 1 + std::max(ai->mergedAllies, 1) || hasStriders || isPlop || pylons.empty()){
      int n = (int)(kgb_rand() * potentialFacList.size());
      auto it = potentialFacList.begin();
      std::advance(it, n);
      facName = *it;
      factory = callback->GetUnitDefByName(facName.c_str());
      potentialFacList.erase(facName);

      // don't build two airfacs in small teams or on small maps.
      if(ai->mergedAllies < 5 && !bigMap){
        if(facName == "factoryplane") potentialFacList.erase("factorygunship");
        else if(facName == "factorygunship") potentialFacList.erase("factoryplane");
      }
    } else {
      facName = "striderhub";
      factory = callback->GetUnitDefByName(facName.c_str());
      potentialFacList.erase(facName);
    }

    springai::AIFloat3 position = worker.getPos();

    short facing;
    int mapWidth = callback->GetMap()->GetWidth() *8;
    int mapHeight = callback->GetMap()->GetHeight() *8;

    if(std::abs(mapWidth - 2*position.x) > std::abs(mapHeight - 2*position.z)){
      if(2*position.x>mapWidth){
        // facing="west"
        facing=3;
      } else {
        // facing="east"
        facing=1;
      }
    } else {
      if(2*position.z>mapHeight){
        // facing="north"
        facing=2;
      } else {
        // facing="south"
        facing=0;
      }
    }

    int i = 0;
    bool good = false;
    float clearance = std::max(factory->GetXSize(), factory->GetZSize()) * 4.f;
    if(facManager->factories.empty() || isPlop){
      ConstructionTask* ct;
      while(!good){
        if(i++ > 10){
          potentialFacList.insert(facName);
          return;
        }
        setpos(position,worker.getPos());
        setpos(position,getDirectionalPoint(position, graphManager->getMapCenter(), clearance + 20.f));
        setpos(position,callback->GetMap()->FindClosestBuildSite(factory, position,600.f, 3, facing));

        // don't let factories get blocked by random crap, nor build them on top of mexes
        int j = 0;
        ct =  new ConstructionTask(factory, position, 0);
        MetalSpot* closest = graphManager->getClosestSpot(position);
        while(!callback->GetFriendlyUnitsIn(position, clearance, true).empty() || !callback->GetFeaturesIn(position, clearance, true).empty() || (closest!=nullptr && distance(closest->getPos(), position) < 100) || !buildCheck(*ct)){
          if(j++ > 5) break;
          setpos(position,(i < 8) ? getAngularPoint(worker.getPos(), graphManager->getMapCenter(), clearance + 20.f) : getRadialPoint(worker.getPos(), 125.f));
          setpos(position,callback->GetMap()->FindClosestBuildSite(factory,position,600.f, 3, facing));
          closest = graphManager->getClosestSpot(position);
          delete ct; ct = new ConstructionTask(factory, position, 0);
        }
        delete ct; ct = new ConstructionTask(factory, position, facing); // only for preventing stupid placement
        if(buildCheck(*ct)){
          good = true;
        }
        delete ct;
      }
    } else {
      while(!good){
        if(i > 5){
          potentialFacList.insert(facName);
          return;
        }
        // don't cram factories together
        setpos(position,getAngularPoint(graphManager->getAllyCenter(), graphManager->getAllyBase(), std::max(450.f, distance(graphManager->getAllyBase(), graphManager->getAllyCenter()) * (float) kgb_rand())));
        Worker* fac = getNearestFac(position);
        if(fac==nullptr){ i++; continue; }
        springai::AIFloat3 facpos(getNearestFac(position)->getPos());

        // don't let factories get blocked by random crap, nor build them on top of mexes
        if(facName == "striderhub"){
          // 74145: Talas: whatever, it grids..
          setpos(position,graphManager->getOverdriveSweetSpot(position, facName));
        } else {
          int j = 0;
          MetalSpot* closest = graphManager->getClosestSpot(position);
          while(distance(facpos, position) < 400.f || callback->GetFriendlyUnitsIn(position, 150.f, true).size() > 0 || (closest!=nullptr && distance(closest->getPos(), position) < 100) || graphManager->isFrontLine(position)){
            if(j > 5) break;
            setpos(position,getRadialPoint(position, 175.f));
            setpos(facpos,getNearestFac(position)->getPos());
            closest = graphManager->getClosestSpot(position);
            j++;
          }
        }
        setpos(position,callback->GetMap()->FindClosestBuildSite(factory, position, 600.f, 3, facing));

        ConstructionTask* ct =  new ConstructionTask(factory, position, facing); // only for preventing stupid placement
        MetalSpot* closest = graphManager->getClosestSpot(position);
        if(distance(facpos, position) > 400.f && callback->GetFriendlyUnitsIn(position, 150.f, true).empty() && (closest==nullptr || distance(closest->getPos(), position) > 100) && !graphManager->isFrontLine(position) && buildCheck(*ct)){
          good = true;
        }
        i++;
        delete ct;
      }
    }

    ConstructionTask* ct = new ConstructionTask(factory, position, facing, isPlop);
    if(buildCheck(*ct) && !CONTAINSP(factoryTasks, ct) && worker.canReach(position)){
      if(isPlop){
        ct->assignedPlop = worker.id;
        worker.assignedPlop = true;
      }
      constructionTasks.push_back(ct);
      factoryTasks.push_back(ct);
    } else {
      if(!facName.empty()) potentialFacList.insert(facName);
      delete ct;
    }
}

void EconomyManager::createAATask(Worker& worker){
    springai::AIFloat3 position(graphManager->getClosestFrontLineLink(worker.getPos()));
    if(position == nullpos) return;

    springai::UnitDef* aa;

    if(!graphManager->eminentTerritory || effectiveIncome < 30.f || kgb_rand() > 0.5){
      aa = callback->GetUnitDefByName("turretaalaser");
    } else {
      aa = callback->GetUnitDefByName("turretaafar");
    }

    setpos(position,callback->GetMap()->FindClosestBuildSite(aa,position,600.f, 3, 0));

    // don't build AA too close together.
    for(springai::Unit* a : AAs){
      if(distance(a->GetPos(), position) < 250.f){
        return;
      }
    }

    ConstructionTask* ct = new ConstructionTask(aa, position, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      AATasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::chooseSuperWeapon(){
    if((superWeps.size() < 4 && kgb_rand() > 0.4) || !bigMap){
      sWep = 0;
    } else {
      double rnd = kgb_rand();
      if(!warManager->enemyHasAntiNuke && rnd > 0.66){
        sWep = 1;
      } else if(rnd > 0.33){
        sWep = 2;
      } else {
        sWep = 3;
      }
    }
    choseSupWep = true;
}

void EconomyManager::createBerthaTask(){
    if(facManager->factories.empty() || radars.empty()) return; // Eh, no.
    springai::UnitDef* bertha = callback->GetUnitDefByName("staticheavyarty");
    springai::AIFloat3 position;

    bool good = false;
    int i = 0;
    while(!good){
      if(++i > 4){
        return;
      }
      setpos(position,getAngularPoint(graphManager->getAllyBase(), graphManager->getMapCenter(), distance(graphManager->getAllyBase(), graphManager->getMapCenter()) * 0.65f));
      setpos(position,heightMap->getHighestPointInRadius(position, 800.f));
      setpos(position,callback->GetMap()->FindClosestBuildSite(bertha, position, 600.f, 3, 0));
      int j = 0;
      while(!good){
        if(++j > 3){
          break;
        }
        good = true;
        for(springai::Unit* b : superWeps){
          if(distance(b->GetPos(), position) < 350){
            good = false;
            break;
          }
        }

        if(graphManager->isEnemyTerritory(position) || graphManager->isFrontLine(position) || distance(position, getNearestFac(position)->getPos()) < 300.f) {
          good = false;
        }

        if(!good){
          setpos(position,getRadialPoint(position, 300.f));
          setpos(position,callback->GetMap()->FindClosestBuildSite(bertha, position, 600.f, 3, 0));
        }
      }
    }

    ConstructionTask* ct = new ConstructionTask(bertha, position, 0);
    if(buildCheck(*ct) && !CONTAINSP(superWepTasks,ct)){
      ct->frameIssued = frame;
      constructionTasks.push_back(ct);
      superWepTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::createSuperWepTask(){
    if(facManager->factories.empty() || radars.empty()) return; // Eh, no.
    springai::UnitDef* death;
    if(sWep == 1){
      death = callback->GetUnitDefByName("staticnuke");
    } else if(sWep == 2){
      death = callback->GetUnitDefByName("zenith");
    } else {
      death = callback->GetUnitDefByName("raveparty");
    }

    springai::AIFloat3 position;
    try {
      setpos(position,getDirectionalPoint(graphManager->getAllyBase(), graphManager->getAllyCenter(), distance(graphManager->getAllyBase(), graphManager->getAllyCenter()) * 0.3f));
      setpos(position,callback->GetMap()->FindClosestBuildSite(death, position, 600.f, 3, 0));
    } catch(const std::exception& e){
      ai->debug(e.what());
      return;
    }

    bool good = false;
    int i = 0;
    while(!good){
      if(++i > 3){
        return;
      }
      good = true;
      if(graphManager->isEnemyTerritory(position) || graphManager->isFrontLine(position) || distance(position, getNearestFac(position)->getPos()) < 300.f) {
        good = false;
      }

      if(!good){
        setpos(position,getRadialPoint(position, 400.f));
        setpos(position,callback->GetMap()->FindClosestBuildSite(death, position, 600.f, 3, 0));
      }
    }

    ConstructionTask* ct = new ConstructionTask(death, position, 0);
    if(buildCheck(*ct) && !CONTAINSP(superWepTasks,ct)){
      ct->frameIssued = frame;
      constructionTasks.push_back(ct);
      superWepTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::defendSingu(const springai::AIFloat3& position){
    springai::UnitDef* llt = callback->GetUnitDefByName("turretlaser");
    springai::UnitDef* defender = callback->GetUnitDefByName("turretmissile");
    springai::UnitDef* hlt = callback->GetUnitDefByName("turretheavylaser");
    springai::UnitDef* cobra = callback->GetUnitDefByName("turretaaflak");
    springai::UnitDef* shield = callback->GetUnitDefByName("staticshield");
    springai::AIFloat3 pos;
    bool good = false;
    ConstructionTask* ct;
    int i = 0;

    // build an llt
    while(!good){
      if(i++ > 100) break;
      setpos(pos,getRadialPoint(position, 150.f));
      setpos(pos,callback->GetMap()->FindClosestBuildSite(llt, pos, 600.f, 3, 0));

      ct = new ConstructionTask(llt, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
        constructionTasks.push_back(ct);
        porcTasks.push_back(ct);
        good = true;
      } else delete ct;
    }

    good = false;
    i = 0;
    // build a defender
    while(!good){
      if(i++ > 100) break;
      setpos(pos,getRadialPoint(position, 150.f));
      setpos(pos,callback->GetMap()->FindClosestBuildSite(defender, pos, 600.f, 3, 0));

      ct = new ConstructionTask(defender, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
        constructionTasks.push_back(ct);
        porcTasks.push_back(ct);
        good = true;
      } else delete ct;
    }

    good = false;
    i = 0;
    // build an hlt
    while(!good){
      if(i++ > 100) break;
      setpos(pos,getRadialPoint(position, 150.f));
      setpos(pos,callback->GetMap()->FindClosestBuildSite(hlt, pos, 600.f, 3, 0));

      ct = new ConstructionTask(hlt, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
        constructionTasks.push_back(ct);
        porcTasks.push_back(ct);
        good = true;
      } else delete ct;
    }

    good = false;
    i = 0;
    // build a cobra
    while(!good){
      if(i++ > 100) break;
      setpos(pos,getRadialPoint(position, 200.f));
      setpos(pos,callback->GetMap()->FindClosestBuildSite(cobra, pos, 600.f, 3, 0));

      ct = new ConstructionTask(cobra, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
        constructionTasks.push_back(ct);
        AATasks.push_back(ct);
        good = true;
      } else delete ct;
    }

    good = false;
    i = 0;
    // build an area shield
    while(!good){
      if(i++ > 100) break;
      setpos(pos,getRadialPoint(position, 75.f));
      setpos(pos,callback->GetMap()->FindClosestBuildSite(shield, pos, 600.f, 3, 0));

      ct = new ConstructionTask(shield, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(constructionTasks,ct)){
        constructionTasks.push_back(ct);
        good = true;
      } else delete ct;
    }
}

void EconomyManager::defendMex(const springai::AIFloat3& position){
    springai::UnitDef* turret = callback->GetUnitDefByName("turretlaser");
    if(shipsViable && kgb_rand() > 0.5){
      turret = callback->GetUnitDefByName("turrettorp");
    }

    for(springai::Unit* u : porcs){
      float dist = distance(position, u->GetPos());
      if(dist < 350){
        return;
      }
    }

    for(ConstructionTask* c : porcTasks){
      float dist = distance(position, c->getPos());
      if(dist < 350){
        return;
      }
    }

    springai::AIFloat3 pos(getDirectionalPoint(graphManager->getAllyBase(), position, distance(graphManager->getAllyBase(), position) + 100.f));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(turret, pos, 600.f, 3, 0));

    ConstructionTask* ct = new ConstructionTask(turret, pos, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::fortifyMex(const springai::AIFloat3& position, springai::WeaponDef* wd){
    springai::UnitDef* porc;
    ConstructionTask* ct;
    springai::AIFloat3 pos;

    if(adjustedIncome > 30.f){
      if(wd == nullptr){
        porc = callback->GetUnitDefByName("turretemp");
      } else if(wd->GetRange() > 620.f){ // 74145: TODO: move these ranges to config.
        porc = callback->GetUnitDefByName("turretgauss");
      } else if(wd->GetRange() > 410.f){
        porc = callback->GetUnitDefByName("turretheavylaser");
      } else if(shipsViable){
        porc = callback->GetUnitDefByName("turrettorp");
      } else {
        porc = callback->GetUnitDefByName("turretriot");
      }
    } else {
      if(shipsViable && kgb_rand() > 0.5)
        porc = callback->GetUnitDefByName("turrettorp");
      else
        porc = callback->GetUnitDefByName("turretlaser");

      if(adjustedIncome > 20.f){
        setpos(pos,getAngularPoint(position, graphManager->getEnemyCenter(), 150.f));
        setpos(pos,callback->GetMap()->FindClosestBuildSite(porc, pos, 600.f, 3, 0));

        ct = new ConstructionTask(porc, pos, 0);
        if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
          constructionTasks.push_back(ct);
          porcTasks.push_back(ct);
        } else delete ct;
      }
    }


    setpos(pos,getAngularPoint(position, graphManager->getEnemyCenter(), 150.f));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(porc, pos, 600.f, 3, 0));

    ct = new ConstructionTask(porc, pos, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::defendNano(Nanotower& n){
    if(n.target == nullptr || n.target->GetHealth() <= 0 || n.target->GetTeam() != ai->teamID) return;
    springai::UnitDef* turret = callback->GetUnitDefByName("turretlaser");
    if(shipsViable && kgb_rand() > 0.5) turret = callback->GetUnitDefByName("turrettorp");

    springai:: AIFloat3 pos(getDirectionalPoint(n.target->GetPos(), n.unit->GetPos(), distance(n.target->GetPos(), n.unit->GetPos()) + 100.f));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(turret, pos, 600.f, 3, 0));

    ConstructionTask* ct = new ConstructionTask(turret, pos, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::defendFusion(const springai::AIFloat3& position, bool factory){
    springai::UnitDef* hlt = callback->GetUnitDefByName("turretheavylaser");
    springai::UnitDef* emp = callback->GetUnitDefByName("turretemp");
    ConstructionTask* ct;
    // build an hlt and a faraday
    springai::AIFloat3 pos(getRadialPoint(position, factory ? 175.f : 125.f));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(hlt, pos, 600.f, 3, 0));

    ct = new ConstructionTask(hlt, pos, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;

    setpos(pos,getDirectionalPoint(position, pos, distance(position, pos) + 75.f));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(emp, pos, 600.f, 3, 0));
    ct = new ConstructionTask(emp, pos, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::defendFac(const springai::AIFloat3& position){
    springai::UnitDef* turret = callback->GetUnitDefByName("turretlaser");
    if(shipsViable && kgb_rand() > 0.5) turret = callback->GetUnitDefByName("turrettorp");
    springai::AIFloat3 pos(0,0,0);

    if(ai->mergedAllies == 0){
      ConstructionTask* ct;
      bool good = false;
      int i = 0;
      while(!good){
        if(i++ > 10){
          break;
        }
        setpos(pos,getAngularPoint(position, graphManager->getMapCenter(), 250.f));
        setpos(pos,callback->GetMap()->FindClosestBuildSite(turret, pos, 600.f, 3, 0));
        ct = new ConstructionTask(turret, pos, 0);
        if(buildCheck(*ct) && !CONTAINSP(porcTasks, ct)){
          good = true;
          ct->facDef = true;
          constructionTasks.push_back(ct);
          porcTasks.push_back(ct);
        } else delete ct;
      }
    } else {
      setpos(pos,getDirectionalPoint(graphManager->getAllyBase(), position, distance(position, graphManager->getAllyBase())+175.f));
      setpos(pos,getDirectionalPoint(pos, graphManager->getMapCenter(), 75.f));
      setpos(pos,callback->GetMap()->FindClosestBuildSite(turret, pos, 600.f, 3, 0));
      ConstructionTask* ct = new ConstructionTask(turret, pos, 0);
      if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
        constructionTasks.push_back(ct);
        porcTasks.push_back(ct);
      } else delete ct;
    }
}

void EconomyManager::defendMexes(){
    for(MetalSpot* ms : graphManager->getOwnedSpots()){
      defendMex(ms->getPos());
    }
}

void EconomyManager::defendLinks(){
    if(ai->mergedAllies == 0){
      for(Link* l : graphManager->getLinks()){
        if(l->length <= 550 && l->isOwned()){
          defendLink(*l);
        }
      }
    } else {
      for(Link* l : graphManager->getLinks()){
        if(l->length < 800 && l->isOwned() && graphManager->isFrontLine(l->getPos())){
          defendLink(*l);
        }
      }
    }
}

void EconomyManager::defendLink(Link& l){
    springai::AIFloat3 position(l.getPos());
    springai::UnitDef* llt = callback->GetUnitDefByName("turretlaser");

    // don't spam redundant porc
    float porcdist = 300;
    for(springai::Unit* u : porcs){
      float dist = distance(position,u->GetPos());
      if(dist < porcdist){
        return;
      }
    }

    for(ConstructionTask* c : porcTasks){
      float dist = distance(position, c->getPos());
      if(dist < porcdist){
        return;
      }
    }

    ConstructionTask* ct;
    // build an llt
    springai::AIFloat3 pos(callback->GetMap()->FindClosestBuildSite(llt, position, 600.f, 3, 0));

    ct = new ConstructionTask(llt, pos, 0);
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::porcPush(const springai::AIFloat3& position){
    springai::UnitDef* porc;
    double rnd = kgb_rand();
    if(rnd > 0.15 * std::min(graphManager->territoryFraction * 2.f, 1.f)){
      porc = callback->GetUnitDefByName("turretmissile");
    } else {
      rnd = kgb_rand();
      porc = rnd > 0.35 ? callback->GetUnitDefByName("turretheavylaser") : callback->GetUnitDefByName("turretemp");
    }

    if(enemyHasAir && kgb_rand() > 0.95){
      porc = callback->GetUnitDefByName("turretaalaser");
    }
    Enemy* enemy = warManager->getClosestEnemyPorc(position);
    if(enemy == nullptr){
      return;
    }

    springai::AIFloat3 pos(getAngularPoint(enemy->position, position, enemy->ud->GetMaxWeaponRange() + 100.f));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(porc,pos,600.f, 3, 0));

    float porcdist = 150.f + (150.f * (1.f - std::min(graphManager->territoryFraction * 2.f, 1.f)));

    for(springai::Unit* u : porcs){
      float dist = distance(pos, u->GetPos());
      if(dist < porcdist){
        return;
      }
    }

    for(ConstructionTask* c : porcTasks){
      float dist = distance(pos, c->getPos());
      if(dist < porcdist){
        return;
      }
    }

    ConstructionTask* ct = new ConstructionTask(porc, pos, 0);
    ct->frameIssued = frame;
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::defendFront(const springai::AIFloat3& position){
    springai::UnitDef* porc;
    double rnd = kgb_rand();
    if(rnd > 0.6){
      porc = callback->GetUnitDefByName("turretmissile");
    } else if(rnd > 0.25 * std::min(graphManager->territoryFraction * 2.f, 1.f)){
      porc = callback->GetUnitDefByName("turretlaser");
    } else {
      rnd = kgb_rand();
      porc = rnd > 0.75 ? callback->GetUnitDefByName("turretheavylaser") : rnd > 0.5 ? callback->GetUnitDefByName("turretgauss") : rnd > 0.25 ? callback->GetUnitDefByName("turretriot") : callback->GetUnitDefByName("turretemp");
    }

    MetalSpot* ms = graphManager->getClosestFrontLineSpot(position);
    if(ms == nullptr){
      return;
    }

    springai::AIFloat3 pos(ms->getPos());
    springai::AIFloat3 fpos(graphManager->getClosestBattleFront(ms->getPos()));

    setpos(pos,getAngularPoint(pos, fpos, 150.f + ((float) kgb_rand() * 350.f)));
    setpos(pos,callback->GetMap()->FindClosestBuildSite(porc, pos,600.f, 3, 0));

    float porcdist = 200.f + (150.f * (1.f - std::min(graphManager->territoryFraction * 2.f, 1.f)));

    for(springai::Unit* u : porcs){
      float dist = distance(pos, u->GetPos());
      if(dist < porcdist){
        return;
      }
    }

    for(ConstructionTask* c : porcTasks){
      float dist = distance(pos, c->getPos());
      if(dist < porcdist){
        return;
      }
    }

    ConstructionTask* ct = new ConstructionTask(porc, pos, 0);
    ct->frameIssued = frame;
    if(buildCheck(*ct) && !CONTAINSP(porcTasks,ct)){
      constructionTasks.push_back(ct);
      porcTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::createRadarTask(Worker& worker){
    springai::UnitDef* radar = callback->GetUnitDefByName("staticradar");
    springai::AIFloat3 position(worker.getPos());
    setpos(position,heightMap->getHighestPointInRadius(position, 800.f));
    setpos(position,callback->GetMap()->FindClosestBuildSite(radar,position,600.f, 3, 0));
    if(!needRadar(position) || !worker.canReach(position)){
      setpos(position,worker.getPos());
      setpos(position,heightMap->getHighestPointInRadius(position, 500.f));
      setpos(position,callback->GetMap()->FindClosestBuildSite(radar,position,600.f, 3, 0));
      if(!needRadar(position) || !worker.canReach(position)) return;
    }

    ConstructionTask* ct = new ConstructionTask(radar, position, 0);
    if(buildCheck(*ct) && !CONTAINSP(radarTasks,ct)){
      constructionTasks.push_back(ct);
      radarTasks.push_back(ct);
    } else delete ct;
}

bool EconomyManager::needRadar(const springai::AIFloat3& position){
    for(springai::Unit* r : radars){
      if(distance(r->GetPos(),position) <= 1250.f) return false;
    }
    for(ConstructionTask* r : radarTasks){
      if(distance(r->getPos(),position) <= 1250.f) return false;
    }
    return true;
}

void EconomyManager::captureMexes(){
    springai::UnitDef* mex = callback->GetUnitDefByName("staticmex");
    std::vector<MetalSpot*> metalSpots = graphManager->getNeutralSpots();

    for(MetalSpot* ms : metalSpots){
      springai::AIFloat3 position(ms->getPos());
      if(callback->GetMap()->IsPossibleToBuildAt(mex, position, 0) && warManager->getPorcThreat(position) == 0){
        ConstructionTask* ct = new ConstructionTask(mex, position, 0);
        if(!CONTAINSP(constructionTasks,ct)){
          constructionTasks.push_back(ct);
        } else delete ct;
      }
    }
}

void EconomyManager::morphComs(){
    // 74145: TODO: configurable?
    // 74145: TODO: or just find something "smart"?
    for(Worker* w : commanders){
      springai::Unit* u = w->getUnit();
      float level = u->GetRulesParamFloat("comm_level", 0.f);
      float chassis = u->GetRulesParamFloat("comm_chassis", 0.f);
      float oldModCount = u->GetRulesParamFloat("comm_module_count", 0.f);
      float newModCount = 1.0f; // 74145: TODO: moar
      std::vector<float> params;
      params.push_back(level);
      params.push_back(chassis);
      params.push_back(oldModCount);
      params.push_back(newModCount);

      for(int i = 1; i <= oldModCount; i++){
        float modID = u->GetRulesParamFloat("comm_module_" + i, -1.f);
        if(modID != -1.f){
          params.push_back(modID);
        }
      }


      params.push_back(13.0f); // shotgun

      //params.push_back(30.0f); // field radar // 74145: TODO: uhh, no that is free now.
      u->ExecuteCustomCommand(CMD_MORPH_UPGRADE_INTERNAL, params, 0);
      params.clear();

      params.push_back(3.0f);
      u->ExecuteCustomCommand(CMD_MISC_PRIORITY, params, 0);
    }
    morphedComs = true;
}

void EconomyManager::morphSparrow(){
    if(!facManager->factories.empty() && !radars.empty()){
      // find the most useless radar and turn it into a sparrow
      springai::Unit* best = nullptr;
      float bestdist = 65535;
      for(springai::Unit* r : radars){
        if(r->GetHealth() <= 0 || r->GetTeam() != ai->teamID) continue;
        Worker* f = getNearestFac(r->GetPos());
        if(f == nullptr) continue;
        float tmpdist = distance(f->getPos(), r->GetPos());
        if(tmpdist < bestdist){
          bestdist = tmpdist;
          best = r;
        }
      }
      if(best != nullptr){
        sparrow = best;
        ERASEUNIT(radars,sparrow);
        std::vector<float> params;
        params.push_back((float) callback->GetUnitDefByName("planelightscout")->GetUnitDefId());
        sparrow->ExecuteCustomCommand(CMD_MORPH, params, 0);
        params.clear();
        params.push_back(3.f);
        sparrow->ExecuteCustomCommand(CMD_MISC_PRIORITY, params, 0); // low prio, cause sparrows are freaking expensive.
      }
    }
}

void EconomyManager::createEnergyTask(Worker& worker){
    springai::UnitDef* solar = callback->GetUnitDefByName("energysolar");
    springai::UnitDef* windgen = callback->GetUnitDefByName("energywind");
    springai::AIFloat3 position(worker.getPos());

    ConstructionTask* ct;
    if(position == nullpos){
      return;
    }

    // for solars
    if(((solars.size() + solarTasks.size()) < std::round(rawMexIncome/2.f) || kgb_rand() > 0.25)
        && callback->GetMap()->GetElevationAt(position.x, position.z) > 0){
      setpos(position,graphManager->getOverdriveSweetSpot(position, "energysolar"));
      // in case all metal spots are connected
      if(position == nullpos){
        return;
      }
      setpos(position,callback->GetMap()->FindClosestBuildSite(solar, position, 600.f, 3, 0));

      float solarDist = 400;

      // prevent ecomanager from spamming solars/windgens that graphmanager doesn't know about yet
      for(ConstructionTask* s : solarTasks){
        float dist = distance(s->getPos(), position);
        if(dist < solarDist && s->target == nullptr){
          return;
        }
      }

      // prevent ecomanager from spamming solars/windgens that graphmanager doesn't know about yet
      for(ConstructionTask* s: windTasks){
        float dist = distance(s->getPos(), position);
        if(dist < solarDist && s->target == nullptr){
          return;
        }
      }

      ct = new ConstructionTask(solar, position, 0);
      if(buildCheck(*ct) && !CONTAINSP(solarTasks,ct)){
        constructionTasks.push_back(ct);
        solarTasks.push_back(ct);
      } else delete ct;
    } else {
      // for windgens
      setpos(position,graphManager->getOverdriveSweetSpot(position, "energywind"));
      // in case all metal spots are connected
      if(position == nullpos){
        return;
      }
      setpos(position,callback->GetMap()->FindClosestBuildSite(windgen, position, 600.f, 3, 0));

      float solarDist = 400;

      // prevent ecomanager from spamming solars/windgens that graphmanager doesn't know about yet
      for(ConstructionTask* s : solarTasks){
        float dist = distance(s->getPos(), position);
        if(dist < solarDist && s->target == nullptr){
          return;
        }
      }
      for(ConstructionTask* s : windTasks){
        float dist = distance(s->getPos(), position);
        if(dist < solarDist && s->target == nullptr){
          return;
        }
      }

      ct = new ConstructionTask(windgen, position, 0);
      if(buildCheck(*ct) && !CONTAINSP(windTasks,ct)){
        constructionTasks.push_back(ct);
        windTasks.push_back(ct);
      } else delete ct;
    }
}

void EconomyManager::createFusionTask(Worker& worker){
    springai::UnitDef* fusion = callback->GetUnitDefByName("energyfusion");
    springai::UnitDef* singu = callback->GetUnitDefByName("energysingu");
    if(facManager->factories.empty()) return;
    springai::AIFloat3 position;
    ConstructionTask* ct;
    int started = 0;
    for(ConstructionTask* ft : fusionTasks){
      if(ft->target != nullptr){
        started++;
      }
    }

    // for fusions
    if(effectiveIncome > 35.f && graphManager->eminentTerritory
        && std::floor(staticIncome/27.5f) > fusions.size() && !facManager->factories.empty()
        && (int)fusionTasks.size() + (int)fusions.size() - started < 3 && (int)fusionTasks.size() < 1 + ai->mergedAllies){
      setpos(position,getAngularPoint(graphManager->getAllyCenter(), graphManager->getAllyBase(), distance(graphManager->getAllyCenter(), graphManager->getAllyBase())));
      setpos(position,graphManager->getOverdriveSweetSpot(position, "energyfusion"));
      if(position == nullpos){
        return;
      }
      setpos(position,callback->GetMap()->FindClosestBuildSite(fusion, position, 600.f, 3, 0));
      ct = new ConstructionTask(fusion, position, 0);
      if(buildCheck(*ct)){
        constructionTasks.push_back(ct);
        fusionTasks.push_back(ct);
      } else delete ct;
    }
    // for singus
    else if(fusions.size() > 2 && fusions.size() < 5 + (ai->mergedAllies > 1 ? 1 : 0)
        && graphManager->eminentTerritory
        && !facManager->factories.empty() && fusionTasks.empty()){
      setpos(position,getAngularPoint(graphManager->getAllyCenter(), graphManager->getAllyBase(), distance(graphManager->getAllyCenter(), graphManager->getAllyBase())));
      setpos(position,graphManager->getOverdriveSweetSpot(position, "energysingu"));
      if(position == nullpos){
        return;
      }
      setpos(position,callback->GetMap()->FindClosestBuildSite(singu, position, 600.f, 3, 0));
      ct = new ConstructionTask(singu, position, 0);
      if(buildCheck(*ct)){
        constructionTasks.push_back(ct);
        fusionTasks.push_back(ct);
      } else delete ct;
    }
}

void EconomyManager::createGridTask(const springai::AIFloat3& position){
    ConstructionTask* ct;
    springai::UnitDef* pylon = callback->GetUnitDefByName("energypylon");

    float fdist = 65535;
    springai::AIFloat3 best(position);
    for(springai::Unit* f : fusions){
      float tmpdist = distance(f->GetPos(), position);
      if(tmpdist < fdist)
      { fdist=tmpdist; setpos(best,f->GetPos()); }
    }

    setpos(best,graphManager->getOverdriveSweetSpot(best, "energypylon"));
    if(best == nullpos) return;
    setpos(best,callback->GetMap()->FindClosestBuildSite(pylon,best,600.f, 3, 0));
    // don't build pylons in the middle of nowhere
    if(getFusionDist(best) > 1500) return;

    // check the build site for existing pylons, since getOverdriveSweetSpot may cluster them.
    float gdist = 65535;
    for(springai::Unit* u : pylons){
      float dist = distance(best,u->GetPos());
      if(dist<gdist){
        gdist = dist;
      }
    }

    for(ConstructionTask* c : pylonTasks){
      float dist = distance(best,c->getPos());
      if(dist<gdist){
        gdist = dist;
      }
    }
    if(gdist <= 750) return; // Already have one

    ct = new ConstructionTask(pylon, best, 0);
    if(buildCheck(*ct) && !CONTAINSP(pylonTasks,ct)){
      constructionTasks.push_back(ct);
      pylonTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::createNanoTurretTask(Worker& w, springai::Unit* target){
    springai::UnitDef* nano = callback->GetUnitDefByName("staticcon");
    springai::AIFloat3 position;

    float buildDist;
    float radius;
    std::string defName(target->GetDef()->GetName());
    if(defName == "striderhub" || defName.find("factory") != std::string::npos){
      radius = 400.f;
      buildDist = 350.f;
    } else {
      radius = 350.f; // for superweapons
      buildDist = 300.f;
    }

    int i = 0;
    while(true){
      setpos(position,getRadialPoint(target->GetPos(), buildDist));
      setpos(position,callback->GetMap()->FindClosestBuildSite(nano, position, 600.f, 3, 0));

      if(distance(position, target->GetPos()) < radius){
        ConstructionTask* ct = new ConstructionTask(nano, position, 0);
        ct->ctTarget = target;
        if(buildCheck(*ct) && !CONTAINSP(nanoTasks,ct) && w.canReach(position)){
          constructionTasks.push_back(ct);
          nanoTasks.push_back(ct);
          return;
        }
        delete ct;
      }
      i++;
      if(i >= 8) buildDist -= 50.f;
      if(i > 10) return;
    }
}

void EconomyManager::createStorageTask(Worker& w, springai::Unit* target){
    springai::UnitDef* stor = callback->GetUnitDefByName("staticstorage");
    springai::AIFloat3 position;
    const float buildDist = 400.f;

    bool good = false;
    int i = 0;
    while(!good){
      setpos(position,getRadialPoint(target->GetPos(), buildDist));
      setpos(position,callback->GetMap()->FindClosestBuildSite(stor, position, 600.f, 3, 0));
      if(distance(position, target->GetPos()) < 500){
        good = true;
      } else if (i++ > 1000) return;
    }
    ConstructionTask* ct = new ConstructionTask(stor, position, 0);
    if(buildCheck(*ct) && !CONTAINSP(nanoTasks,ct) && w.canReach(position)){
      constructionTasks.push_back(ct);
      storageTasks.push_back(ct);
    } else delete ct;
}

void EconomyManager::createAirPadTask(springai::Unit* target){
    if(std::string(target->GetDef()->GetName()) == "striderhub") return;
    springai::UnitDef* airpad = callback->GetUnitDefByName("staticrearm");
    springai::AIFloat3 position;
    const float buildDist = 400.f;

    setpos(position,getRadialPoint(target->GetPos(), buildDist));
    setpos(position,callback->GetMap()->FindClosestBuildSite(airpad, position, 600.f, 3, 0));

    ConstructionTask* ct =  new ConstructionTask(airpad, position, 0);
    if(buildCheck(*ct) && !CONTAINSP(airpadTasks,ct)){
      constructionTasks.push_back(ct);
      airpadTasks.push_back(ct);
    } else delete ct;
}


springai::Unit* EconomyManager::getCaretakerTarget(){
    springai::Unit* target = nullptr;
    float ctCount = 9001;
    for(auto & [k, f] : facManager->factories){
      // Spread caretakers evenly between facs, with some priority.
      float numCT = 0;
      for(auto & [j, n] : nanos){
        if(n->target != nullptr && n->target->GetHealth() > 0.f && n->target->GetUnitId() == f->id){
          numCT++;
        }
      }
      for(ConstructionTask* c : nanoTasks){
        if(c->ctTarget != nullptr && c->ctTarget->GetHealth() > 0.f && c->ctTarget->GetUnitId() == f->id){
          numCT++;
        }
      }
      std::string defName(f->getUnit()->GetDef()->GetName());
      if(defName == "striderhub"){
        numCT += ai->mergedAllies > 0 ? -0.3f : 0.1f;
      } else if(defName == "factoryplane" || defName == "factorygunship"){
        numCT += ai->mergedAllies > 0 ? -0.2f : 0.1f;
      } else if(defName == "factoryshield"){
        numCT += ai->mergedAllies > 0 ? -0.1f : 0.f;
      }
      if(numCT < ctCount){
        target = f->getUnit();
        ctCount = numCT;
      }
    }

    for(ConstructionTask* c : factoryTasks){
      if(c->target != nullptr && c->target->GetHealth() > 0){
        float numCT = 0;
        for( auto & [k, n] : nanos){
          if(n->target != nullptr && n->target->GetHealth() > 0.f && n->target->GetUnitId() == c->target->GetUnitId()){
            numCT++;
          }
        }
        for(ConstructionTask* nt : nanoTasks){
          if(nt->ctTarget != nullptr && nt->ctTarget->GetHealth() > 0.f && nt->ctTarget->GetUnitId() == c->target->GetUnitId()){
            numCT++;
          }
        }
        std::string defName(c->target->GetDef()->GetName());
        if(defName == "striderhub"){
          numCT -= 0.9f;
        }
        if(defName == "factoryplane" || defName == "factorygunship" || defName == "factoryshield"){
          numCT -= 0.5f;
        }
        if(numCT < ctCount){
          target = c->target;
          ctCount = numCT;
        }
      }
    }
    return target;
}

void EconomyManager::collectReclaimables(Worker& w){
    if((int)reclaimTasks.size() > std::max(facManager->numWorkers, 20)) return;
    std::vector<springai::Feature*> feats = callback->GetFeaturesIn(w.getPos(), ai->mapDiag * 2.5f, true);

    // Sort by closest first.
    std::sort(feats.begin(), feats.end(),
        [w] (springai::Feature* lhs, springai::Feature* rhs){
      float dist1 = distance(w.getPos(), lhs->GetPosition());
      float dist2 = distance(w.getPos(), rhs->GetPosition());
      return dist1 < dist2;
    });

    for(springai::Feature* f : feats){
      if(!f->GetDef()->IsReclaimable() || warManager->getPorcThreat(f->GetPosition()) > 0) continue;
      if(f->GetDef()->GetContainedResource(m) > f->GetDef()->GetContainedResource(e)){
        if((int)reclaimTasks.size() > std::max(facManager->numWorkers, 20)){
          break;
        }
        ReclaimTask* rt = new ReclaimTask(f, f->GetDef()->GetContainedResource(m));
        if(!CONTAINSP(reclaimTasks,rt) && w.canReach(f->GetPosition())){
          reclaimTasks.push_back(rt);
          break;
        } delete rt;
      } else {
        if((int)energyReclaimTasks.size() > std::max(facManager->numWorkers, 20)){
          break;
        }
        ReclaimTask* rt = new ReclaimTask(f, f->GetDef()->GetContainedResource(e));
        if(!CONTAINSP(energyReclaimTasks,rt) && w.canReach(f->GetPosition())){
          energyReclaimTasks.push_back(rt);
          break;
        } delete rt;
      }
    }
}

float EconomyManager::getReclaimValue(){
    if(lastRCValueFrame == 0 || frame - lastRCValueFrame > 300){
      reclaimValue = 0;
      lastRCValueFrame = frame;
      std::vector<springai::Feature*> feats = callback->GetFeatures();
      for(springai::Feature* f : feats) {
        reclaimValue += f->GetDef()->GetContainedResource(m);
      }
    }
    reclaimValue *= (1.f + ai->mergedAllies)/(1.f + ai->allies.size());
    return reclaimValue;
}

Worker* EconomyManager::getNearestFac(const springai::AIFloat3& position){
    Worker* nearestFac = nullptr;
    float dist = 0;
    for(auto& [k,f] : facManager->factories){
      float tdist = distance(position, f->getPos());
      if(nearestFac == nullptr || tdist < dist){
        dist = tdist;
        nearestFac = f;
      }
    }
    return nearestFac;
}

Worker* EconomyManager::getFarthestFac(const springai::AIFloat3& position){
    Worker* farthestFac = nullptr;
    float dist = 0;
    for(auto& [k,f] : facManager->factories){
      float tdist = distance(position, f->getPos());
      if(farthestFac == nullptr || tdist > dist){
        dist = tdist;
        farthestFac = f;
      }
    }
    return farthestFac;
}

float EconomyManager::getFusionDist(const springai::AIFloat3& position){
    float dist = 65535;
    for(springai::Unit* f : fusions){
      float tdist = distance(position, f->GetPos());
      if(tdist < dist){
        dist = tdist;
      }
    }
    return dist;
}

}
