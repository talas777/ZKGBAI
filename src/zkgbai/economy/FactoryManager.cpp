#include "FactoryManager.h"

#include <algorithm> // std::find (CONTAINSI / CONTAINSS)

#include "../ZKGBAI.h"

namespace zkgbai {

FactoryManager::FactoryManager(ZKGBAI* ai){
    this->ai = ai;
    this->callback = ai->getCallback();

    this->bigMap = ai->mapDiag > 1080.f;
    this->earlyWorker = bigMap;
    this->m = callback->GetResourceByName("Metal");
}

FactoryManager::~FactoryManager(){
    for(auto &[k,v] : factories) delete v;
}

int FactoryManager::init(int AIID, springai::OOAICallback* cb){
    this->warManager = ai->warManager;
    this->economyManager = ai->ecoManager;
    this->graphManager = ai->graphManager;
    this->unitTypes = UnitClasses::getInstance();
    return 0;
}

int FactoryManager::update(int f){
    this->frame = f;
    if(frame % 60 == ai->offset % 60 && striderHub != nullptr){
      if(striderTarget == nullptr){
        assignFactoryTask(*striderHub);
      } else {
        striderHub->getUnit()->Repair(striderTarget, 0);
      }
    }
    return 0;
}

int FactoryManager::unitCreated(springai::Unit* unit, springai::Unit* builder){
    springai::UnitDef* def = unit->GetDef();
    std::string defName(def->GetName());

    if(defName.find("hub") != std::string::npos){
      unit->SetMoveState(0, 0);
    }

    if(defName.find("factory") != std::string::npos){
      // Set default rally point.
      int facing = unit->GetBuildingFacing();
      springai::AIFloat3 target = unit->GetPos();
      float rallydist = 175.f;
      switch (facing){
        case 0:
          target.z += rallydist;
          break;
        case 1:
          target.x += rallydist;
          break;
        case 2:
          target.z -= rallydist;
          break;
        case 3:
          target.x -= rallydist;
          break;
      }
      unit->MoveTo(target, 0);
    }

    if(def->GetBuildSpeed() > 0 && def->GetSpeed() > 0){
      numWorkers++;
      if(def->GetBuildSpeed() < 8.f){
        workerValue += def->GetCost(m);
      }
      mobileBP += def->GetBuildSpeed();
    } else if(def->GetSpeed() > 0 && !def->IsAbleToFly() && !CONTAINSS(unitTypes->AAs,defName)){
      newUnits.insert(unit->GetUnitId());
      armyValue += def->GetCost(m)/2.f;
    } else if(CONTAINSS(unitTypes->AAs,defName)){
      newUnits.insert(unit->GetUnitId());
      numAAs++;
      AAvalue += def->GetCost(m);
      armyValue += def->GetCost(m)/2.f;
    }

    if(builder != nullptr && factories.find(builder->GetUnitId()) != factories.end()){
      Factory* scoutFac = factories[builder->GetUnitId()];
      if(scoutFac->creatingScout){
        unit->SetMoveState(1, 0);
        warManager->raiderHandler->addSoloRaider({ai, unit, def->GetCost(m)});
      }
    }

    if(defName == "energyfusion" || defName == "energysingu"){
      fighterValue += 250.f; // 74145: TODO: WHAT?
    }

    if(defName == "tankheavyarty" || defName == "hoverarty" || defName == "spidercrabe" || defName == "amphassault" || defName == "striderarty"){
      artyValue += def->GetCost(m);
    }

    if(defName == "shieldarty"){
      artyValue += def->GetCost(m)/2.f;
    }

    if(defName == "tankarty"){
      artyValue += def->GetCost(m)/2.f;
    }

    if(defName == "vehheavyarty" || defName == "cloaksnipe"){
      heavyArtyValue += def->GetCost(m);
      artyValue += def->GetCost(m);
    }

    if(defName == "veharty" || defName == "cloakarty"){
      artyValue += def->GetCost(m)/2.f;
      lightArtyValue += def->GetCost(m)/2.f;
    }

    if(defName == "hoverassault" || defName == "cloakassault" || defName == "vehassault"
        || defName == "amphfloater" || defName == "shieldassault" || defName == "tankassault"){
      assaultValue += def->GetCost(m);
    }

    if(defName == "cloakriot") numWarriors++;
    if(defName == "cloakassault") numZeus++;
    if(defName == "cloakskirm") numRockos++;
    if(defName == "cloakraid") numGlaives++;
    if(defName == "cloakheavyraid") numScythes++;
    if(defName == "shieldraid") numBandits++;
    if(defName == "shieldfelon") numFelons++;
    if(defName == "shieldskirm") numRogues++;
    if(defName == "shieldassault") numThugs++;
    if(defName == "shieldriot") numLaws++;
    if(defName == "shieldarty") numRackets++;
    if(defName == "striderfunnelweb") numFunnels++;
    if(defName == "vehcapture") numDomis++;
    if(defName == "spideremp") numVenoms++;
    if(defName == "spiderriot") numRedbacks++;
    if(defName == "spiderskirm") numRecluses++;
    if(defName == "spiderassault") numHermits++;
    if(defName == "hoverriot") numMaces++;
    if(defName == "hoverskirm") numScalpels++;
    if(defName == "tankassault") numReapers++;
    if(defName == "tankriot") numBanishers++;
    if(defName == "vehscout" || defName == "hoverraid") numScouts++;
    if(defName == "tankraid") numKodachis++;
    if(defName == "vehassault") numRavagers++;
    if(defName == "vehriot") numLevelers++;
    if(defName == "bomberprec" || defName == "bomberriot") numRavens++; // 74145: TODO: split raven and phoenix
    if(defName == "spiderantiheavy") numInfis++;
    if(defName == "cloakjammer") numErasers++;
    if(defName == "shieldshield") numAspis++;
    if(defName == "striderdante" || defName == "striderscorpion" || defName == "striderarty" || defName == "striderfunnelweb"){
      striderTarget = unit;
      numStriders++;
    }

    if(defName == "striderantiheavy"){
      striderTarget = unit;
      numUltis++;
    }

    if(defName == "striderbantha" || defName == "striderdetriment"){
      striderTarget = unit;
      numHeavyStriders++;
    }

    return 0;
}

int FactoryManager::unitFinished(springai::Unit* unit){
    if(striderTarget != nullptr && striderTarget->GetUnitId() == unit->GetUnitId()){
      striderTarget = nullptr;
    }

    std::string defName(unit->GetDef()->GetName());
    if(newUnits.find(unit->GetUnitId()) != newUnits.end()){
      newUnits.erase(unit->GetUnitId());
      if(!CONTAINSS(unitTypes->AAs,defName)){
        fighterValue += unit->GetDef()->GetCost(m);
      }
      armyValue += unit->GetDef()->GetCost(m)/2.f;
    }

    if(defName.find("factory") != std::string::npos || defName.find("hub") != std::string::npos){
      Factory* fac;
      if((int)factories.size() < 1 + ai->mergedAllies){
        fac = new Factory(ai, unit, true);
      } else {
        fac = new Factory(ai, unit, false);
      }
      if(defName.find("hub") == std::string::npos){
        for(springai::UnitDef* ud : unit->GetDef()->GetBuildOptions()){
          if(!ud->GetBuildOptions().empty()){
            fac->costPerBP = ud->GetCost(m)/ud->GetBuildSpeed();
            fac->workerBP = ud->GetBuildSpeed();
            break;
          }
        }
      }
      factories[fac->id] = fac;

      assignFactoryTask(*fac);
      unit->SetMoveState(2, 0);

      if(defName == "striderhub"){
        striderHub = fac;
      }
    }

    return 0;
}

int FactoryManager::unitDestroyed(springai::Unit* unit, springai::Unit* attacker){
    if(striderTarget != nullptr && striderTarget->GetUnitId() == unit->GetUnitId()){
      striderTarget = nullptr;
    }

    springai::UnitDef* def = unit->GetDef();
    std::string defName(def->GetName());

    if(defName == "striderhub"){
      striderHub = nullptr;
    }

    if(def->IsBuilder() && def->GetSpeed() > 0){
      numWorkers--;
      if(def->GetBuildSpeed() < 8.f){
        workerValue -= def->GetCost(m);
      }
      mobileBP -= def->GetBuildSpeed();
    } else if(newUnits.find(unit->GetUnitId()) != newUnits.end()){
      newUnits.erase(unit->GetUnitId());
      armyValue -= def->GetCost(m)/2.f;
    } else if(def->GetSpeed() > 0 && !def->IsAbleToFly() && !CONTAINSS(unitTypes->AAs,defName)){
      fighterValue -= def->GetCost(m);
      armyValue -= def->GetCost(m);
    } else if(CONTAINSS(unitTypes->AAs,defName)){
      numAAs--;
      AAvalue -= def->GetCost(m);
      armyValue -= def->GetCost(m);
    }

    if(defName == "energyfusion" || defName == "energysingu"){
      fighterValue -= 250.f;
    }

    if(defName == "tankheavyarty" || defName == "hoverarty" || defName == "spidercrabe" || defName == "amphassault" || defName == "striderarty"){
      artyValue -= def->GetCost(m);
    }

    if(defName == "tankarty"){
      artyValue -= def->GetCost(m)/2.f;
    }

    if(defName == "vehheavyarty" || defName == "cloaksnipe"){
      heavyArtyValue -= def->GetCost(m);
      artyValue -= def->GetCost(m);
    }

    if(defName == "cloakarty" || defName == "veharty"){
      artyValue -= def->GetCost(m)/2.f;
      lightArtyValue -= def->GetCost(m)/2.f;
    }

    if(defName == "hoverassault" || defName == "cloakassault" || defName == "vehassault"
        || defName == "amphfloater" || defName == "shieldassault" || defName == "tankassault"){
      assaultValue -= def->GetCost(m);
    }

    if(defName == "cloakjammer") numErasers--;
    if(defName == "shieldshield") numAspis--;
    if(defName == "cloakriot") numWarriors--;
    if(defName == "cloakassault") numZeus--;
    if(defName == "cloakskirm") numRockos--;
    if(defName == "shieldraid") numBandits--;
    if(defName == "shieldfelon") numFelons--;
    if(defName == "shieldskirm") numRogues--;
    if(defName == "shieldassault") numThugs--;
    if(defName == "shieldriot") numLaws--;
    if(defName == "shieldarty") numRackets--;
    if(defName == "striderfunnelweb") numFunnels--;
    if(defName == "vehcapture") numDomis--;
    if(defName == "spideremp") numVenoms--;
    if(defName == "spiderriot") numRedbacks--;
    if(defName == "spiderskirm") numRecluses--;
    if(defName == "spiderassault") numHermits--;
    if(defName == "hoverriot") numMaces--;
    if(defName == "hoverskirm") numScalpels--;
    if(defName == "vehscout" || defName == "hoverraid") numScouts--;
    if(defName == "tankraid") numKodachis--;
    if(defName == "tankassault") numReapers--;
    if(defName == "tankriot") numBanishers--;
    if(defName == "vehassault") numRavagers--;
    if(defName == "vehriot") numLevelers--;
    if(defName == "bomberprec" || defName == "bomberriot") numRavens--;
    if(defName == "spiderantiheavy") numInfis--;
    if(defName == "striderantiheavy") numUltis--;

    if(defName == "striderdante" || defName == "striderscorpion" || defName == "striderarty" || defName == "striderfunnelweb"){
      numStriders--;
    }

    if(defName == "striderbantha" || defName == "striderdetriment"){
      numHeavyStriders--;
    }

    if(factories.find(unit->GetUnitId()) != factories.end()){
      factories.erase(unit->GetUnitId());
    }
    return 0; // signaling: OK
}

int FactoryManager::unitCaptured(springai::Unit* unit, int oldTeamID, int newTeamID){
    (void)newTeamID;
    if(oldTeamID == ai->teamID){
      return unitDestroyed(unit, nullptr);
    }
    return 0;
}

int FactoryManager::unitGiven(springai::Unit* unit, int oldTeamID, int newTeamID){
    (void)oldTeamID;
    if(newTeamID == ai->teamID){
      if(std::string(unit->GetDef()->GetName()) == "striderhub"){
        unit->SelfDestruct(0);
      } else {
        return unitCreated(unit, nullptr);
      }
    }
    return 0;
}

int FactoryManager::unitIdle(springai::Unit* unit){
    std::string defName(unit->GetDef()->GetName());
    if(factories.find(unit->GetUnitId()) != factories.end() && defName != "striderhub" && defName != "athena"){
      Factory* fac = factories[unit->GetUnitId()];
      assignFactoryTask(*fac);
    }
    return 0; // signaling: OK
}

int FactoryManager::enemyEnterLOS(springai::Unit* unit){
    springai::UnitDef* def = unit->GetDef();
    std::string defName(def->GetName());
    if(def->IsAbleToFly() && defName != "dronelight" && defName != "droneheavyslow" && defName != "dronecarry"){
      enemyHasAir = true; // 74145: TODO: also ignore sparrow (and athena?)
    } else if(defName == "dronelight" || defName == "droneheavyslow"  || defName == "dronecarry"){
      enemyHasDrones = true;
    }

    if(std::string(def->GetTooltip()).find("Amph") != std::string::npos){
      enemyHasAmphs = true;
    }

    if(defName == "turretheavylaser" || defName == "turretriot" || defName == "turretgauss" || defName == "turretheavy" || defName == "turretantiheavy" || defName == "spidercrabe"){
      enemyHasHeavyPorc = true;
    }
    return 0;
}

void FactoryManager::assignFactoryTask(Factory& fac){
    std::string defName(fac.getUnit()->GetDef()->GetName());
    springai::UnitDef* unit;

    if(fac.creatingScout){
      fac.creatingScout = false;
      fac.scoutAllowance--;
    }

    scoutsNeeded = 0;
    for(auto &[k,f] : factories) scoutsNeeded += f->maxScoutAllowance;
    if(defName == "factorytank"){
      scoutsNeeded -= warManager->raiderHandler->kodachis.size();
    } else {
      scoutsNeeded -= warManager->raiderHandler->soloRaiders.size();
    }
    scoutBudget = std::max(0, std::min(scoutsNeeded, fac.scoutAllowance));

    hasFusion = (!economyManager->fusions.empty()
        && (!economyManager->fusions[0]->IsBeingBuilt() || economyManager->fusions[0]->GetHealth() > economyManager->fusions[0]->GetDef()->GetHealth() / 2.f));

    if(defName == "factorycloak"){
      unit = callback->GetUnitDefByName(getCloaky(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == "factoryship"){
      unit = callback->GetUnitDefByName(getBoats(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == "factoryshield"){
      unit = callback->GetUnitDefByName(getShields(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == "factorygunship"){
      unit = callback->GetUnitDefByName(getGunship(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == "factoryplane"){
      unit = callback->GetUnitDefByName(getPlanes(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == "factoryamph"){
      unit = callback->GetUnitDefByName(getAmphs(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == "factoryveh"){
      unit = callback->GetUnitDefByName(getLV(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == ("factoryhover")){
      unit = callback->GetUnitDefByName(getHovers(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == ("factorytank")){
      unit = callback->GetUnitDefByName(getTanks(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == ("factoryspider")){
      unit = callback->GetUnitDefByName(getSpiders(fac).c_str());
      fac.getUnit()->Build(unit, fac.getPos(), 0, 0);
    } else if(defName == ("striderhub")){
      fac.getUnit()->Stop(0);
      unit = callback->GetUnitDefByName(getStrider().c_str());
      springai::AIFloat3 pos = callback->GetMap()->FindClosestBuildSite(unit, fac.getPos(), 250.f, 3, 0);
      fac.getUnit()->Build(unit, pos, 0, 0);
    }
}

bool FactoryManager::needWorkers(Factory& fac){
    std::string facType(fac.getUnit()->GetDef()->GetName());

    if((facType == "factoryplane" || facType == "factorygunship")
        && numWorkers >= 1 + ai->mergedAllies){
      return false;
    }

    if(ai->mergedAllies > 0 && (fac.raiderSpam < 0 || fac.expensiveRaiderSpam < 0)){
      return false;
    }

    float reclaimValue = economyManager->getReclaimValue();

    float income = economyManager->effectiveIncomeMetal + (reclaimValue / ((2.f + 20.f * graphManager->territoryFraction) * fac.costPerBP));
    int workerDeficit = std::round((income - mobileBP)/fac.workerBP);

    if((workerDeficit > 0 && fighterValue > workerValue || numWorkers == 0)
        || (economyManager->adjustedIncome > 15.f && economyManager->metal/economyManager->maxStorage > 0.8f && economyManager->maxStorage > 0.5f && workerDeficit > -1)
        || (earlyWorker && numWorkers < 2 * (1 + ai->mergedAllies)))
    {
      return true;
    } else if(((fac.raiderSpam >= 0 && scoutBudget == 0 && fac.expensiveRaiderSpam >= 0) && numWorkers < 2 * (1 + ai->mergedAllies))){
      return true;
    } else if(ai->mergedAllies == 0 && economyManager->effectiveIncome >= 15.f && numWorkers < 3){
      return true;
    }
    return false;
}

std::string FactoryManager::getCloaky(Factory& fac){
    if(needWorkers(fac)){
      return "cloakcon";
    }

    if(warManager->slasherSpam * 280 > assaultValue){
      return "cloakassault";
    }

    if(fac.raiderSpam < 0 || scoutBudget > 0){
      if(scoutBudget == 0){
        fac.raiderSpam++;
      } else {
        fac.creatingScout = true;
      }
      return "cloakraid";
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      return "cloakheavyraid";
    }

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "cloakaa";
    }

    // 74145: Talas: bumped required fighterValue up from 2000 and suad count up from >1
    if(hasFusion && numErasers == 0 && fighterValue > 9000.f && warManager->squadHandler->squads.size() > 2 && kgb_rand() > 0.75){
      return "cloakjammer";
    }


    if(economyManager->adjustedIncome > 15 && kgb_rand() > 0.95 - (graphManager->territoryFraction/20.f)){
      return "cloakbomb";
    }

    if(hasFusion && kgb_rand() < (graphManager->territoryFraction/4.f) * (1.f - heavyArtyValue/warManager->enemyHeavyPorcValue)){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
      return "cloaksnipe";
    }

    if(economyManager->adjustedIncome > 20
        && kgb_rand() < (graphManager->territoryFraction/5.f) * (1.f - lightArtyValue/warManager->enemyPorcValue)){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
      return "cloakarty";
    }

    if(!highprio && kgb_rand() > (warManager->sniperSightings.empty() ? 0.95 + std::min(0.025f, graphManager->territoryFraction/20.f) : 0.85)){
      fac.raiderSpam -= std::min(12, std::max(6, (int) std::floor(economyManager->adjustedIncome/5.f))) - 1;
      return "cloakraid";
    }
    if(economyManager->adjustedIncome > 15.f && kgb_rand() > 0.925 + std::min(0.05f, graphManager->territoryFraction/10.f)){
      fac.expensiveRaiderSpam -= (int) std::min(4, std::max(2, (int)std::floor(economyManager->adjustedIncome / 10))) - 1;
      return "cloakheavyraid";
    }

    fac.raiderSpam--;
    double rnd = kgb_rand();
    if(!hasFusion){
      if(numRockos < (2 * numWarriors) + (3 * numZeus)){
        return "cloakskirm";
      } else if(rnd > 0.6){
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
        return "cloakassault";
      } else {
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
        return "cloakriot";
      }
    } else {
      if(numRockos < (2 * numWarriors) + (3 * numZeus)){
        return "cloakskirm";
      } else if(rnd > 0.6){
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
        return "cloakassault";
      } else if(rnd > 0.15){
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
        return "cloakriot";
      } else {
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
        return "cloaksnipe";
      }
    }
}

std::string FactoryManager::getBoats(Factory& fac){
    if(needWorkers(fac)){
      return "shipcon";
    }

    if(scoutBudget > 0){
      fac.creatingScout = true;
      return "shiptorpraider";
    }

    if(!fac.smallRaiderSpam){
      fac.smallRaiderSpam = true;
      fac.expensiveRaiderSpam = -5;
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      return "subraider";
    }

    fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "shipaa";
    }

    if(economyManager->adjustedIncome > 25.f && warManager->enemyHeavyPorcValue > 0.f && kgb_rand() < (graphManager->territoryFraction) * (1.f - artyValue/warManager->enemyPorcValue)){
      fac.expensiveRaiderSpam--;
      return "shiparty";
    }

    if(kgb_rand() > 0.9){
      fac.expensiveRaiderSpam -= (int) std::min(8, std::max(2, (int)std::floor(economyManager->adjustedIncome / (bigMap ? 2.5f : 5.f)))) + 1;
    }

    double rnd = kgb_rand();
    if(economyManager->adjustedIncome < 35 && !graphManager->eminentTerritory){
      fac.expensiveRaiderSpam -= 2;
      if(armyValue < 1000.0f || rnd < 0.3){
        fac.expensiveRaiderSpam--;
        return "shipassault";
      }
      if(rnd < 0.2){
        return "shipriot";
      } else {
        return "shipskirm";
      }
    } else {
      fac.expensiveRaiderSpam--;
      if(armyValue < 2000.f || rnd < 0.3){
        fac.expensiveRaiderSpam -= 2;
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
        return "shipassault";
      }
      if(rnd < 0.1){
        return "shipriot";
      } else if(rnd < 0.2){
        fac.expensiveRaiderSpam--;
        return "shiparty";
      } else {
        return "shipskirm";
      }
    }
}

std::string FactoryManager::getShields(Factory& fac){
    if(needWorkers(fac)){
      return "shieldcon";
    }

    if(warManager->slasherSpam * 140 > assaultValue && warManager->slasherSpam * 140 > warManager->enemyPorcValue){
      return "shieldassault";
    }

    if(fac.raiderSpam < 0 || scoutBudget > 0){
      if(scoutBudget == 0){
        fac.raiderSpam++;
      } else {
        fac.creatingScout = true;
      }
      if(fac.smallRaiderSpam && scoutBudget == 0) return "shieldassault";
      return "shieldraid";
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      if(fac.smallRaiderSpam) return "shieldriot";
      return "shieldskirm";
    }

    if(fac.smallRaiderSpam) fac.smallRaiderSpam = false;

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "shieldaa";
    }

    fac.raiderSpam--;
    if(kgb_rand() > graphManager->territoryFraction) fac.raiderSpam--;
    if(kgb_rand() > 0.5) fac.expensiveRaiderSpam -= 2;
    fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);

    if(numFelons > 0 && (std::round(std::log(warManager->enemyHeavyValue)) > numRackets || kgb_rand() < (graphManager->territoryFraction) * (1.f - (numRackets * 350.f)/warManager->enemyHeavyPorcValue))) return "shieldarty";

    if(hasFusion && numFelons > 0 && ((numAspis < numFelons && kgb_rand() > 0.5) || (numAspis >= numFelons && numAspis < 2 * numFelons && kgb_rand() > 0.8))){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
      fac.raiderSpam -= 4;
      fac.expensiveRaiderSpam -= 2;
      return "shieldshield";
    }

    if(economyManager->adjustedIncome > 20.f && numFelons < std::min((int)std::floor(economyManager->adjustedIncome/20.f), 4) && kgb_rand() > 0.5){
      fac.smallRaiderSpam = true;
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
      fac.raiderSpam -= 4;
      fac.expensiveRaiderSpam--;
      return "shieldfelon";
    }

    if(economyManager->adjustedIncome > 20 && kgb_rand() > 0.95 - std::min(0.05f, graphManager->territoryFraction/10.f)){
      return "shieldbomb";
    }

    if(!highprio && kgb_rand() > (warManager->sniperSightings.empty() ? 0.95 + std::min(0.025f, graphManager->territoryFraction/20.f) : 0.85)){
      fac.raiderSpam -= std::min(12, std::max(6, (int) std::floor(economyManager->adjustedIncome/5.f)));
    }


    if(numLaws * 3 > numThugs){
      return "shieldassault";
    } else if(economyManager->adjustedIncome > 20 && numRackets < numLaws){
      return "shieldarty";
    } else {
      return "shieldriot";
    }
}

std::string FactoryManager::getAmphs(Factory& fac){
    if(needWorkers(fac)){
      return "amphcon";
    }

    if(warManager->slasherSpam * 140 > assaultValue){
      return "amphfloater";
    }

    if(fac.raiderSpam < 0 || scoutBudget > 0){
      if(scoutBudget == 0){
        fac.raiderSpam++;
      } else {
        fac.creatingScout = true;
      }
      return "amphraid";
    }

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "amphaa";
    }

    if(economyManager->adjustedIncome > 30.f
        && fighterValue - artyValue > 1500.f && kgb_rand() < (graphManager->territoryFraction/2.f) * (1.f - artyValue/warManager->enemyPorcValue)){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 3);
      return "amphassault";
    }

    if(economyManager->adjustedIncome > 20 && kgb_rand() > 0.9){
      return "amphbomb";
    }

    fac.raiderSpam--;
    if(kgb_rand() > 0.5) fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);

    if(!highprio && kgb_rand() > (warManager->sniperSightings.empty() ? 0.9 + std::min(0.05f, graphManager->territoryFraction/10.f): 0.85)){
      fac.raiderSpam -= std::min(12, std::max(6, (int) std::floor(economyManager->adjustedIncome/5.f)));
    }

    if(economyManager->adjustedIncome > 35.f && kgb_rand() > 0.9 && fighterValue - artyValue > 2000.f){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
      return "amphassault";
    }
    double rnd = kgb_rand();
    if(rnd > 0.3){
      return "amphfloater";
    } else {
      if(kgb_rand() > 0.85) return "amphriot";
      return "amphimpulse";
    }
}

std::string FactoryManager::getLV(Factory& fac){
    if(needWorkers(fac)){
      return "vehcon";
    }

    if(warManager->slasherSpam * 140.f > assaultValue && warManager->slasherSpam * 140.f > warManager->enemyPorcValue){
      return "vehassault";
    }

    if(scoutBudget > 0){
      fac.creatingScout = true;
      return "vehscout";
    }

    if(fac.raiderSpam < 0){
      fac.raiderSpam++;
      return "vehraid";
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      return "vehsupport";
    }

    fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
    if(kgb_rand() > graphManager->territoryFraction) fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);

    if(graphManager->eminentTerritory
        && kgb_rand() < (graphManager->territoryFraction/10.f) * (1.f - heavyArtyValue/warManager->enemyHeavyPorcValue)){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
      return "vehheavyarty";
    }

    if(economyManager->adjustedIncome > 20.f
        && kgb_rand() < std::min(1.f, 2.f * graphManager->territoryFraction) * (1.f - lightArtyValue/warManager->enemyPorcValue)){
      return "veharty";
    }

    if((numAspis > 0 && !warManager->squadHandler->shieldSquads.empty()) && numDomis < 8 && kgb_rand() > 0.5){
      return "vehcapture";
    }

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "vehaa";
    }

    if(kgb_rand() > (warManager->sniperSightings.empty() ? 0.95 + std::min(0.025f, graphManager->territoryFraction/20.f) : 0.85)){
      fac.raiderSpam -= std::min(8.f, std::max(4.f, std::floor(economyManager->adjustedIncome / 4.f))) + 1;
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
    }

    if(kgb_rand() > 0.9 + std::min(0.05f, graphManager->territoryFraction/10.f)){
      fac.expensiveRaiderSpam -= 4 + std::min(2, (int)std::floor(economyManager->adjustedIncome/25.f));
    }

    if(kgb_rand() > (graphManager->territoryFraction + (graphManager->territoryFraction * graphManager->territoryFraction))/2.f) fac.raiderSpam--;
    double rnd = kgb_rand();
    if(!graphManager->eminentTerritory){
      if(numRavagers > numLevelers * 3 || numLevelers == 0){
        return "vehriot";
      }
      return "vehassault";
    } else {
      if(rnd > 0.2){
        if(numRavagers > numLevelers * 3 || numLevelers == 0){
          return "vehriot";
        }
        return "vehassault";
      } else {
        return "veharty";
      }
    }
}

std::string FactoryManager::getHovers(Factory& fac){
    if(needWorkers(fac)){
      return "hovercon";
    }

    if(warManager->slasherSpam * 140 > assaultValue && warManager->slasherSpam * 140 > warManager->enemyPorcValue){
      return "hoverassault";
    }

    if(fac.raiderSpam < 0 || scoutBudget > 0){
      if(scoutBudget == 0){
        fac.raiderSpam++;
      } else {
        fac.creatingScout = true;
      }
      return "hoverraid";
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      return "hoverassault";
    }

    if(economyManager->adjustedIncome > 30.f
        && armyValue - artyValue > 2000.f && kgb_rand() < graphManager->territoryFraction * (1.f - artyValue/warManager->enemyPorcValue)){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
      return "hoverarty";
    }

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "hoveraa";
    }

    if(!highprio && kgb_rand() > (warManager->sniperSightings.empty() ? 0.9 + std::min(0.05f, graphManager->territoryFraction/10.f) : 0.85)){
      fac.raiderSpam -= std::min(8, std::max(4, (int) std::floor(economyManager->adjustedIncome/5.f)));
    }

    if(kgb_rand() < graphManager->territoryFraction){
      fac.expensiveRaiderSpam--;
    } else {
      fac.raiderSpam--;
    }
    if(kgb_rand() > 0.8){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
      double rnd = kgb_rand();
      if(economyManager->shipsViable){
        if(rnd > 0.5) return "hoverdepthcharge";
        return "hoverriot";
      }
      if(rnd > 0.7) return "hoverdepthcharge";
      return "hoverriot";
    }

    if(graphManager->eminentTerritory && kgb_rand() > 0.9){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 2);
      return "hoverarty";
    }
    if(kgb_rand() > 0.5) fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
    return "hoverskirm";
}

std::string FactoryManager::getTanks(Factory& fac){
    if(needWorkers(fac)){
      return "tankcon";
    }

    if(warManager->slasherSpam * 140.f > warManager->enemyPorcValue || warManager->slasherSpam * 560.f > assaultValue){
      return "tankassault";
    }

    if(scoutBudget > 0){
      fac.creatingScout = true;
      return "tankraid";
    }

    if(!fac.smallRaiderSpam){
      fac.smallRaiderSpam = true;
      fac.expensiveRaiderSpam = -5;
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      return "tankheavyraid";
    }

    fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "tankaa";
    }

    if(economyManager->adjustedIncome > 25.f && warManager->enemyHeavyPorcValue > 0.f && kgb_rand() < (graphManager->territoryFraction) * (1.f - artyValue/warManager->enemyPorcValue)){
      fac.expensiveRaiderSpam--;
      return "tankarty";
    }

    if(kgb_rand() > (warManager->sniperSightings.empty() ? 0.9 : 0.75)){
      fac.expensiveRaiderSpam -= (int) std::min(8, std::max(2, (int)std::floor(economyManager->adjustedIncome / (bigMap ? 2.5f : 5.f)))) + 1;
    }

    double rnd = kgb_rand();
    if(economyManager->adjustedIncome < 35 && !graphManager->eminentTerritory){
      fac.expensiveRaiderSpam -= 2;
      if(numBanishers > numReapers || warManager->enemyHeavyPorcValue > 850.f * numReapers){
        fac.expensiveRaiderSpam--;
        return "tankassault";
      } else {
        return "tankriot";
      }
    } else {
      fac.expensiveRaiderSpam--;
      if(armyValue > 2000.f && kgb_rand() > 0.9){
        fac.expensiveRaiderSpam -= 2;
        fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 1);
        return "tankheavyassault";
      }
      if(rnd > 0.15){
        fac.expensiveRaiderSpam--;
        return "tankassault";
      } else {
        return "tankriot";
      }
    }
}

std::string FactoryManager::getSpiders(Factory& fac){
    if(needWorkers(fac)){
      return "spidercon";
    }

    if(warManager->slasherSpam * 140 > assaultValue){
      if(kgb_rand() > 0.5){
        return "spiderassault";
      } else {
        return "spiderskirm";
      }
    }

    if(scoutBudget > 0){
      fac.creatingScout = true;
      return "spiderscout";
    }

    if(fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "spideraa";
    }

    if(economyManager->adjustedIncome > 30 && kgb_rand() < (graphManager->territoryFraction/5.f) * (1.f - artyValue/warManager->enemyPorcValue)){
      fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 6);
      return "spidercrabe";
    }

    fac.scoutAllowance = std::min(fac.maxScoutAllowance, fac.scoutAllowance + 3);
    if(numRedbacks < numVenoms){
      return "spiderriot";
    }

    if(numRecluses < (numVenoms + numRedbacks)){
      return "spiderskirm";
    }

    if(numHermits < (numVenoms + numRedbacks)){
      return "spiderassault";
    }

    double rnd = kgb_rand();
    if(rnd > 0.9 || numVenoms == 0){
      return "spideremp";
    } else {
      if(kgb_rand() > 0.5) return "spiderskirm";
      return "spiderassault";
    }
}

std::string FactoryManager::getGunship(Factory& fac){
    if(fac.raiderSpam < 0){
      if(kgb_rand() > 0.5){
        fac.raiderSpam++;
      }
      // 74145: disabled, don't know how to use it.
      //return "gunshipbomb";
    }

    if(fac.expensiveRaiderSpam < 0){
      fac.expensiveRaiderSpam++;
      return "gunshipheavyskirm";
    }

    if(needWorkers(fac)){
      return "gunshipcon";
    }

    if(graphManager->eminentTerritory && fighterValue > AAvalue && kgb_rand() > (AAvalue/warManager->maxEnemyAirValue) * (AAvalue/warManager->maxEnemyAirValue)){
      return "gunshipaa";
    }

    if(kgb_rand() > 0.75){
      fac.raiderSpam -= 2;
    }

    if(graphManager->eminentTerritory && kgb_rand() > 0.95 - std::max(0.1, 0.2 * graphManager->territoryFraction)){
      fac.raiderSpam -= 3;
      fac.expensiveRaiderSpam -= 6;
      return "gunshipkrow";
    }

    if(!graphManager->eminentTerritory){
      return "gunshipskirm";
    }

    if(kgb_rand() > 0.9){
      fac.expensiveRaiderSpam -= 6;
    }

    if(kgb_rand() > 0.85){
      return "gunshipassault";
    }
    return "gunshipskirm";
}

std::string FactoryManager::getPlanes(Factory& fac){
    // 74145: TODO: swifts? chonky?
    if(needWorkers(fac)){
      return "planecon";
    }
    if(economyManager->sparrow == nullptr) return "planescout";
    // raven raven raven raven raven... :D
    return "bomberprec";
}

std::string FactoryManager::getStrider(){
    if(economyManager->botsViable){
      if(economyManager->adjustedIncome > 60 && (int)warManager->miscHandler->striders.size() > 1 + numHeavyStriders && kgb_rand() > 0.5){
        if(numHeavyStriders < 2 || kgb_rand() > 0.35){
          return "striderbantha";
        } else {
          return "striderdetriment";
        }
      }
    }

    if(numUltis < std::round(std::log(warManager->enemyHeavyValue)) - 3 && hasFusion && ((numStriders > 0 && kgb_rand() > 0.5) || warManager->enemyHasTrollCom)){
      return "striderantiheavy";
    }

    if(economyManager->botsViable){
      double rnd = kgb_rand();
      if(hasFusion && rnd > 0.75){
        return  "striderscorpion";
      } else {
        return "striderdante";
      }
    }
    if(economyManager->shipsViable){
      if(hasFusion && economyManager->adjustedIncome > 100 && kgb_rand() < 0.35){
        return "striderdetriment";
      } else {
        return "shipheavyarty"; // Shogun
      }
    }
    if(hasFusion && economyManager->spidersViable){
      return "striderscorpion";
    }
    return "striderantiheavy";
}

}

