#include "Factory.h"

#include "../ZKGBAI.h"

namespace zkgbai {

Factory::Factory(ZKGBAI* ai, springai::Unit* u, bool firstFac) : Worker(ai, u), pos(u->GetPos()){
    std::string defName(u->GetDef()->GetName());

    if(firstFac){
      raiderSpam = -6;
      scoutAllowance = 2;
      maxScoutAllowance = 2;

      bool earlyWorker = ai->mapDiag > 910.f;
      bool medMap = ai->mapDiag > 1080.f;
      bool bigMap = ai->mapDiag > 1270.f;

      if(earlyWorker){
        scoutAllowance = 3;
        maxScoutAllowance = 4;
      }

      //if(medMap) maxScoutAllowance = 4;
      if(bigMap) raiderSpam = -9;

      if(defName == "factorytank"){
        scoutAllowance = 1;
        raiderSpam = 0;
      } else if(defName == "factorygunship"){
        raiderSpam = -2;
      } else if(defName == "factoryveh"){
        scoutAllowance--;
        maxScoutAllowance++;
        raiderSpam = -4;
      } else if(defName == "factoryhover"){
        scoutAllowance--;
        maxScoutAllowance++;
        raiderSpam = -5;
        if(bigMap) raiderSpam = -10;
      } else if(defName == "factoryspider"){
        maxScoutAllowance = 12;
        scoutAllowance = 12;
      } else if(defName == "factoryplane"){
        raiderSpam = -1;
      }
    } else if(defName == "factorygunship"){
      expensiveRaiderSpam = -6;
    }

    if(defName == "factorygunship" || defName == "factoryplane" || defName == "striderhub"){
      scoutAllowance = 0;
      maxScoutAllowance = 0;
    }
}

}
