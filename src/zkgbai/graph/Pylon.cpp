#include "Pylon.h"

#include <algorithm>
#include "kgbutil/KgbUtil.h"

namespace zkgbai {

Pylon::Pylon(springai::Unit* u, int rad) :
    radius(rad), position(u->GetPos()), unit(u){}

void Pylon::addNeighbour(Pylon& p){
    if(!CONTAINSP(neighbours,&p)){
      neighbours.push_back(&p);
    }
}

void Pylon::removeNeighbour(Pylon& p){
    ERASEONE(neighbours,&p);
}

void Pylon::addSpot(MetalSpot& s){
    spots.push_back(&s);
}

void Pylon::addLink(Link& l){
    links.push_back(&l);
}

int Pylon::getUnitId(){
    return unit->GetUnitId();
}

std::vector<MetalSpot*> Pylon::getSpots(){
    return spots;
}

springai::Unit* Pylon::getUnit(){
    return unit;
}

}
