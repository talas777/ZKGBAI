#ifndef _ZKGBAI_METALSPOT_H
#define _ZKGBAI_METALSPOT_H

// generated by the C++ Wrapper scripts
#include "Unit.h"
#include "AIFloat3.h"

namespace zkgbai {

class Link;
class Pylon;

class MetalSpot {
public:
    MetalSpot(float x, float y, float z, float m);

    friend bool operator<(const MetalSpot& lhs, const MetalSpot& rhs){
      return( std::tie(lhs.value, lhs.position.x, lhs.position.z)
              <
              std::tie(rhs.value, rhs.position.x, rhs.position.z) );
    }

    friend bool operator==(const MetalSpot& lhs, const MetalSpot& rhs){
      return lhs.value == rhs.value && lhs.position.x == rhs.position.x && lhs.position.z == rhs.position.z;
    }

    void addPylon(Pylon& p);
    void addLink(Link& l);

    int getPylonCount() const;
    springai::AIFloat3 getPos() const;
    float getLastSeen() const;
    float getValue() const;

    bool isFrontLine() const;
    bool isConnected();

    const springai::AIFloat3 position;
    const float value;
    bool visible = false;

    bool owned = false;
    bool touched = false;
    bool hostile = false;
    bool enemyShadowed = false;
    bool allyShadowed = false;
    springai::Unit* extractor = nullptr;

    int lastSeen = 0;
    std::vector<Pylon*> pylons;
    std::vector<Link*> links;
};
}
#endif //_ZKGBAI_METALSPOT_H
