#include "MetalSpot.h"

#include "Link.h"

namespace zkgbai {

MetalSpot::MetalSpot(float x, float y, float z, float m) :
    position(x,y,z), value(m){}

void MetalSpot::addPylon(Pylon& p){
    pylons.push_back(&p);
}

void MetalSpot::addLink(Link& l){
    links.push_back(&l);
}

int MetalSpot::getPylonCount() const {
    return pylons.size();
}

springai::AIFloat3 MetalSpot::getPos() const {
    return position;
}

float MetalSpot::getLastSeen() const {
    return lastSeen;
}

float MetalSpot::getValue() const {
    return value;
}

bool MetalSpot::isFrontLine() const {
    if(!owned && !allyShadowed) return false;
    else if(enemyShadowed) return true;

    for(const Link* l : links){
      if(l->isHostile()){
        return true;
      }
    } return false;
}

bool MetalSpot::isConnected(){
    int numConnected = 0;
    if(extractor == nullptr || extractor->IsBeingBuilt()){
      // don't allow more than one connected link to uncapped or partially finished mexes where we can't check grid number.
      for(const Link* l : links) if(l->connected) return true;
      return false;
    } else {
      for(Link* l : links){
        if(l->isOwned()){
          if(l->v0->extractor == nullptr || l->v0->extractor->IsBeingBuilt() || l->v1->extractor == nullptr || l->v1->extractor->IsBeingBuilt())
          { if(l->connected) numConnected++;
            continue;
          }
          if(!l->connected && l->v0->extractor->GetHealth() > 0 && l->v1->extractor->GetHealth() > 0 && l->v0->extractor->GetRulesParamFloat("gridNumber", 0.f) != l->v1->extractor->GetRulesParamFloat("gridNumber", 0.f))
          { return false; }
          numConnected++;
        }
      }
      if(numConnected == 0){
        return false;
      }
      return true;
    }
}

}
