#include "Link.h"

#include <set>
#include <queue>
#include <algorithm>
#include "kgbutil/KgbUtil.h"

namespace zkgbai {

Link::Link(MetalSpot& a, MetalSpot& b) : v0(&a), v1(&b)
{
    calcCenterPos();
    calcLength();
}

void Link::addPylon(Pylon& p){
    pylons[p.getUnitId()] = &p;
}

void Link::removePylon(Pylon& p){
    pylons.erase(p.getUnitId());
}

void Link::calcCenterPos(){
    springai::AIFloat3 pos1(v0->position);
    springai::AIFloat3 pos2(v1->position);
    centerPos.x = (pos1.x+pos2.x)/2;
    centerPos.y = (pos1.y+pos2.y)/2;
    centerPos.z = (pos1.z+pos2.z)/2;
}

void Link::calcLength(){
    springai::AIFloat3 pos1(v0->position);
    springai::AIFloat3 pos2(v1->position);
    float dx = (pos1.x-pos2.x);
    float dz = (pos1.z-pos2.z);
    length = (float) std::sqrt(dx*dx+dz*dz);
}

springai::AIFloat3 Link::getPos(){
    return centerPos;
}

bool Link::isOwned(){
    return (v0->owned && v1->owned) || ((v0->owned || v1->owned) && !v0->hostile && !v0->enemyShadowed && !v1->hostile && !v1->enemyShadowed);
}

bool Link::isAllyShadowed(){
    return (v0->allyShadowed || v1->allyShadowed) && !v0->hostile && !v1->hostile;
}

bool Link::isHostile() const {
    return v0->hostile || v0->enemyShadowed || v1->hostile || v1->enemyShadowed;
}

void Link::checkConnected(){
    std::set<int> visited;
    std::queue<Pylon*> queue;
    float minDist = 65535;
    int i = 0;

    // check each pylon
    for(Pylon* p : v1->pylons){
      queue.push(p);
    }

    MetalSpot* target=v0;
    while(!queue.empty()){
      if(i++>50){
        connected = false;
        return;
      }// infinite loop guard
      Pylon* q = queue.front();
      queue.pop();
      visited.insert(q->getUnitId());

      // iterate through each pylon starting at v1 and
      // progressively moving towards v0 until v0 is reached
      float dist = distance(q->position, v0->position);
      if(dist < minDist){
        minDist = dist;
        for(Pylon* child : q->neighbours){
          if(visited.find(child->getUnitId()) == visited.end()){
            queue.push(child);
          }
        }
      }
      if(CONTAINSP(q->spots,target)){
        connected = true;
        return;
      }
    }
    connected = false;
}

Pylon* Link::getConnectionHead(const springai::AIFloat3& position){
    Pylon* winner = nullptr;
    float minDist = 65535;
    int i = 0;

    //start from the closest end
    MetalSpot *start, *end;
    if(distance(position, v0->getPos()) < distance(position, v1->getPos())){
      start=v0; end=v1;
    } else {
      start=v1; end=v0;
    }
    if(connected || start->pylons.size() == 0){
      return nullptr;
    }

    std::set<int> visited;
    std::queue<Pylon*> queue;

    for(Pylon* p : start->pylons) queue.push(p);

    while(!queue.empty()){
      if(i++ > 50) return nullptr; // infinite loop guard
      Pylon* q = queue.front();
      queue.pop();
      visited.insert(q->getUnitId());
      float dist = distance(q->position, end->position);
      if(dist < minDist){
        minDist=dist; winner=q;
        for(Pylon* child : q->neighbours){
          if(visited.find(child->getUnitId()) == visited.end()){
            queue.push(child);
          }
        }
      }
    } return winner;
}

}
