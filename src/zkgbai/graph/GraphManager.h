#ifndef _ZKGBAI_GRAPHMANAGER_H
#define _ZKGBAI_GRAPHMANAGER_H

#include <set> // std::set

#include "../Module.h"
#include "../kgbutil/ByteArrayGraphics.h"

// generated by the C++ Wrapper scripts
#include "OOAICallback.h"
#include "AIFloat3.h"
#include "UnitDef.h"
#include "Resource.h"

#include "MetalSpot.h"

namespace zkgbai {

class ZKGBAI;
class MilitaryManager;
class LosManager;

class GraphManager : public Module {
public:
    GraphManager(ZKGBAI* ai);
    virtual ~GraphManager();

    int init(int AIID, springai::OOAICallback* cb) override;
    std::string getModuleName() override { return  "GraphManager"; }
    int luaMessage(const std::string& inData) override;
    int unitCreated(springai::Unit* unit, springai::Unit* builder) override;
    int unitFinished(springai::Unit* unit) override;
    int unitDestroyed(springai::Unit* unit, springai::Unit* attacker) override;
    int enemyEnterLOS(springai::Unit* enemy) override;
    // TODO: move los stuff to a separate handler
    int update(int uframe) override;

    void setStartPos(const springai::AIFloat3& pos);
    springai::AIFloat3 getStartPos();

    std::vector<Link*> getLinks();
    bool isAllyTerritory(const springai::AIFloat3& position);
    bool isEnemyTerritory(const springai::AIFloat3& position);
    bool isFrontLine(const springai::AIFloat3& position);
    std::vector<MetalSpot*> getMetalSpots();
    std::vector<MetalSpot*> getEnemySpots();
    std::vector<MetalSpot*> getEnemyTerritory();
    std::vector<MetalSpot*> getAllyTerritory();
    void paintASCIIGraph();

    springai::AIFloat3 getAllyCenter();
    springai::AIFloat3 getAllyBase();
    springai::AIFloat3 getMapCenter();
    springai::AIFloat3 getEnemyCenter();
    springai::AIFloat3 getClosestHaven(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestRaiderHaven(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestFrontLineLink(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestAirHaven(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestLeadingLink(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestAirLeadingLink(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestBattleFront(const springai::AIFloat3& position);
    std::vector<MetalSpot*> getOwnedSpots();
    std::vector<MetalSpot*> getUnownedSpots();
    std::vector<MetalSpot*> getNeutralSpots();
    std::vector<MetalSpot*> getTouchedSpots();
    MetalSpot* getClosestNeutralSpot(const springai::AIFloat3& position);
    MetalSpot* getClosestEnemySpot(const springai::AIFloat3& position);
    MetalSpot* getClosestSpot(const springai::AIFloat3& position);
    std::vector<MetalSpot*> getFrontLineSpots();
    MetalSpot* getClosestFrontLineSpot(const springai::AIFloat3& position);
    springai::AIFloat3 getClosestPylonSpot(const springai::AIFloat3& position);
    springai::AIFloat3 getOverdriveSweetSpot(const springai::AIFloat3& position, std::string pylonName);

    int myTeamID = -1;

    float territoryFraction = 0;
    bool eminentTerritory = false;
    bool isWaterMap = false;

    std::map<std::string, int> pylonDefs;

private:
    ZKGBAI* ai;
    springai::OOAICallback* callback;
    MilitaryManager* warManager;
    std::vector<MetalSpot*> metalSpots;
    std::vector<Link*> links;

    int frame = 0;
    springai::UnitDef*mexDef = nullptr;
    int mexDefID = -1;
    LosManager* losManager = nullptr;
    springai::Resource* e = nullptr;
    springai::Resource* m = nullptr;

    ByteArrayGraphics frontLineGraphics;
    ByteArrayGraphics allyTerritoryGraphics;
    ByteArrayGraphics enemyTerritoryGraphics;

    float avgMexValue = 0;
    bool graphInitialized = false;
    bool bigMap = false;

    springai::AIFloat3 allyCenter;
    springai::AIFloat3 enemyCenter;
    springai::AIFloat3 mapCenter;
    springai::AIFloat3 allyBase;
    springai::AIFloat3 startPos;

    int pylonCounter = 0;

    GraphManager(ZKGBAI* ai, int w, int h);

    std::vector<std::map<std::string, float>> parseMetalSpotsGRP();
    void setHostile(MetalSpot& ms);
    void setOwned(MetalSpot& ms);
    void setNeutral(MetalSpot& ms);
    void cleanShadows(MetalSpot& ms);
    void initializeGraph(std::vector<std::map<std::string, float>> jsonData);

    void doInitialInference();

    void calcCenters();
};
}
#endif //_ZKGBAI_GRAPHMANAGER_H
