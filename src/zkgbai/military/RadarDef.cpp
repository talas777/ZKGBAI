#include "RadarDef.h"

namespace zkgbai {

RadarDef::RadarDef(const std::vector<springai::UnitDef*>& defs, springai::Resource* r)
{
    float totalSpeed = 0;
    float totalValue = 0;
    float totalRange = 0;
    float totalDanger = 0;

    sampleName = defs[0]->GetName();

    for(springai::UnitDef* ud : defs){
      totalSpeed += ud->GetSpeed();
      totalValue += ud->GetCost(r);
      totalDanger += std::sqrt(ud->GetPower() * std::min(1, (int)ud->GetWeaponMounts().size()));
      totalRange += ud->GetMaxWeaponRange();
    }

    int udSize = defs.size(); 
    averageSpeed = totalSpeed / udSize / 30;
    averageValue = totalValue / udSize;
    averageRange = totalRange / udSize;
    averageRange = totalDanger / udSize;
}

float RadarDef::getDanger(){
    return averageDanger;
}

float RadarDef::getSpeed(){
    return averageSpeed;
}

std::string RadarDef::getName(){
    return sampleName;
}

float RadarDef::getRange(){
    return averageRange;
}

float RadarDef::getValue(){
    return averageValue;
}

}
