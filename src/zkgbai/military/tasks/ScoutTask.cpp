#include "ScoutTask.h"

#include <algorithm>

#include "../unitwrappers/Raider.h"

#include "graph/MetalSpot.h"

namespace zkgbai {

ScoutTask::ScoutTask(const springai::AIFloat3& t, MetalSpot& s) :
    FighterTask(t), spot(&s){}

void ScoutTask::addRaider(Raider& r){
    auto it = std::find(assignedRaiders.begin(), assignedRaiders.end(), &r);
    if(it==assignedRaiders.end()){
      assignedRaiders.push_back(&r);
    }
}

void ScoutTask::removeRaider(Raider& r){
    auto it = std::find(assignedRaiders.begin(), assignedRaiders.end(), &r);
    if(it!=assignedRaiders.end()){
      (void)assignedRaiders.erase(it);
    }
}

void ScoutTask::endTask(){
    for(Raider* r : assignedRaiders) r->endTask();
}

bool ScoutTask::hasRaider(const Raider& r) const {
    auto it = std::find(assignedRaiders.begin(), assignedRaiders.end(), &r);
    return it != assignedRaiders.end();
}
int ScoutTask::countRaiders() const {
    return assignedRaiders.size();
}

}
