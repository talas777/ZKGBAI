#include "BomberHandler.h"

#include <algorithm>

#include "ZKGBAI.h"
#include "kgbutil/Pathfinder.h"
#include "../Enemy.h"

namespace zkgbai {

BomberHandler::BomberHandler(ZKGBAI* zkgbai) : ai(zkgbai){
    this->warManager = ai->warManager;
    this->graphManager = ai->graphManager;
    this->ecoManager = ai->ecoManager;
    this->losManager = ai->losManager;
    this->pathfinder = ai->pathfinder;
}

BomberHandler::~BomberHandler(){
    for(auto &[k,b] : bombers) delete b;
}

void BomberHandler::addBomber(Bomber&& b0){
    if(bombers.find(b0.id) != bombers.end()) return;
    Bomber* b = new Bomber(std::move(b0));
    bombers[b->id] = b;
    readyBombers.push_back(b);
}

void BomberHandler::removeUnit(springai::Unit* u){
  auto it = bombers.find(u->GetUnitId());
    if(it != bombers.end()){
      Bomber* const f = (*it).second;
      ERASEONE(unarmedBombers, f);
      ERASEONE(readyBombers, f);
      ERASEONE(activeBombers, f);
      delete f;
      bombers.erase(it);
    }
}

void BomberHandler::removeTarget(Enemy* e){
    for(auto &[k,b] : bombers){
      if(b->target == e) b->target = nullptr;
    }
}

int BomberHandler::getBomberSize(){
    return unarmedBombers.size() + readyBombers.size() + activeBombers.size();
}

void BomberHandler::update(int f){
    this->frame = f;

    if(frame % 60 == ai->offset % 60){
      if(unarmedBombers.empty() && readyBombers.empty() && activeBombers.empty()){return;}
      cleanUnits();
      checkBombers();
      assignBombers();
    }
}

void BomberHandler::checkBombers(){
    for(auto it=unarmedBombers.begin(); it!=unarmedBombers.end(); ){
      Bomber* b = *it;
      bool isUnarmed = false;
      if(b->getUnit()->GetHealth() < b->getUnit()->GetMaxHealth()){
        isUnarmed = true;
      } else if(b->getUnit()->GetRulesParamFloat("noammo", 0.0f) > 0.f){
        isUnarmed = true;
      }
      if(!isUnarmed){
        readyBombers.push_back(b);
        it = unarmedBombers.erase(it);
      } else ++it;
    }
}

void BomberHandler::assignBombers(){
    for(auto it=activeBombers.begin(); it!=activeBombers.end(); ){
      Bomber* b = *it;
      // sift unarmed bombers back into the unarmed list.
      bool isUnarmed = false;
      if(b->getUnit()->GetRulesParamFloat("noammo", 0.0f) > 0.f){
        isUnarmed = true;
      }

      if(isUnarmed){
        unarmedBombers.push_back(b);
        it = activeBombers.erase(it);
        continue;
      }

      if(b->targetMissing()){
        unarmedBombers.push_back(b);
        b->flyTo(graphManager->getAllyCenter());
        it = activeBombers.erase(it);
      } else {
        b->bomb();
        ++it;
      }
    }

    // have all unarmed bombers reload/heal themselves.
    for(Bomber* b : unarmedBombers){
      if(!graphManager->isEnemyTerritory(b->getPos())){
        b->findPad();
      } else {
        b->flyTo(graphManager->getAllyCenter()); // if in enemy territory, maneuver back to safety before finding an airpad.
      }
    }

    if(unarmedBombers.empty() && (int)readyBombers.size() >= minFlock){
      std::vector<Enemy*> targets = warManager->getBomberTargets();
      if(!targets.empty())
        minFlock = std::max(8, (int)(std::ceil(targets.front()->ud->GetHealth()/800.f) + (targets.front()->isImportant ? 1 : 0)));

      while(!targets.empty() && !readyBombers.empty()){
        Enemy* e = targets.front(); targets.erase(targets.begin());
        int neededBombers = (int)(std::ceil(e->ud->GetHealth()/800.f) + (e->isImportant ? 1 : 0));
        if(neededBombers > (int)readyBombers.size()) continue;
        while(neededBombers > 0){
          Bomber* b = readyBombers.front();
          readyBombers.erase(readyBombers.begin());
          b->target = e;
          activeBombers.push_back(b);
          neededBombers--;
        }
      }
    }
}

void BomberHandler::cleanUnits(){
    std::vector<Bomber*> invalidBombers;
    for(auto &[k,f] : bombers){
      if(f->isDead()) invalidBombers.push_back(f);
    }
    for(Bomber* const f : invalidBombers){
        ERASEONE(unarmedBombers, f);
        ERASEONE(readyBombers, f);
        ERASEONE(activeBombers, f);
        bombers.erase(f->id);
        delete f;
    }
}

}
