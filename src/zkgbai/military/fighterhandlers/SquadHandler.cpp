#include "SquadHandler.h"

#include <algorithm>

#include "ZKGBAI.h"
#include "../MilitaryManager.h"
#include "economy/EconomyManager.h"
#include "graph/GraphManager.h"
#include "RetreatHandler.h"
#include "../UnitClasses.h"

namespace zkgbai {

SquadHandler::SquadHandler(ZKGBAI* zkgbai) : ai(zkgbai){
    this->warManager = ai->warManager;
    this->ecoManager = ai->ecoManager;
    this->graphManager = ai->graphManager;

    this->unitTypes = UnitClasses::getInstance();
}

SquadHandler::~SquadHandler(){
    for(auto* s : squads) delete s;
    for(auto* s : shieldSquads) delete s;
    for(auto &[k,f] : fighters) delete f;
}

void SquadHandler::update(int frame){
    this->frame = frame;

    if(frame % 30 == (ai->offset + 7) % 30){
      for(auto it=fighters.begin(); it!=fighters.end(); ){
        int id=(*it).first;
        Fighter* const f=(*it).second;
        if(f->isDead()){
          if(f->squad != nullptr) f->squad->removeUnit(*f);
          delete f; it=fighters.erase(it); continue;
        }
        bool retreating = warManager->retreatHandler->isRetreating(f->getUnit());
        if(!retreating && !f->getUnit()->GetDef()->IsAbleToFly() && f->isStuck(frame)){
          if(f->squad != nullptr) f->squad->removeUnit(*f);
          ai->ecoManager->addCombatReclaimTask(CombatReclaimTask(f->getUnit()));
          warManager->retreatHandler->removeUnit(f->getUnit());
          delete f; it=fighters.erase(it); continue;
        }
        if(f->squad == nullptr && !retreating){
          if(CONTAINSS(unitTypes->assaults,f->getUnit()->GetDef()->GetName())){
            assignAssault(f);
          } else {
            assignShieldMob(f);
          }
        } ++it;
      }
    }
    if(frame % 7 == ai->offset % 7) assignSquads();
    if(frame % 7 == (ai->offset + 4) % 7) assignShieldSquads();
    if(frame % 14 == (ai->offset + 9) % 14) assignAirSquads();
}

void SquadHandler::assignAssault(Fighter* f){
    std::string defName(f->getUnit()->GetDef()->GetName());
    float tf = graphManager->territoryFraction;
    float inc = ecoManager->effectiveIncome;
    int allies = ai->mergedAllies;
    if(defName.find("cloak")==0){
      if(nextCloakySquad == nullptr){
        nextCloakySquad = new Squad(ai);
        nextCloakySquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextCloakySquad);
      } nextCloakySquad->addUnit(*f);
      if(((nextCloakySquad->metalValue > nextCloakySquad->income * 45.f && nextCloakySquad->metalValue > 1000.f)
          || nextCloakySquad->metalValue > 2500.f) && nextCloakySquad->getSize() > 2){
        nextCloakySquad->status = 'r';
        nextCloakySquad = nullptr;
      }
    } else if(defName.find("shield")==0){
      if(nextShieldSquad == nullptr){
        nextShieldSquad = new Squad(ai);
        nextShieldSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextShieldSquad);
      } nextShieldSquad->addUnit(*f);
      if(((nextShieldSquad->metalValue > nextShieldSquad->income * 45.f && nextShieldSquad->metalValue > 1000.f)
          || nextShieldSquad->metalValue > 2500.f) && nextShieldSquad->getSize() > 2){
        nextShieldSquad->status = 'r';
        nextShieldSquad = nullptr;
      }
    } else if(defName.find("amph")==0){
      if(nextAmphSquad == nullptr){
        nextAmphSquad = new Squad(ai);
        nextAmphSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextAmphSquad);
      } nextAmphSquad->addUnit(*f);
      if(((nextAmphSquad->metalValue > nextAmphSquad->income * 45.f && nextAmphSquad->metalValue > 1000.f)
          || nextAmphSquad->metalValue > 2500.f) && nextAmphSquad->getSize() > 2){
        nextAmphSquad->status = 'r';
        nextAmphSquad = nullptr;
      }
    } else if(defName == "spiderskirm"){
      if(nextRecluseSquad == nullptr){
        nextRecluseSquad = new Squad(ai);
        nextRecluseSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextRecluseSquad);
      } nextRecluseSquad->addUnit(*f);
      if(((nextRecluseSquad->metalValue > nextRecluseSquad->income * 45.f && nextRecluseSquad->metalValue > 1000.f)
          || nextRecluseSquad->metalValue > 2500.f) && nextRecluseSquad->getSize() > 2){
        nextRecluseSquad->status = 'r';
        nextRecluseSquad = nullptr;
      }
    } else if(defName.find("spider")==0){
      if(nextSpiderSquad == nullptr){
        nextSpiderSquad = new Squad(ai);
        nextSpiderSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextSpiderSquad);
      } nextSpiderSquad->addUnit(*f);
      if(((nextSpiderSquad->metalValue > nextSpiderSquad->income * 45.f && nextSpiderSquad->metalValue > 1000.f)
          || nextSpiderSquad->metalValue > 2500.f) && nextSpiderSquad->getSize() > 2){
        nextSpiderSquad->status = 'r';
        nextSpiderSquad = nullptr;
      }
    } else if(defName.find("veh")==0){
      if(nextVehSquad == nullptr){
        nextVehSquad = new Squad(ai);
        nextVehSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextVehSquad);
      } nextVehSquad->addUnit(*f);
      if(((nextVehSquad->metalValue > nextVehSquad->income * 45.f && nextVehSquad->metalValue > 1000.f)
          || nextVehSquad->metalValue > 2500.f) && nextVehSquad->getSize() > 2){
        nextVehSquad->status = 'r';
        nextVehSquad = nullptr;
      }
    } else if(defName == "hoverassault"){
      if(nextHalberdSquad == nullptr){
        nextHalberdSquad = new HoverSquad(ai);
        nextHalberdSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextHalberdSquad);
      } nextHalberdSquad->addUnit(*f);
      if(nextHalberdSquad->getSize() >= (int) std::min(8, 2 + (int)std::floor(ecoManager->adjustedIncome / 15.f))){
        nextHalberdSquad->status = 'r';
        nextHalberdSquad = nullptr;
      }
    } else if(defName.find("hover")==0){
      if(nextHoverSquad == nullptr){
        nextHoverSquad = new Squad(ai);
        nextHoverSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextHoverSquad);
      } nextHoverSquad->addUnit(*f);
      if(((nextHoverSquad->metalValue > nextHoverSquad->income * 45.f && nextHoverSquad->metalValue > 1000.f)
          || nextHoverSquad->metalValue > 2500.f) && nextHoverSquad->getSize() > 2){
        nextHoverSquad->status = 'r';
        nextHoverSquad = nullptr;
      }
    } else if(defName.find("tank")==0){
      if(nextTankSquad == nullptr){
        nextTankSquad = new Squad(ai);
        nextTankSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextTankSquad);
      } nextTankSquad->addUnit(*f);
      if(((nextTankSquad->metalValue > nextTankSquad->income * 45.f && nextTankSquad->metalValue > 1000.f)
          || nextTankSquad->metalValue > 2500.f) && nextTankSquad->getSize() > 2){
        nextTankSquad->status = 'r';
        nextTankSquad = nullptr;
      }
    } else if(defName.find("ship")==0){
      if(nextBoatSquad == nullptr){
        nextBoatSquad = new Squad(ai);
        nextBoatSquad->income = inc / (1.f + (allies * tf * tf));
        squads.push_back(nextBoatSquad);
      } nextBoatSquad->addUnit(*f);
      if(((nextBoatSquad->metalValue > nextBoatSquad->income * 45.f && nextBoatSquad->metalValue > 1000.f)
          || nextBoatSquad->metalValue > 2500.f) && nextBoatSquad->getSize() > 2){
        nextBoatSquad->status = 'r';
        nextBoatSquad = nullptr;
      }
    } else if(defName == "gunshipheavyskirm"){
      if(nextBrawlerSquad == nullptr){
        nextBrawlerSquad = new Squad(ai);
        nextBrawlerSquad->income = inc / (allies == 0 ? 1 : 2);
        nextBrawlerSquad->isAirSquad = true;
        airSquads.push_back(nextBrawlerSquad);
      } nextBrawlerSquad->addUnit(*f);
      if(nextBrawlerSquad->metalValue > nextBrawlerSquad->income * 45){
        nextBrawlerSquad->status = 'r';
        nextBrawlerSquad = nullptr;
      }
    } else {
      // for rapier/revenant
      if(nextAirSquad == nullptr){
        nextAirSquad = new Squad(ai);
        nextAirSquad->income = inc / (allies == 0 ? 1.f : 2.f);
        nextAirSquad->isAirSquad = true;
        airSquads.push_back(nextAirSquad);
      } nextAirSquad->addUnit(*f);
      if(nextAirSquad->metalValue > nextAirSquad->income * 45.f){
        nextAirSquad->status = 'r';
        nextAirSquad = nullptr;
      }
    }
}

void SquadHandler::addAssault(Fighter&& f0){
    if(fighters.find(f0.id) != fighters.end()) return;
    Fighter* f = new Fighter(std::move(f0));
    fighters[f->id] = f;
    assignAssault(f);
}

void SquadHandler::assignShieldMob(Fighter* f){
    std::string defName(f->getUnit()->GetDef()->GetName());
    if(defName == "shieldfelon" && (ai->mergedAllies == 0 || shieldSquads.empty())){
      // Create a new shieldball for each felon, unless on teams where you want just one giant death ball.
      warManager->retreatHandler->removeUnit(f->getUnit());
      ShieldSquad* s = new ShieldSquad(ai); shieldSquads.push_back(s);
      s->status = 'a'; s->addUnit(*f); callShieldMobs(*s);
    } else if(defName == "shieldshield" && !shieldSquads.empty()){
      // Assign aspis to the squad with the fewest aspis, giving preference to higher metal value shieldballs
      // where they do the most good and are more likely to survive.
      warManager->retreatHandler->removeUnit(f->getUnit());
      ShieldSquad* needed = nullptr;
      float metalv; int minAspis;
      for(ShieldSquad* s : shieldSquads){
        if(needed==nullptr || s->numAspis < minAspis || (s->numAspis == minAspis && s->metalValue > metalv))
        { needed = s; metalv = s->metalValue; minAspis = s->numAspis; }
      } needed->addUnit(*f);
    } else { // For other units.
      if(shieldSquads.empty()){ // If there's no felon, treat shield mobs as regular assaults.
        warManager->retreatHandler->addCoward(f->getUnit());
        assignAssault(f);
      } else { // Add the unit to the squad with the lowest value.
        warManager->retreatHandler->removeUnit(f->getUnit());
        ShieldSquad* needed = nullptr;
        float metalv;
        for(ShieldSquad* s : shieldSquads){
          if(needed==nullptr || s->metalValue < metalv)
          { needed = s; metalv = s->metalValue; }
        } needed->addUnit(*f);
      }
    }
}

void SquadHandler::addShieldMob(Fighter&& f0){
    if(fighters.find(f0.id) != fighters.end()) return;
    Fighter* f = new Fighter(std::move(f0)); 
    f->shieldMob = true;
    std::vector<Fighter*> fit;
    for(auto it=shieldSquads.begin(); it!=shieldSquads.end(); ){
      ShieldSquad* const s = *it;
      if(s->isDead() || s->numFelons == 0){
        // disband felonless squads and repurpose the units as regular mobs until a new felon appears.
        std::vector<Fighter*> losers(s->disband());
        fit.insert(fit.end(), losers.begin(), losers.end());
        for(auto &[k,g] : warManager->miscHandler->supports) if(g->squad == s) g->squad=nullptr;
        delete s; it=shieldSquads.erase(it);
      } else ++it;
    }
    for(Fighter* fi : fit) assignShieldMob(fi);
    fighters[f->id] = f;
    assignShieldMob(f);
}

void SquadHandler::callShieldMobs(ShieldSquad& s){
    // Call a flash mob when a felon appears.
    for(auto &[k,f] : fighters){
      if(!f->shieldMob) continue;
      if(f->squad != nullptr && f->squad->isShieldSquad()) continue;
      if(warManager->retreatHandler->isRetreating(f->getUnit()) || f->getUnit()->GetHealth() <= 0 || f->getUnit()->GetTeam() != ai->teamID){
        continue;
      }
      if(f->squad != nullptr){
        f->squad->removeUnit(*f);
        f->squad = nullptr;
      }
      warManager->retreatHandler->removeUnit(f->getUnit());
      s.addUnit(*f);
    }
}

void SquadHandler::removeUnit(springai::Unit* u){
    auto it = fighters.find(u->GetUnitId());
    if(it == fighters.end()) return;
    Fighter* const f = (*it).second;
    if(f->squad != nullptr) f->squad->removeUnit(*f);
    delete f; fighters.erase(it);
}

void SquadHandler::removeFromSquad(springai::Unit* u){
    auto it = fighters.find(u->GetUnitId());
    if(it == fighters.end()) return;
    Fighter* const f = (*it).second;
    if(f->squad != nullptr) f->squad->removeUnit(*f);
}

void SquadHandler::assignSquads(){
    // Assign up to one squad at a time, up to four per second.
    while(!squads.empty()){
      Squad* const s = squads.front(); squads.pop_front();
      s->cutoff();
      if(s->isDead()){
        if(s->status == 'f'){
          squads.push_back(s);
          return;
        }
        for(auto &[k,g] : warManager->miscHandler->supports) if(g->squad == s) g->squad=nullptr;
        delete s; continue;
      } else if(frame - s->lastAssignmentFrame < 30){
        squads.push_front(s);
        return;
      } else {
        assignSquad(*s);
        s->lastAssignmentFrame = frame;
        squads.push_back(s);
        return;
      }
    }
}

void SquadHandler::assignAirSquads(){
    // Assign up to one air squad at a time, up to two per second.
    while(!airSquads.empty()){
      Squad* const s = airSquads.front(); airSquads.pop_front();
      if(s->isDead()){
        if(s->status == 'f'){
          airSquads.push_back(s);
          return;
        }
        for(auto &[k,g] : warManager->miscHandler->supports) if(g->squad == s) g->squad=nullptr;
        delete s; continue;
      } else if(frame - s->lastAssignmentFrame < 60){
        airSquads.push_front(s);
        return;
      } else {
        assignAirSquad(*s);
        s->lastAssignmentFrame = frame;
        airSquads.push_back(s);
        return;
      }
    }
}

void SquadHandler::assignSquad(Squad& s){
    springai::AIFloat3 pos = s.getPos();

    if(s.status == 'f'){
      if(warManager->getThreat(pos) > 2.f * warManager->getTotalFriendlyThreat(pos) || warManager->getPorcThreat(pos) > 0 || (s.target != nullpos && warManager->getPorcThreat(s.target) > 0)){
        s.retreatTo(graphManager->getClosestHaven(pos));
      } else {
        s.setTarget(warManager->getRallyPoint(pos));
      }
    } else if(s.status == 'r'){
      // set rallying for squads that are finished forming and gathering to attack
      s.setTarget(warManager->getRallyPoint(pos));
      if(s.isRallied(frame)){
        s.status = 'a';
      }
    } else if(s.status == 'a'){
      if(warManager->getEffectiveThreat(pos) > 0 && (!graphManager->isAllyTerritory(pos) || warManager->getPorcThreat(pos) > 0)){
        s.retreatTo(graphManager->getClosestHaven(pos));
      }
      springai::Unit* leader = s.getLeader();
      springai::AIFloat3 target = warManager->getTarget(leader, true);
      s.setTarget(target);
    }
}

void SquadHandler::assignAirSquad(Squad& s){
    springai::AIFloat3 pos = s.getPos();

    if(s.status == 'f'){
      springai::AIFloat3 target = warManager->getAirRallyPoint(pos);
      if(distance(pos, target) > 1800.f){
        s.retreatTo(target);
      } else {
        s.setTarget(target);
      }
    } else if(s.status == 'r'){
      // set rallying for squads that are finished forming and gathering to attack
      s.setTarget(warManager->getAirRallyPoint(pos));
      if(s.isRallied(frame)){
        s.status = 'a';
      }
    } else if(s.status == 'a'){
      if(warManager->getEffectiveAAThreat(pos) > 0){
        s.retreatTo(graphManager->getClosestAirHaven(pos));
      }
      springai::Unit* leader = s.getLeader();
      springai::AIFloat3 target = warManager->getAirTarget(leader, true);
      if(distance(target, pos) > 1800.f){
        s.retreatTo(target);
      } else {
        s.setTarget(target);
      }
    }
}

void SquadHandler::assignShieldSquads(){
    // Process up to one shield squad at a time, four per second.
    while(!shieldSquads.empty()){
      ShieldSquad* const shieldSquad = shieldSquads.front();
      shieldSquads.pop_front();
      if(shieldSquad->isDead() || shieldSquad->numFelons == 0){
        // disband felonless squads and assign them to other felons if available, else dump them back as assaults.
        std::vector<Fighter*> fit(shieldSquad->disband());
        for(Fighter* fi: fit) assignShieldMob(fi);
        for(auto &[k,g] : warManager->miscHandler->supports) if(g->squad == shieldSquad) g->squad=nullptr;
        delete shieldSquad; continue;
      }
      if(frame - shieldSquad->lastAssignmentFrame < 30){
        shieldSquads.push_front(shieldSquad);
        return;
      }

      if(shieldSquad->lowShields && shieldSquad->getShields() > 0.8f){
        shieldSquad->lowShields = false;
      } else if(!shieldSquad->lowShields && shieldSquad->getShields() < 0.5f){
        shieldSquad->lowShields = true;
      }
      if(shieldSquad->lowShields){
        shieldSquad->retreatTo(graphManager->getClosestRaiderHaven(shieldSquad->getPos()));
      } else if(shieldSquad->getHealth() < 0.85f){
        shieldSquad->retreatTo(graphManager->getClosestHaven(shieldSquad->getPos()));
      } else if(shieldSquad->metalValue > 1300.f && std::string(shieldSquad->getLeader()->GetDef()->GetName()) == "shieldfelon" && shieldSquad->isRallied(frame)){
        springai::AIFloat3 target = warManager->getTarget(shieldSquad->getLeader(), false);
        shieldSquad->setTarget(target);
      } else if(warManager->getEffectiveThreat(shieldSquad->getPos()) > 0.f || warManager->getPorcThreat(shieldSquad->getPos()) > 0.f){
        shieldSquad->retreatTo(graphManager->getClosestHaven(shieldSquad->getPos()));
      } else {
        shieldSquad->setTarget(warManager->getRallyPoint(shieldSquad->getPos()));
      }
      shieldSquad->lastAssignmentFrame = frame;
      shieldSquads.push_back(shieldSquad);
      return;
    }
}

}
