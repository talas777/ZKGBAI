#ifndef _ZKGBAI_BOMBERHANDLER_H
#define _ZKGBAI_BOMBERHANDLER_H

#include <queue>

#include "../unitwrappers/Bomber.h"

namespace zkgbai {

class ZKGBAI;
class MilitaryManager;
class LosManager;
class GraphManager;
class EconomyManager;
class Pathfinder;

class BomberHandler {
public:
    BomberHandler(ZKGBAI* ai);
    ~BomberHandler();

    void addBomber(Bomber&& b);
    void removeUnit(springai::Unit* u);
    void removeTarget(Enemy* e);
    int getBomberSize();
    void update(int frame);

private:
    static constexpr int CMD_FIND_PAD = 33411;

    ZKGBAI* ai;
    MilitaryManager* warManager;
    LosManager* losManager;
    GraphManager* graphManager;
    EconomyManager* ecoManager;
    Pathfinder* pathfinder = nullptr;

    std::map<int, Bomber*> bombers;

    std::vector<Bomber*> unarmedBombers;
    std::deque<Bomber*> readyBombers;
    std::vector<Bomber*> activeBombers;

    int frame = 0;
    int minFlock = 8;

    void checkBombers();
    void assignBombers();
    void cleanUnits();
};
}
#endif //_ZKGBAI_BOMBERHANDLER_H
