#include "RetreatHandler.h"

#include <algorithm>

#include "ZKGBAI.h"

namespace zkgbai {

RetreatHandler::RetreatHandler(ZKGBAI* zkgbai) : ai(zkgbai){
    this->warManager = ai->warManager;
    this->graphManager = ai->graphManager;
    this->pathfinder = ai->pathfinder;
    this->unitTypes = UnitClasses::getInstance();

    springai::OOAICallback* callback = ai->getCallback();
    for(springai::UnitDef* def : callback->GetUnitDefs()){
      std::string tooltip(def->GetTooltip());
      std::string defName(def->GetName());
      if(tooltip.find("Anti-Air") != std::string::npos){
        AAdefs.insert(def->GetUnitDefId());
      } else if(defName.find("strider") == 0){
        striders.insert(def->GetUnitDefId());
      }
    }
    // 74145: TODO: this is getting silly, move all this stuff to unitTypes..

    for(std::string defName : unitTypes->smallRaiders){
      smallraiders.insert(callback->GetUnitDefByName(defName.c_str())->GetUnitDefId());
    }

    for(springai::UnitDef* ud : callback->GetUnitDefByName("factoryplane")->GetBuildOptions()){
      if(ud->GetBuildOptions().empty()){
        planes.insert(ud->GetUnitDefId());
      }
    }

    halberdID = callback->GetUnitDefByName("hoverassault")->GetUnitDefId();
    scytheID = callback->GetUnitDefByName("cloakheavyraid")->GetUnitDefId();

    lowManeuverability.insert(callback->GetUnitDefByName("tankraid")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("tankheavyraid")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("tankriot")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("hoverskirm")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("hoverriot")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("vehriot")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("vehassault")->GetUnitDefId());
    lowManeuverability.insert(callback->GetUnitDefByName("shieldriot")->GetUnitDefId());
}

void RetreatHandler::init(){
}

void RetreatHandler::addCoward(springai::Unit* u){
    int defID = u->GetDef()->GetUnitDefId();
    float rlvl;

    if(CONTAINSI(AAdefs,defID)){
      rlvl = 0.85f;
    } else if(u->GetDef()->IsAbleToFly() || CONTAINSI(lowManeuverability,defID)){
      rlvl = 0.65f;
    } else if(CONTAINSI(striders,defID)){
      rlvl = 0.6f;
    } else {
      rlvl = 0.5f;
    }

    Coward* c = new Coward(ai, u, rlvl);
    c->smallRaider = CONTAINSI(smallraiders,defID);
    c->isScythe = u->GetDef()->GetUnitDefId() == scytheID;
    c->isPlane = CONTAINSI(planes,defID);

    cowardUnits[c->id] = c;
    if(c->shield != nullptr) shields.insert(c->id);
}

bool RetreatHandler::isCoward(springai::Unit* u){
    return cowardUnits.find(u->GetUnitId()) != cowardUnits.end();
}

void RetreatHandler::checkUnit(springai::Unit* u, bool onFire){
    auto it = cowardUnits.find(u->GetUnitId());
    if(it == cowardUnits.end()) return; // Not our unit
    lastDamagedID = u->GetUnitId();
    lastDamagedCoward = (*it).second;
    if(lastDamagedCoward->isRetreating || (onFire && lastDamagedCoward->smallRaider)) return;
    if(lastDamagedCoward->shouldRetreat()){
      retreatingUnits.insert(lastDamagedID);
      warManager->squadHandler->removeFromSquad(u);
      warManager->raiderHandler->removeFromSquad(u);
    }
}

void RetreatHandler::update(int f){
    this->frame = f;
    if(frame % 15 == ai->offset % 15){
      if(cowardUnits.empty()) return;
      cleanUnits();
      checkShields();
      retreatCowards();
    }
}

bool RetreatHandler::isRetreating(springai::Unit* u){
    // don't retreat scythes unless they're cloaked
    if(u->GetDef()->GetUnitDefId() == scytheID){
      if(!u->IsCloaked() && warManager->getEffectiveThreat(u->GetPos()) <= 0){
        return false;
      }
    }
    if(CONTAINSI(retreatingUnits,u->GetUnitId())){
      return true;
    } return false;
}

void RetreatHandler::removeUnit(springai::Unit* u){
    const int uid = u->GetUnitId();
    auto it = cowardUnits.find(uid);
    if(it == cowardUnits.end()) return;
    Coward* const cow = (*it).second;
    if(lastDamagedID==uid){ lastDamagedID=0; lastDamagedCoward=nullptr; }
    retreatingUnits.erase(uid);
    retreatedUnits.erase(uid);
    shields.erase(uid);
    delete cow; (void)cowardUnits.erase(it);
}

void RetreatHandler::retreatCowards(){
    bool retreated = false;
    for(auto it=retreatingUnits.begin(); it!=retreatingUnits.end(); ){
        int id=*it;
        auto it2=cowardUnits.find(id);
        Coward* const c = (*it2).second;
        if(c->isHealed()){
          retreatedUnits.erase(id);
          it=retreatingUnits.erase(it); continue;
        }

        if(c->isDead()){
          retreatedUnits.erase(id);
          shields.erase(id);
          if(lastDamagedCoward==c){ lastDamagedID=0; lastDamagedCoward=nullptr; }
          delete c; (void)cowardUnits.erase(it2); it=retreatingUnits.erase(it); continue;
        }

        // don't retreat scythes unless they're cloaked
        if(c->isScythe){
          if(!c->isCloaked() && warManager->getEffectiveThreat(c->getPos()) <= 0){
            ++it; continue;
          }
        }

        if(!CONTAINSI(retreatedUnits,id) || warManager->getThreat(c->getPos()) > 0){
          if(!c->isPlane){
            if(c->isFlyer){
              c->retreatTo(graphManager->getClosestAirHaven(c->getPos()));
            } else {
              c->retreatTo(graphManager->getClosestHaven(c->getPos()));
            }
          } else {
            if(!graphManager->isEnemyTerritory(c->getPos())){
              c->findPad();
            } else {
              c->retreatTo(graphManager->getAllyCenter()); // if in enemy territory, maneuver back to safety before finding an airpad.
            }
          }
          retreatedUnits.insert(id);
          retreated = true;
        } ++it;
    }
    if(!retreated){
      retreatedUnits.clear();
    }
}

void RetreatHandler::checkShields(){
    for(const int& id : shields){
      auto it = cowardUnits.find(id);
      Coward* const c = (*it).second;
      if(!c->isRetreating && c->checkShield()){
        retreatingUnits.insert(id);
        warManager->squadHandler->removeFromSquad(c->getUnit());
      }
    }
}

void RetreatHandler::cleanUnits(){
    for(auto it=cowardUnits.begin(); it!=cowardUnits.end(); ){
      Coward* const c=(*it).second;
      if(c->isDead()){
        int id=(*it).first;
        retreatingUnits.erase(id);
        retreatedUnits.erase(id);
        shields.erase(id);
        delete c; it=cowardUnits.erase(it);
      } else ++it;
    }
    lastDamagedID=0; lastDamagedCoward=nullptr;
}

}
