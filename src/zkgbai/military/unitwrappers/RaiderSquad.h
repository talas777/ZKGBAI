#ifndef _ZKGBAI_RAIDERSQUAD_H
#define _ZKGBAI_RAIDERSQUAD_H

#include <vector>

// generated by the C++ Wrapper scripts
#include "AIFloat3.h"

#include "Squad.h"

namespace zkgbai {

class Raider;
class ZKGBAI;

class RaiderSquad: public Squad {
public:
    RaiderSquad(ZKGBAI* ai);
    virtual ~RaiderSquad();

    virtual void addUnit(Fighter& f) override;
    virtual void removeUnit(Fighter& f) override;

    void sneak(const springai::AIFloat3& pos, int frame);
    void raid(const springai::AIFloat3& pos, int frame);

    virtual springai::AIFloat3 getPos() const override;
    virtual bool isRallied(int frame) override;
    virtual int getThreat() override;
    virtual bool isDead() const override;

    char type = 0;

private:
    Fighter* getNewLeader() override;
};
}
#endif //_ZKGBAI_RAIDERSQUAD_H
