#include "Bomber.h"

#include "ZKGBAI.h"
#include "../Enemy.h"

namespace zkgbai {

Bomber::Bomber(ZKGBAI* ai, springai::Unit* u) : id(u->GetUnitId()), unit(u){
    pathfinder = ai->pathfinder;
    team = ai->teamID;
}

void Bomber::bomb(){
    if(distance(unit->GetPos(), target->position) > 1000.f){
      flyTo(target->position);
    } else {
      if(target->isStatic){
        unit->AttackArea(target->position, 0, 0);
      } else {
        if(target->unit->GetPos() != nullpos){
          unit->Attack(target->unit, 0);
        } else {
          flyTo(target->position);
        }
      }
    }
}

void Bomber::flyTo(const springai::AIFloat3& pos){
    std::queue<springai::AIFloat3> path = pathfinder->findPath(unit, pos, pathfinder->AIR_PATH);
    if(path.size() > 1) path.pop(); // skip several waypoints since they're close together and bombers are extra fast.
    if(path.size() > 1) path.pop();
    if(path.size() > 1) path.pop();
    if(path.size() > 1) path.pop();
    unit->MoveTo(path.front(), 0);
    path.pop();

    // Add the full path, since the command may not be repeated
    while(!path.empty()){
      if(path.size() > 1) path.pop(); // skip several waypoints since they're close together and bombers are extra fast.
      if(path.size() > 1) path.pop();
      if(path.size() > 1) path.pop();
      if(path.size() > 1) path.pop();
      unit->MoveTo(path.front(), OPTION_SHIFT_KEY);
      path.pop();
    }
}

void Bomber::findPad(){
    if(unit->GetHealth() < unit->GetMaxHealth() || unit->GetRulesParamFloat("noammo", 0.0f) > 0){
      std::vector<float> params;
      unit->ExecuteCustomCommand(CMD_FIND_PAD, params, 0);
    }
}

bool Bomber::targetMissing(){
    return target == nullptr || target->isDead || (distance(unit->GetPos(), target->position) < 500.f && target->unit->GetPos() == nullpos);
}

springai::Unit* Bomber::getUnit(){ return unit; }

springai::AIFloat3 Bomber::getPos(){ return unit->GetPos(); }

bool Bomber::isDead() const {
    return unit->GetHealth() <= 0 || unit->GetTeam() != team;
}

}
