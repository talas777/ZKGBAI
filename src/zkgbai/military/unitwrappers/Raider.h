#ifndef _ZKGBAI_RAIDER_H
#define _ZKGBAI_RAIDER_H

#include "Fighter.h"

namespace zkgbai {

class ZKGBAI;
class ScoutTask;
class RaiderSquad;

class Raider: public Fighter {
public:
    Raider(ZKGBAI* ai, springai::Unit* u, float metal);
    virtual ~Raider();

    virtual void setTask(ScoutTask& t);
    virtual void clearTask();
    virtual void endTask();
    virtual ScoutTask* getTask();

    virtual std::queue<springai::AIFloat3> getRaidPath(const springai::AIFloat3& pos);

    virtual void raid(const springai::AIFloat3& target, int frame);
    virtual void sneak(const springai::AIFloat3& target, int frame);
    virtual bool isReloading(int frame);
    virtual bool unstick(int frame);

    int lastTaskFrame = 0;
    int lastRotationFrame = 0;

private:
    ScoutTask* task = nullptr;
};
}
#endif //_ZKGBAI_RAIDER_H
