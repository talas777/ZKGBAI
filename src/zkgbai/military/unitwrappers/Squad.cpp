#include "Squad.h"

#include <algorithm>

#include "ZKGBAI.h"

namespace zkgbai {

Squad::Squad(ZKGBAI* ai) : team(ai->teamID){}

Squad::~Squad(){}

void Squad::addUnit(Fighter& f){
    fighters.push_back(&f);
    f.squad = this;
    f.index = index;
    index++;
    metalValue += f.metalValue;
    f.getUnit()->SetMoveState(1, 0);
    if(leader == nullptr) leader = &f;
}

void Squad::removeUnit(Fighter& f){
    auto it=std::find_if(fighters.begin(), fighters.end(), [f](Fighter*g){return*g==f;});
    if(it != fighters.end()){
      (void)fighters.erase(it);
    }
    if(leader != nullptr && *leader == f){
      leader = getNewLeader();
    }
    metalValue -= f.metalValue;
    f.squad = nullptr;
}

int Squad::getSize(){
    return fighters.size();
}

void Squad::setTarget(const springai::AIFloat3& pos){
    // set a target for the squad to attack.
    setpos(target, pos);

    float maxdist = 200.f;
    float waydist = 100.f;
    if(isAirSquad){
      maxdist = 300.f;
      waydist = 150.f;
    }

    std::queue<springai::AIFloat3> path = leader->getFightPath(pos);
    if(path.empty()) return; // 74145: TODO: say something?
    if(isAirSquad && path.size() > 1) path.pop();
    if(path.size() > 1) path.pop();
    springai::AIFloat3 waypoint(path.front());
    path.pop();
    for(Fighter* f : fighters){
      setpos(f->target, target);
      float fdist = distance(f->getPos(), leader->getPos());
      if(fdist > maxdist){
        f->outOfRange = true;
        if(fdist > 2.f * maxdist){
          f->moveTo(getAngularPoint(leader->getPos(), f->getPos(), lerp(waydist, maxdist, kgb_rand())));
        } else {
          f->getUnit()->MoveTo(getAngularPoint(leader->getPos(), f->getPos(), lerp(waydist, maxdist, kgb_rand())), 0);
        }
      } else {
        f->outOfRange = false;
        f->getUnit()->Fight(getFormationPoint(f->getPos(), leader->getPos(), waypoint), 0);
      }
    }

    // add one extra waypoint.
    if(path.empty()) return;
    if(path.size() > 1) path.pop();
    if(isAirSquad && path.size() > 1) path.pop();
    setpos(waypoint, path.front());
    for(Fighter* f : fighters){
      if(f->getUnit()->GetHealth() <= 0 || f->outOfRange) continue;
      f->getUnit()->Fight(getFormationPoint(f->getPos(), leader->getPos(), waypoint), OPTION_SHIFT_KEY);
    }
}

void Squad::retreatTo(const springai::AIFloat3& pos){
    setpos(target, pos);
    for(Fighter* f : fighters){
      f->moveTo(target);
      setpos(f->target, target);
    }
}

springai::AIFloat3 Squad::getPos() const {
    float maxdist = isAirSquad ? 300.f: 200.f;
    if(fighters.empty()) return target;
    int count = 0;
    float x=0,z=0;
    for(Fighter* f : fighters){
      if(distance(f->getPos(), leader->getPos()) > maxdist) continue;
      x += f->getPos().x;
      z += f->getPos().z;
      count++;
    }
    springai::AIFloat3 pos;
    x = x/count;
    z = z/count;
    pos.x = x;
    pos.z = z;
    return pos; 
}

bool Squad::isRallied(int frame){
    if(firstRallyFrame == 0){
      firstRallyFrame = frame;
    }
    if(frame - firstRallyFrame > 900){
      return true;
    }
    setTarget(target);
    for(Fighter* f : fighters) if(f->outOfRange) return false;
    return true;
}

bool Squad::isDead() const {
    return fighters.empty();
}

void Squad::cutoff(){
    if(fighters.size() < 3 && metalValue < 1000.f){
      for(Fighter* f : fighters) f->squad = nullptr;
      fighters.clear();
      leader = nullptr;
    }
}

springai::Unit* Squad::getLeader() const {
    if(leader == nullptr) return nullptr;
    return leader->getUnit();
}

int Squad::getThreat(){
    float threat=0;
    for(Fighter* f : fighters){
      threat += f->power + f->getUnit()->GetMaxHealth();
    }
    return (int) (threat/10.f);
}

Fighter* Squad::getBestLeader(){
    if(leader == nullptr || leader->getUnit()->GetHealth()<=0 || leader->getUnit()->GetTeam()!=team) leader=getNewLeader();
    if(leader == nullptr) return nullptr;
    Fighter* newLeader = leader;
    float minspeed = leader->getUnit()->GetDef()->GetSpeed();
    for(Fighter* f : fighters){
      if(distance(f->getPos(), leader->getPos()) > 300.f) continue;
      float tmpspeed = f->getUnit()->GetSpeed();
      if(tmpspeed < minspeed){
        minspeed=tmpspeed; newLeader=f;
      }
    } return newLeader;
}

Fighter* Squad::getNewLeader(){
    Fighter* newLeader = nullptr;
    int tmpindex;
    for(Fighter* f : fighters){
      if(newLeader==nullptr || f->index < tmpindex){
        newLeader=f; tmpindex=f->index;
      }
    } return newLeader;
}

}
