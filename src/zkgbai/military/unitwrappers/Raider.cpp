#include "Raider.h"

// generated by the C++ Wrapper scripts
#include "Weapon.h"

#include "../tasks/ScoutTask.h"
//#include "RaiderSquad.h"

#include "kgbutil/Pathfinder.h"

namespace zkgbai {

Raider::Raider(ZKGBAI* ai, springai::Unit* u, float metal) : Fighter(ai, u, metal){
    this->task = nullptr;
}

Raider::~Raider(){
    if(task!=nullptr){
      ScoutTask* t=task; task=nullptr;
      t->removeRaider(*this);
    }
}

void Raider::setTask(ScoutTask& t){
    if(task!=nullptr) task->removeRaider(*this);
    task = &t;
}

void Raider::clearTask(){
    if(task != nullptr) task->removeRaider(*this);
    if(unit->GetHealth() > 0 && unit->GetTeam() == team) unit->Stop(0);
    task = nullptr;
}

void Raider::endTask(){
    if(unit->GetHealth() > 0 && unit->GetTeam() == team) unit->Stop(0);
    task = nullptr;
}

ScoutTask* Raider::getTask(){
    return task;
}

std::queue<springai::AIFloat3> Raider::getRaidPath(const springai::AIFloat3& pos){
    return pathfinder->findPath(unit, pos, unit->IsCloaked() ? pathfinder->SCYTHE_PATH : pathfinder->RAIDER_PATH);
}

void Raider::raid(const springai::AIFloat3& pos, int frame){
    std::queue<springai::AIFloat3> path =
        pathfinder->findPath(unit, getRadialPoint(pos, 100.f), unit->IsCloaked() ? pathfinder->SCYTHE_PATH : pathfinder->RAIDER_PATH);
    if(path.empty()) return; // 74145: TODO: say something?
    bool flier = unit->GetDef()->IsAbleToFly();
    if(flier && path.size() > 1) path.pop();
    if(flier && path.size() > 1) path.pop();
    if(path.size() > 1) path.pop();
    unit->Fight(path.front(), 0);
    path.pop();
    if(flier && path.size() > 1) path.pop();
    if(flier && path.size() > 1) path.pop();
    if(!path.empty()) unit->Fight(path.front(), OPTION_SHIFT_KEY);

    setpos(this->target, pos);
}

void Raider::sneak(const springai::AIFloat3& pos, int frame){
    std::queue<springai::AIFloat3> path =
        pathfinder->findPath(unit, getRadialPoint(pos, 100.f), unit->IsCloaked() ? pathfinder->SCYTHE_PATH : pathfinder->RAIDER_PATH);
    if(path.empty()) return; // 74145: TODO: say something?
    if(path.size() > 1) path.pop();
    unit->MoveTo(path.front(), 0);
    path.pop();
    if(!path.empty()) unit->MoveTo(path.front(), OPTION_SHIFT_KEY);

    setpos(this->target, pos);
}

bool Raider::isReloading(int frame){
    int lastWepFrame = 0;
    for(springai::Weapon* w : unit->GetWeapons()){
      int reload = w->GetReloadFrame();
      if(reload > lastWepFrame) lastWepFrame = reload;
    } return lastWepFrame > frame;
}

bool Raider::unstick(int frame){
    if(unit->GetHealth() <= 0) return false;

    float speed = getSpeed(unit);
    int lastWepFrame = 0;
    for(springai::Weapon* w : unit->GetWeapons()){
      int reload = w->GetReloadFrame();
      if(reload > lastWepFrame) lastWepFrame = reload;
    }

    if(lastWepFrame < frame - 30 && speed < unit->GetDef()->GetSpeed()/10.f && !unit->IsParalyzed()){
      stuck++;
      if(stuck == 15) return true;
      clearTask();
      unit->MoveTo(getRadialPoint(unit->GetPos(), 100.f), 0);
    } else {
      stuck = std::max(0, stuck - 2);
    }
    return false;
}

}
