#ifndef _ZKGBAI_KROW_H
#define _ZKGBAI_KROW_H

#include "Strider.h"

namespace zkgbai {

class Krow: public Strider {
public:
    Krow(ZKGBAI* ai, springai::Unit* u, float metal);

    bool isDgunReadyOrFiring(int frame);
    void flyTo(const springai::AIFloat3& pos, int frame);
};
}
#endif //_ZKGBAI_KROW_H
