#ifndef _ZKGBAI_HOVERSQUAD_H
#define _ZKGBAI_HOVERSQUAD_H

#include "Squad.h"

namespace zkgbai {

class ZKGBAI;

class HoverSquad: public Squad {
public:
    HoverSquad(ZKGBAI* ai);
    virtual ~HoverSquad();

    virtual void setTarget(const springai::AIFloat3& pos) override;
};
}
#endif //_ZKGBAI_HOVERSQUAD_H
