#include "HoverSquad.h"

#include <queue>

#include "Fighter.h"

#include "kgbutil/KgbUtil.h"

namespace zkgbai {

HoverSquad::HoverSquad(ZKGBAI* ai) : Squad(ai){}

HoverSquad::~HoverSquad(){}

void HoverSquad::setTarget(const springai::AIFloat3& pos){
    // set a target for the squad to attack.
    setpos(target, pos);
    float maxdist = 150.f;
    float waydist = 75.f;
    bool farAway = distance(leader->getPos(), target) > 450.f;

    std::queue<springai::AIFloat3> path = leader->getFightPath(pos);
    if(path.empty()) return; // 74145: TODO: say something?
    if(path.size() > 1) path.pop();
    if(path.size() > 1) path.pop();
    const springai::AIFloat3 waypoint(path.front());

    for(Fighter* f : fighters){
      float fdist = distance(leader->getPos(), f->getPos());
      if(fdist > maxdist){
        f->outOfRange = true;
        if(fdist > 2.f * maxdist){
          f->moveTo(getAngularPoint(leader->getPos(), f->getPos(), lerp(waydist, maxdist, kgb_rand())));
        } else {
          f->getUnit()->MoveTo(getAngularPoint(leader->getPos(), f->getPos(), lerp(waydist, maxdist, kgb_rand())), 0);
        }
      } else {
        f->outOfRange = false;
        if(farAway){
          f->getUnit()->MoveTo(waypoint, 0);
        } else {
          f->getUnit()->Fight(waypoint, 0);
        }
      }
    }
}

}
