#include "MilitaryManager.h"

#include <algorithm>

#include "ZKGBAI.h"
#include "Enemy.h"

//#define ZKGBAI_MILPERF

#ifdef ZKGBAI_MILPERF
#include <chrono>
#include <iostream>
#define TT_START() auto start = std::chrono::steady_clock::now();
#define TT_STOP(x) auto stop = std::chrono::steady_clock::now();\
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);\
    if(duration.count() > 1000) std::cout << (x) << " took: " << duration.count()/1000.0 << "ms." << std::endl;
#else
#define TT_START() ;
#define TT_STOP(x) ;
#endif

#include <iostream>
#include <fstream>

namespace zkgbai {

MilitaryManager::MilitaryManager(ZKGBAI* zkgbai, float w, float h) :
    ai(zkgbai), callback(zkgbai->getCallback()), width(w), height(h),
    radarID(zkgbai->getCallback()),
    threatGraphics(w, h),
    enemyPorcGraphics(w, h),
    allyThreatGraphics(w, h),
    allyRaiderGraphics(w, h),
    allyPorcGraphics(w, h),
    aaThreatGraphics(w, h),
    aaPorcGraphics(w, h),
    valueGraphics(w, h),
    raiderValueGraphics(w, h),
    bpGraphics(w, h),
    riotGraphics(w, h),
    scytheGraphics(w, h)
{
    this->nano = callback->GetUnitDefByName("staticcon")->GetUnitDefId();
    this->unitTypes = UnitClasses::getInstance();
    this->m = callback->GetResourceByName("Metal");

    allyPorcGraphics.clear();
    aaPorcGraphics.clear();
}

MilitaryManager::MilitaryManager(ZKGBAI* ai) :
    MilitaryManager(ai, ai->getCallback()->GetMap()->GetWidth()/8, ai->getCallback()->GetMap()->GetWidth()/8){}

MilitaryManager::~MilitaryManager(){
    if(!initialized) return;
    delete retreatHandler;
    delete raiderHandler;
    delete squadHandler;
    delete bomberHandler;
    delete miscHandler;
    for(DefenseTarget* d : defenseTargets) delete d;
    for(DefenseTarget* d : airDefenseTargets) delete d;
    for(DefenseTarget* d : sniperSightings) delete d;
    for(auto &[k,h] : hawks) delete h;
    for(auto &[k,a] : AAs) delete a;
    for(auto &[k,e] : targets) delete e;
}

int MilitaryManager::init(int AIID, springai::OOAICallback* cb){
    initialized = true;
    this->losManager = ai->losManager;
    this->graphManager = ai->graphManager;

    this->pathfinder = ai->pathfinder;

    this->retreatHandler = new RetreatHandler(ai);
    this->raiderHandler = new RaiderHandler(ai);
    this->squadHandler = new SquadHandler(ai);
    this->bomberHandler = new BomberHandler(ai);
    this->miscHandler = new MiscHandler(ai);

    retreatHandler->init();

    wolvMineID = callback->GetUnitDefByName("wolverine_mine")->GetUnitDefId();
    wolvMineBomblet = callback->GetWeaponDefByName("wolverine_mine_bomblet")->GetWeaponDefId();
    sniperBulletID = callback->GetWeaponDefByName("cloaksnipe_shockrifle")->GetWeaponDefId();
    brawlerBulletID = callback->GetWeaponDefByName("gunshipheavyskirm_emg")->GetWeaponDefId();

    return 0;
}

int MilitaryManager::update(int frame){
    this->frame = frame;

    if(frame % 15 == (ai->offset + 3) % 15){ TT_START() updateTargets(); TT_STOP("updateTargets") }
    if(frame % 7 == (ai->offset + 2) % 7){ TT_START() paintThreatMap(); TT_STOP("paintThreatMap") }

    { TT_START()
      retreatHandler->update(frame);
      TT_STOP("reatreatHandler") }
    { TT_START()
      raiderHandler->update(frame);
      TT_STOP("raiderHandler") }
    { TT_START()
      squadHandler->update(frame);
      TT_STOP("squadHandler") }
    { TT_START()
      miscHandler->update(frame);
      TT_STOP("miscHandler") }
    { TT_START()
      bomberHandler->update(frame);
      TT_STOP("bomberHandler") }

    if(frame % 22 == ai->offset % 22 && !AAs.empty()){
      std::vector<int> dead;
      for(auto &[k,r] : AAs){
        if(r->getUnit()->GetHealth() <= 0 || r->getUnit()->GetTeam() != ai->teamID){
          dead.push_back(r->id);
          continue;
        }
        if(retreatHandler->isRetreating(r->getUnit())){
          continue;
        }
        if(getPorcThreat(r->getPos()) > 0 || getEffectiveThreat(r->getPos()) > 0){
          if(r->getUnit()->GetDef()->IsAbleToFly()){
            r->sneak(graphManager->getClosestAirHaven(r->getPos()), frame);
          } else {
            r->sneak(graphManager->getClosestHaven(r->getPos()), frame);
          }
        } else {
          r->raid(getAngularPoint(getAATarget(*r), r->getPos(), 350.f), frame);
        }
      }
      for(int id : dead){
        AAs.erase(id);
      }
    }

    return 0; // signaling: OK
}

int MilitaryManager::unitFinished(springai::Unit* unit){
    springai::UnitDef* ud = unit->GetDef();
    std::string defName(ud->GetName());

    if(defName == "planelightscout" || defName == "planescout"){
      unit->SetIdleMode(0, 0);
      unit->SetMoveState(1, 0);
      raiderHandler->addSparrow({ai, unit, ud->GetCost(m)});
    }

    if(CONTAINSS(unitTypes->bombers,defName)){
      unit->SetIdleMode(0, 0);
      unit->SetMoveState(0, 0);
      unit->SetFireState(0, 0);
      bomberHandler->addBomber({ai, unit});
    }

    // Deactivate outlaws so they don't destroy reclaim
    if(defName == "shieldriot"){
      std::vector<float> params;
      params.push_back(0.f);
      unit->ExecuteCustomCommand(CMD_WANT_ONOFF, params, 0);
    }

    if(defName == "staticnuke"){
      hasNuke = true;
      miscHandler->addNuke(unit);
    }

    if(defName == "zenith"){
      miscHandler->addZenith(unit);
    }

    if(defName == "raveparty"){
      miscHandler->addDRP(unit);
    }

    if(defName == "staticheavyarty"){
      miscHandler->addBertha(unit);
    }

    if(defName == "striderantiheavy"){
      unit->SetMoveState(0, 0);
      unit->SetFireState(1, 0);
      miscHandler->addUlti({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->striders,defName)){
      unit->SetMoveState(1, 0);
      miscHandler->addStrider({ai, unit, ud->GetCost(m)});
    } else if(ud->GetSpeed() > 0 && ud->GetBuildOptions().empty()){
      newUnits.insert(unit->GetUnitId());
    }

    if(ud->GetSpeed() > 0 && ud->GetBuildOptions().empty()
        && !CONTAINSS(unitTypes->noRetreat,defName)){
      retreatHandler->addCoward(unit);
    }

    return 0;
}

int MilitaryManager::unitIdle(springai::Unit* unit){
    if(newUnits.find(unit->GetUnitId()) != newUnits.end()){
      newUnits.erase(unit->GetUnitId());
    } else {
      return 0;
    }

    springai::UnitDef* ud = unit->GetDef();
    std::string defName(ud->GetName());

    // enable snipers and penetrators to shoot radar dots
    if(defName == "cloaksnipe" || defName == "hoverarty"){
      std::vector<float> params;
      params.push_back(0.f);
      unit->ExecuteCustomCommand(CMD_DONT_FIRE_AT_RADAR, params, 0);
      unit->SetMoveState(1, 0);
    }

    // disable air strafe for brawlers
    if(defName == "gunshipheavyskirm"){
      std::vector<float> params;
      params.push_back(0.f);
      unit->ExecuteCustomCommand(CMD_AIR_STRAFE, params, 0);
    }

    // set blastwings to land when idle
    if(defName == "gunshipbomb"){
      unit->SetIdleMode(1, 0);
    }

    if(CONTAINSS(unitTypes->planes,defName)){
      unit->SetIdleMode(0, 0);
    }

    if(defName == "planefighter"){
      unit->SetMoveState(0, 0);
      miscHandler->addSwift(unit);
    }

    if(defName == "planeheavyfighter"){
      unit->SetMoveState(1, 0);
      Raider* f = new Raider(ai, unit, ud->GetCost(m));
      hawks[f->id] = f;
      if(hawks.size() > 4){
        AAs.merge(hawks);
      }
    }

    if(defName == "gunshipkrow"){
      unit->SetMoveState(1, 0);
      miscHandler->addKrow({ai, unit, ud->GetCost(m)});
    }

    if(CONTAINSS(unitTypes->smallRaiders,defName)){
      if(defName == "amphbomb"){
        unit->SetMoveState(2, 0);
        unit->SetFireState(2, 0);
        std::vector<float> params;
        params.push_back(0.f);
        unit->ExecuteCustomCommand(CMD_UNIT_AI, params, 0);
      }
      unit->SetMoveState(1, 0);
      raiderHandler->addSmallRaider({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->mediumRaiders,defName)){
      unit->SetMoveState(1, 0);
      raiderHandler->addMediumRaider({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->assaults,defName)){
      unit->SetMoveState(1, 0);
      squadHandler->addAssault({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->shieldMobs,defName)){
      squadHandler->addShieldMob({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->mobSupports,defName)){
      miscHandler->addSupport({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->loners,defName)){
      if(defName == "vehsupport" || defName == "spidercrabe"){
        unit->SetMoveState(0, 0);
        std::vector<float> params;
        params.push_back(1.f);
        unit->ExecuteCustomCommand(CMD_UNIT_AI, params, 0);
      } else {
        unit->SetMoveState(1, 0);
      }
      miscHandler->addLoner({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->arties,defName)){
      if(defName == "vehheavyarty" || defName == "tankarty" || defName == "striderarty" || defName == "tankheavyarty"){
        unit->SetMoveState(0, 0);
        std::vector<float> params;
        params.push_back(1.f);
        unit->ExecuteCustomCommand(CMD_UNIT_AI, params, 0);
      } else {
        unit->SetMoveState(1, 0);
      }
      miscHandler->addArty({ai, unit, ud->GetCost(m)});
    } else if(CONTAINSS(unitTypes->AAs,defName)){
      unit->SetMoveState(2, 0);
      Raider* f = new Raider(ai, unit, ud->GetCost(m));
      AAs[f->id] = f;
      springai::AIFloat3 pos(graphManager->getAllyCenter());
      if(pos != nullpos){
        unit->Fight(pos, 0);
      }
    } else if(CONTAINSS(unitTypes->sappers,defName)){
      unit->SetMoveState(2, 0);
      unit->SetFireState(2, 0);
      std::vector<float> params;
      params.push_back(0.f);
      unit->ExecuteCustomCommand(CMD_UNIT_AI, params, 0);
      unit->Fight(getRadialPoint(graphManager->getAllyCenter(), 800.f), 0);
      miscHandler->addSapper(unit);
    }

    return 0; // signaling: OK
}

void MilitaryManager::addDefenseTarget(DefenseTarget && d0){
    DefenseTarget* d = new DefenseTarget(std::move(d0));
    defenseTargets.push_back(d);
}

int MilitaryManager::unitDamaged(springai::Unit* h, springai::Unit* attacker, float damage, const springai::AIFloat3& dir, springai::WeaponDef* weaponDef, bool paralyzed){
    if(h->GetDef()->GetUnitDefId() == wolvMineID || (weaponDef != nullptr && weaponDef->GetWeaponDefId() == wolvMineBomblet)){
      return 0;
    }

    // check if the damaged unit is on fire.
    bool on_fire = h->GetRulesParamFloat("on_fire", 0.0f) > 0;

    // retreat scouting raiders so that they don't suicide into enemy raiders
    if(h->GetMaxSpeed() > 0){
      retreatHandler->checkUnit(h, on_fire);
      if(!on_fire) raiderHandler->avoidEnemies(h, attacker, dir);
    }

    // create a defense task, if appropriate.
    if((attacker == nullptr || attacker->GetDef() == nullptr || attacker->GetPos() == nullpos
        || (attacker->GetDef() == nullptr && !losManager->isInLos(attacker->GetPos()))
        || (h->GetDef() != nullptr && h->GetDef()->GetBuildSpeed() > 0 && h->GetDef()->GetSpeed() == 0))
        && weaponDef != nullptr && (frame - lastDefenseFrame > 30 || weaponDef->GetWeaponDefId() == sniperBulletID)){
      DefenseTarget* dt = nullptr;

      // only create defense targets on unitDamaged if the attacker is invisible or out of los.
      if(!CONTAINSI(unitTypes->porcWeps,weaponDef->GetWeaponDefId())){
        springai::AIFloat3 target;
        if(attacker == nullptr || attacker->GetPos() == nullpos){
          float dist = weaponDef->GetRange();
          float x = dist * dir.x;
          float z = dist * dir.z;
          springai::AIFloat3 pos(h->GetPos());
          target.x = pos.x + x;
          target.z = pos.z + z;
          if(losManager->isInLos(target)) return 0;
        } else {
          setpos(target, attacker->GetPos());
        }
        if(h->GetDef() != nullptr && h->GetDef()->GetBuildSpeed() > 0 && h->GetDef()->GetSpeed() == 0){
          dt = new DefenseTarget(target, h->GetMaxHealth(), frame);
        } else {
          dt = new DefenseTarget(target, weaponDef->GetRange(), frame);
        }
      }

      if(dt != nullptr){
        lastDefenseFrame = frame;
        if(weaponDef != nullptr && weaponDef->GetWeaponDefId() == sniperBulletID){
          defenseTargets.push_back(dt);
          sniperSightings.push_back(new DefenseTarget(dt->position, dt->damage, dt->frameIssued));
        } else if(weaponDef != nullptr && weaponDef->GetWeaponDefId() == brawlerBulletID){
          airDefenseTargets.push_back(dt);
        } else {
          defenseTargets.push_back(dt);
        }
      }
    }
    return 0;
}

int MilitaryManager::unitDestroyed(springai::Unit* unit, springai::Unit* attacker){
    std::string defName(unit->GetDef()->GetName());
    if(defName == "wolverine_mine"){
      return 0;
    }

    if(newUnits.find(unit->GetUnitId()) != newUnits.end()){
      newUnits.erase(unit->GetUnitId());
    }

    retreatHandler->removeUnit(unit);
    raiderHandler->removeUnit(unit);
    squadHandler->removeUnit(unit);
    bomberHandler->removeUnit(unit);
    miscHandler->removeUnit(unit);

    if(defName == "staticnuke"){
      hasNuke = false;
    }

    { auto it = AAs.find(unit->GetUnitId()); if(it != AAs.end()){ delete (*it).second; AAs.erase(it); }}
    { auto it = hawks.find(unit->GetUnitId()); if(it != hawks.end()){ delete (*it).second; hawks.erase(it); }}

    if(CONTAINSS(unitTypes->sappers,defName)){
      // rally units to fight if a tick/roach etc dies.
      DefenseTarget* dt = new DefenseTarget(unit->GetPos(), 1500.f, frame);
      defenseTargets.push_back(dt);
    }

    // Unpaint ally threat for porc
    if(unit->GetDef()->GetSpeed() == 0 && unit->GetDef()->IsAbleToAttack() && defName != "staticheavyarty"){
      if(unbuiltPorcs.find(unit->GetUnitId()) != unbuiltPorcs.end()){
        unbuiltPorcs.erase(unit->GetUnitId());
      } else {
        int power = (int) ((unit->GetPower() + unit->GetMaxHealth()) / 10);
        float radius = unit->GetMaxRange();
        springai::AIFloat3 pos(unit->GetPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = std::round(radius / 64.f);

        allyPorcGraphics.unpaintCircle(x, y, rad, power);
      }
    }

    return 0; // signaling: OK
}

int MilitaryManager::unitCaptured(springai::Unit* unit, int oldTeamID, int newTeamID){
    (void)newTeamID;
    if(oldTeamID == ai->teamID){
      return unitDestroyed(unit, nullptr);
    }
    return 0;
}

int MilitaryManager::unitGiven(springai::Unit* unit, int oldTeamID, int newTeamID){
    // remove units that allies captured from enemy targets
    auto it = targets.find(unit->GetUnitId());
    if(it != targets.end()){
      Enemy* const e = (*it).second;
      bomberHandler->removeTarget(e);
      enemyPorcs.erase((*it).first);
      delete e; (void)targets.erase(it);
    }
    if(newTeamID == ai->teamID){
      unitCreated(unit, nullptr);
      unitFinished(unit);
      unitIdle(unit);
    }
    return 0;
}

int MilitaryManager::enemyEnterLOS(springai::Unit* enemy){
    auto it = targets.find(enemy->GetUnitId());
    if(it != targets.end()){
      Enemy* const e = (*it).second;
      if(enemy->GetDef()->GetUnitDefId() == wolvMineID){
        bomberHandler->removeTarget(e);
        delete e;
        (void)targets.erase(it);
        return 0;
      }
      e->visible = true;
      e->lastSeen = frame;
      e->checkNano();
      if(!e->identified){
        e->setIdentified();
        e->updateFromUnitDef(enemy->GetDef(), enemy->GetDef()->GetCost(m));
        if(e->isPorc && !e->isNanoSpam){
          enemyPorcs[enemy->GetUnitId()] = e;
        }
      }

      // paint enemy threat for aa
      if(e->isStatic && (e->isAA || e->isFlexAA) && !e->isPainted && !e->unit->IsBeingBuilt()){
        int effectivePower = (int) e->getDanger();
        springai::AIFloat3 position(e->position);
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int r = std::round((e->threatRadius) / 64.f);

        aaPorcGraphics.paintCircle(x, y, r + 1, effectivePower);
        e->isPainted = true;
      }
    } else if(enemy->GetDef()->GetUnitDefId() != wolvMineID){
      Enemy* e = new Enemy(enemy, enemy->GetDef()->GetCost(m));
      targets[enemy->GetUnitId()] = e;
      e->visible = true;
      e->setIdentified();
      e->lastSeen = frame;
      e->checkNano();

      if(e->isPorc && !e->isNanoSpam){
        enemyPorcs[enemy->GetUnitId()] = e;
      }

      if(e->isStatic && e->isAA && !e->isPainted && !e->unit->IsBeingBuilt()){
        int effectivePower = (int) e->getDanger();
        springai::AIFloat3 position(e->position);
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int r = std::round((e->threatRadius) / 64.f);

        aaPorcGraphics.paintCircle(x, y, r + 1, effectivePower);
        e->isPainted = true;
      }
    }
    return 0; // signaling: OK
}

int MilitaryManager::enemyFinished(springai::Unit* enemy){
    if(enemy->GetDef() == nullptr){
      return 0;
    }

    if(targets.find(enemy->GetUnitId()) != targets.end()){
      Enemy* e = targets[enemy->GetUnitId()];
      e->checkNano();
      if(!e->identified){
        e->setIdentified();
        e->updateFromUnitDef(enemy->GetDef(), enemy->GetDef()->GetCost(m));
        if(e->isPorc && !e->isNanoSpam){
          enemyPorcs[enemy->GetUnitId()] = e;
        }
      }
      // paint enemy threat for aa
      if(e->isStatic && (e->isAA || e->isFlexAA) && !e->isPainted){
        int effectivePower = (int) e->getDanger();
        springai::AIFloat3 position(e->position);
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int r = std::round((e->threatRadius) / 64.f);

        aaThreatGraphics.paintCircle(x, y, r + 1, effectivePower);
        e->isPainted = true;
      }
    } else if(enemy->GetDef()->GetUnitDefId() != wolvMineID){
      Enemy* e = new Enemy(enemy, enemy->GetDef()->GetCost(m));
      targets[enemy->GetUnitId()] = e;
      e->visible = true;
      e->setIdentified();
      e->lastSeen = frame;

      if(e->isPorc && !e->isNanoSpam){
        enemyPorcs[enemy->GetUnitId()] = e;
      }
    }
    return 0;
}

int MilitaryManager::enemyLeaveLOS(springai::Unit* enemy){
    if(targets.find(enemy->GetUnitId()) != targets.end()){
      targets[enemy->GetUnitId()]->visible = false;
      targets[enemy->GetUnitId()]->lastSeen = frame;
    }
    return 0; // signaling: OK
}

int MilitaryManager::enemyEnterRadar(springai::Unit* enemy){
    if(targets.find(enemy->GetUnitId()) != targets.end()){
      targets[enemy->GetUnitId()]->isRadarVisible = true;
      targets[enemy->GetUnitId()]->lastSeen = frame;
    } else {
      Enemy* e = new Enemy(enemy, 50);
      targets[enemy->GetUnitId()] = e;
      e->isRadarVisible = true;
      e->lastSeen = frame;
    }
    return 0; // signaling: OK
}

int MilitaryManager::enemyLeaveRadar(springai::Unit* enemy){
    if(targets.find(enemy->GetUnitId()) != targets.end()){
      targets[enemy->GetUnitId()]->isRadarVisible = false;
      targets[enemy->GetUnitId()]->lastSeen = frame;
    }
    return 0; // signaling: OK
}

int MilitaryManager::enemyDestroyed(springai::Unit* unit, springai::Unit* attacker){
    auto it = targets.find(unit->GetUnitId());
    if(it != targets.end()){
      Enemy* const e = (*it).second;
      e->isDead = true;
      if(e->identified && e->ud != nullptr && !e->isNanoSpam){
        std::string defName(e->ud->GetName());
        if(defName == "energysingu" || defName == "energyheavygeo"){
          if(!unit->IsBeingBuilt() || unit->GetBuildProgress() > 0.8) ai->say("SHINY!");
        }
      }

      // Unpaint enemy threat for statics
      if(e->isStatic && (e->isAA || e->isFlexAA) && e->isPainted){
        int effectivePower = (int) e->getDanger();
        springai::AIFloat3 position(e->position);
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int r = std::round((e->threatRadius) / 64.f);
        aaPorcGraphics.unpaintCircle(x, y, r + 1, effectivePower);
      }

      bomberHandler->removeTarget(e);
      enemyPorcs.erase((*it).first);
      delete e;
      targets.erase(it);
    }
    return 0; // signaling: OK
}

void MilitaryManager::paintValueMap(std::string wepName){
    valueGraphics.clear();
    int r, rr;
    if(wepName == "staticnuke"){
      r = 14; // general superwep kill radius
      rr = 20;
    } else {
      r = 10;
      rr = 13;
    }
    for(auto &[k,t] : targets){
      springai::AIFloat3 position(t->position);
      if(position != nullpos && t->identified && t->ud != nullptr && !t->isNanoSpam && !t->ud->IsAbleToFly()){
        std::string defName(t->ud->GetName());
        if(defName != "turretaalaser" && defName != "turretgauss"){
          int x = std::round(position.x / 64.f);
          int y = std::round(position.z / 64.f);
          int value = (int) std::min(t->ud->GetCost(m)/10.f, 750.f);
          valueGraphics.paintCircle(x, y, r, value);
        }
      }
    }

    for(Squad* s : squadHandler->squads){
      if(!s->isDead()){
        springai::AIFloat3 position(s->getPos());
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int value = (int) (s->metalValue/10.f);
        valueGraphics.unpaintCircle(x, y, rr, value);
      }
    }

    for(auto &[k,s] : miscHandler->striders){
      springai::AIFloat3 position(s->getPos());
      int x = std::round(position.x / 64.f);
      int y = std::round(position.z / 64.f);
      int value = (int) std::min(s->metalValue/10.f, 750.f);
      valueGraphics.unpaintCircle(x, y, rr, value);
    }

    for(ShieldSquad* s : squadHandler->shieldSquads){
      if(!s->isDead()){
        springai::AIFloat3 position(s->getPos());
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int value = (int) (s->metalValue / 10.f);
        valueGraphics.unpaintCircle(x, y, r, value);
      }
    }
}

void MilitaryManager::paintThreatMap(){
    if(frame % 14 == (ai->offset + 2) % 14){
      allyThreatGraphics.clear();
      allyRaiderGraphics.clear();
      availableMobileThreat = 0;
      availableRaiderThreat = 0;
      // paint allythreat for raiders
      for(Raider* r : raiderHandler->soloRaiders){
        if(r->getUnit()->GetHealth() <= 0 || r->getUnit()->GetTeam() != ai->teamID) continue;
        int power = (int) ((r->power + r->getUnit()->GetMaxHealth()) / 10.f);
        availableRaiderThreat += power;
        springai::AIFloat3 pos(r->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 7;
        allyThreatGraphics.paintCircle(x, y, rad, power);
        allyRaiderGraphics.paintCircle(x, y, rad, power);
      }

      for(Raider* r : raiderHandler->kodachis){
        if(r->getUnit()->GetHealth() <= 0 || r->getUnit()->GetTeam() != ai->teamID) continue;
        int power = (int) ((r->power + r->getUnit()->GetMaxHealth()) / 10.f);
        springai::AIFloat3 pos(r->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 7;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      for(RaiderSquad* rs : raiderHandler->raiderSquads){
        if(rs->isDead()) continue;
        int power = rs->getThreat();
        if(rs->status != 'a'){
          availableMobileThreat += power;
        } else {
          availableRaiderThreat += power;
        }
        springai::AIFloat3 pos(rs->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = rs->type == 's' ? 8 : 10;
        allyThreatGraphics.paintCircle(x, y, rad, power);
        if(rs->status != 'f') allyRaiderGraphics.paintCircle(x, y, rad, power);
      }

      // paint allythreat for squads
      for(Squad* s : squadHandler->squads){
        if(s->isDead()) continue;
        int power = s->getThreat();
        availableMobileThreat += power;
        springai::AIFloat3 pos(s->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 10;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      for(Squad* s : squadHandler->airSquads){
        if(s->isDead()) continue;
        int power = s->getThreat();
        availableMobileThreat += power;
        springai::AIFloat3 pos(s->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 13;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      for(ShieldSquad* s : squadHandler->shieldSquads){
        if(s->isDead()) continue;
        int power = s->getThreat();
        availableMobileThreat += power;
        springai::AIFloat3 pos(s->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 10;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      // paint allythreat for loners
      for(auto &[k,f] : miscHandler->loners){
        if(f->isDead()) continue;
        int power = (int) ((f->power + f->getUnit()->GetMaxHealth()) / 10.f);
        if(!retreatHandler->isRetreating(f->getUnit())) availableMobileThreat += power;
        springai::AIFloat3 pos(f->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 10;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      // paint allythreat for striders
      for(auto &[k,s] : miscHandler->striders){
        if(s->getUnit()->GetHealth() <= 0 || s->getUnit()->GetTeam() != ai->teamID) continue;
        int power = (int) ((s->power + s->getUnit()->GetMaxHealth()) / 10.f);
        if(!retreatHandler->isRetreating(s->getUnit())) availableMobileThreat += power;
        springai::AIFloat3 pos(s->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 15;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      // paint allythreat for krows
      for(auto &[k,s] : miscHandler->krows){
        if(s->getUnit()->GetHealth() <= 0 || s->getUnit()->GetTeam() != ai->teamID) continue;
        int power = (int) ((s->power + s->getUnit()->GetMaxHealth()) / 10.f);
        if(!retreatHandler->isRetreating(s->getUnit())) availableMobileThreat += power;
        springai::AIFloat3 pos(s->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 15;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      availableMobileThreat /= 500.f;
      availableMobileThreat *= 1.5f;

      availableRaiderThreat /= 500.f;

      // paint allythreat for commanders and welders
      for(auto &[k,w] : ai->ecoManager->workers){
        if(w->getUnit()->GetHealth() <= 0 || w->getUnit()->GetTeam() != ai->teamID || !w->getUnit()->GetDef()->IsAbleToAttack())
          continue;
        int power = (int) ((w->power + w->getUnit()->GetMaxHealth()) / 14.f);
        springai::AIFloat3 pos(w->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = 10;
        allyThreatGraphics.paintCircle(x, y, rad, power);
      }

      //paint buildpower
      for(auto &[k,w] : ai->ecoManager->workers){
        if(w->getUnit()->GetHealth() <= 0 || w->getUnit()->GetTeam() != ai->teamID) continue;
        springai::AIFloat3 pos(w->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        bpGraphics.paintCircle(x, y, w->buildRange, std::round(w->bp));
      }

      threatGraphics.clear();
      aaThreatGraphics.clear();
      riotGraphics.clear();
      for(auto &[k,t] : targets){
        int effectivePower = (int) t->getDanger();
        springai::AIFloat3 position(t->position);
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        if(position != nullpos && t->ud != nullptr
            && effectivePower > 0
            && (!CONTAINSS(unitTypes->planes,t->ud->GetName()))
            && !t->unit->IsBeingBuilt()
            && !t->unit->IsParalyzed()
            && t->unit->GetRulesParamFloat("disarmed", 0.f) == 0){
          int r = std::round(t->threatRadius / 64.f);
          if(std::string(t->ud->GetName()) == "gunshipskirm"){
            r *= 5;
          }
          if(!t->isStatic){
            if(!t->ud->IsAbleToFly()){
              // Reduce threat gradually for units that haven't been seen recently.
              float timeMod = (((frame - t->lastSeen)) / 1800.f);
              timeMod *= timeMod;
              timeMod = std::max(0.f, (1.f - timeMod));
              effectivePower = (int) (effectivePower * timeMod);
            }
            // paint enemy threat for mobiles
            if(t->isAA || t->isFlexAA){
              aaThreatGraphics.paintCircle(x, y, std::round(r * 1.25f), effectivePower);
            } else {
              if(t->isRiot) riotGraphics.paintCircle(x, y, r + 2, 1);
              threatGraphics.paintCircle(x, y, r + 2, effectivePower);
            }
          } else if(!t->isAA){
            // for statics
            if(t->isRiot) riotGraphics.paintCircle(x, y, r + 1, 1);
            threatGraphics.paintCircle(x, y, r + 1, effectivePower);
          }
        }
      }
    } else {
      allyPorcGraphics.clear();
      for(auto &[k,p] : ai->ecoManager->dporcs){
        if(p->isDead()) continue;
        springai::AIFloat3 pos(p->getPos());
        int x = std::round(pos.x / 64.f);
        int y = std::round(pos.z / 64.f);
        int rad = p->radius;
        allyPorcGraphics.paintCircle(x, y, rad, p->getPower());
      }

      enemyPorcGraphics.clear();
      raiderValueGraphics.clear();
      scytheGraphics.clear();
      // Update porc threat and loot value for raiders.
      for(auto &[k,t] : targets){
        springai::AIFloat3 position(t->position);
        int x = std::round(position.x / 64.f);
        int y = std::round(position.z / 64.f);
        int r;
        if(!t->identified || t->ud == nullptr || !t->ud->IsAbleToFly()){
          scytheGraphics.paintCircle(x, y, 4, 1);
        }
        if(!t->identified || t->ud == nullptr || (!t->isWorker && !t->isStatic) || t->isCom) continue;
        float value = t->value / 10.f;
        if(t->isPorc){
          value /= 2.f;
          if(!t->unit->IsBeingBuilt()
              && !t->unit->IsParalyzed()
              && t->unit->GetRulesParamFloat("disarmed", 0.f) == 0){
            // paint trimmed porc threat radii for workers and raiders to reference
            r = std::round(t->threatRadius / 64.f);
            int effectivePower = (int) t->getDanger();
            enemyPorcGraphics.paintCircle(x, y, r - 1, effectivePower);
          }
        }
        r = 7;

        float rthreat = getThreat(position) * (1.f + getRiotThreat(position));
        if(rthreat > availableRaiderThreat) continue;
        float threatMod = rthreat / availableRaiderThreat;
        threatMod *= threatMod;
        value *= std::max(0.5f, 1.f - threatMod);
        raiderValueGraphics.paintCircle(x, y, r, std::round(value));
      }
    }
}

void MilitaryManager::updateTargets(){
    std::vector<Enemy*> outdated;
    for(auto &[k,t] : targets){
      springai::AIFloat3 tpos(t->unit->GetPos());
      if(tpos != nullpos){
        t->lastSeen = frame;
        setpos(t->position, tpos);
      } else if(frame - t->lastSeen > 1800 && !t->isStatic){
        // remove mobiles that haven't been seen for over 60 seconds.
        outdated.push_back(t);
      } else if(t->position != nullpos && losManager->isInLos(t->position)){
        // remove targets that aren't where we last saw them.
        outdated.push_back(t);

        // Unpaint enemy threat for aa
        if((t->isAA || t->isFlexAA) && t->isPainted){
          int effectivePower = (int) t->getDanger();
          springai::AIFloat3 position(t->position);
          int x = std::round(position.x / 64.f);
          int y = std::round(position.z / 64.f);
          int r = std::round((t->threatRadius) / 64.f);
          aaPorcGraphics.unpaintCircle(x, y, r + 1, effectivePower);
        }
      }
    }

    for(Enemy* t : outdated){
      bomberHandler->removeTarget(t);
      targets.erase(t->unitID);
      enemyPorcs.erase(t->unitID);
      delete t;
    }

    for(auto it=sniperSightings.begin(); it!=sniperSightings.end(); ){
      DefenseTarget* d = *it;
      if(frame - d->frameIssued > 900){
        delete d; it = sniperSightings.erase(it);
      } else ++it;
    }

    for(auto it=defenseTargets.begin(); it!=defenseTargets.end(); ){
      DefenseTarget* d = *it;
      if(frame - d->frameIssued > 300){
        delete d; it = defenseTargets.erase(it);
      } else ++it;
    }

    for(auto it=airDefenseTargets.begin(); it!=airDefenseTargets.end(); ){
      DefenseTarget* d = *it;
      if(frame - d->frameIssued > 900){
        delete d; it = airDefenseTargets.erase(it);
      } else ++it;
    }

    enemyPorcValue = 0.f;
    enemyLightPorcValue = 0.f;
    enemyHeavyPorcValue = 0.f;
    slasherSpam = 0;
    // add up the value of heavy porc that the enemy has
    // and also check for slasher spam
    enemyHasAntiNuke = false;
    enemyHasNuke = false;
    float enemyAirValue = 0.f;
    enemyHeavyValue = 0;
    enemyFighterValue = 0;
    for(auto &[k,t] : targets){
      if(t->identified){
        std::string defName(t->ud->GetName());
        if(defName == "turretheavylaser" || defName == "turretgauss" || defName == "turretriot"
            || defName == "turretemp" || defName == "turretimpulse" || defName == "spidercrabe"){
          enemyPorcValue += t->ud->GetCost(m);
          enemyHeavyPorcValue += t->ud->GetCost(m);
        } else if(defName == "turretmissile" || defName == "turretlaser"){
          enemyPorcValue += t->ud->GetCost(m);
          enemyLightPorcValue += t->ud->GetCost(m);
        } else if(defName == "turretantiheavy" || defName == "turretheavy" || defName == "staticshield" || defName == "shieldshield"){
          enemyPorcValue += 1.5f * t->ud->GetCost(m);
          enemyHeavyPorcValue += 1.5f * t->ud->GetCost(m);
        } else if(defName == "shieldfelon" || defName == "shieldassault" || defName == "amphassault"){
          enemyPorcValue += 2.f * t->ud->GetCost(m);
          enemyLightPorcValue += t->ud->GetCost(m);
          enemyHeavyPorcValue += t->ud->GetCost(m);
        } else if(defName == "vehsupport"){
          slasherSpam++;
        } else if(t->isRaider){
          slasherSpam--;
        } else if(defName == "staticantinuke"){
          enemyHasAntiNuke = true;
        } else if(defName == "staticnuke"){
          enemyHasNuke = true;
        } else if(defName == "strider" || defName == "tankassault" || defName == "tankheavyassault" || defName == "amphassault" || defName == "spidercrabe" || defName == "jumpsumo" || defName == "jumpassault"){
          enemyHeavyValue += t->ud->GetCost(m);
        } else if(t->ud->IsAbleToFly() && !t->isNanoSpam){
          if(defName == "dronelight" || defName == "droneheavyslow" || defName == "dronecarry"){
            enemyAirValue += 25.f;
          } else {
            enemyAirValue += t->ud->GetCost(m);
          }
        } else if(t->unit->GetRulesParamFloat("comm_level", 0.f) > 3.f){
          enemyHasTrollCom = true;
        }

        if(t->identified && t->ud->GetSpeed() > 0 && t->ud->GetMaxWeaponRange() > 0 && t->ud->GetBuildOptions().empty()){
          enemyFighterValue += t->ud->GetCost(m);
        }
      }
    }

    maxEnemyAirValue = std::max(enemyAirValue, maxEnemyAirValue);

    for(auto it=targetMarkers.begin(); it!=targetMarkers.end(); ){
      TargetMarker& tm = *it;
      if(frame - tm.frame > 150) it=targetMarkers.erase(it);
      else ++it;
    }
}

float MilitaryManager::getThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (threatGraphics.getValue(x, y))/500.f;
}

float MilitaryManager::getPorcThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (enemyPorcGraphics.getValue(x, y))/500.f;
}

float MilitaryManager::getRiotThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return riotGraphics.getValue(x, y);
}

float MilitaryManager::getScytheThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return scytheGraphics.getValue(x, y);
}

float MilitaryManager::getEffectiveThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return ((threatGraphics.getValue(x, y)) - (allyThreatGraphics.getValue(x, y) + allyPorcGraphics.getValue(x, y)))/500.f;
}

float MilitaryManager::getTacticalThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return ((threatGraphics.getValue(x, y)) - allyPorcGraphics.getValue(x, y))/500.f;
}

float MilitaryManager::getFriendlyThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return allyThreatGraphics.getValue(x, y)/500.f;
}

float MilitaryManager::getFriendlyRaiderThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return allyRaiderGraphics.getValue(x, y)/500.f;
}

float MilitaryManager::getFriendlyPorcThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return allyPorcGraphics.getValue(x, y);
}

float MilitaryManager::getTotalFriendlyThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (allyThreatGraphics.getValue(x, y) + allyPorcGraphics.getValue(x, y))/500.f;
}

float MilitaryManager::getAAThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (aaThreatGraphics.getValue(x, y) + aaPorcGraphics.getValue(x, y))/500.f;
}

float MilitaryManager::getEffectiveAAThreat(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return std::max(0.f, ((aaThreatGraphics.getValue(x, y) + aaPorcGraphics.getValue(x, y) + threatGraphics.getValue(x, y)) - (allyThreatGraphics.getValue(x, y) + allyPorcGraphics.getValue(x, y)))/500.f);
}

float MilitaryManager::getValue(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (valueGraphics.getValue(x, y));
}

float MilitaryManager::getRaiderValue(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (raiderValueGraphics.getValue(x, y));
}

float MilitaryManager::getBP(const springai::AIFloat3& position){
    int x = std::round(position.x / 64.f);
    int y = std::round(position.z / 64.f);
    return (bpGraphics.getValue(x, y));
}

bool MilitaryManager::isWater(const springai::AIFloat3& pos){
    return callback->GetMap()->GetElevationAt(pos.x, pos.z) < 10.f;
}

springai::AIFloat3 MilitaryManager::getTarget(springai::Unit* u, bool defend){
    springai::AIFloat3 origin(u->GetPos());
    springai::AIFloat3 target;
    float fthreat = getFriendlyThreat(origin);
    float cost = 65535;
    bool waterTargetsOnly = false;
    std::string defName(u->GetDef()->GetName());
    if(defName == "subraider" || defName == "shiptorpraider"){
      waterTargetsOnly = true; // torpedoes can only hit stuff in water.
    }

    // first check defense targets
    if(defend){
      // check for defense targets first
      for(DefenseTarget* d : defenseTargets){
        if(waterTargetsOnly && !isWater(d->position)) continue;
        if(getTacticalThreat(d->position) > availableMobileThreat) continue;
        float tmpcost = distance(origin, d->position) - (d->damage * (1.f + getEffectiveThreat(d->position)));

        if(tmpcost < cost){
          cost = tmpcost;
          setpos(target, d->position);
        }
      }
    }

    // then look for enemy mexes to kill, and see which is cheaper
    for(MetalSpot* ms : graphManager->getEnemySpots()){
      if(waterTargetsOnly && !isWater(ms->getPos())) continue;
      if(getThreat(ms->getPos()) > fthreat || !pathfinder->isAssaultReachable(u, ms->getPos(), fthreat)){
        continue;
      }
      float tmpcost = (distance(ms->getPos(), origin)/4.f) - 500.f;
      tmpcost += 500.f * getThreat(ms->getPos());

      if(tmpcost < cost){
        setpos(target, ms->getPos());
        cost = tmpcost;
      }
    }


    // then look for enemy units to kill, and see if any are better targets
    // then check for nearby enemies that aren't raiders.
    for(auto &[k,e] : targets){
      if(waterTargetsOnly && !isWater(e->position)) continue;
      bool allyTerritory = graphManager->isAllyTerritory(e->position);
      float ethreat = getEffectiveThreat(e->position);
      if((ethreat > fthreat && (!allyTerritory || getPorcThreat(e->position) > 0))
          || getTacticalThreat(e->position) > availableMobileThreat
          || (e->identified && e->ud->IsAbleToFly())
          || ((!e->identified || e->isRaider) && distance(origin, e->position) > 1250.f)){
        continue;
      }
      float tmpcost = distance(origin, e->position);
      if(!e->isAA){
        if(e->identified && !e->isImportant && !e->isRaider && allyTerritory){
          tmpcost /= 2;
          tmpcost -= e->getDanger() * (1.f + getThreat(e->position));
        } else if(e->isImportant && pathfinder->isAssaultReachable(u, e->position, fthreat)){
          tmpcost /= 4.f;
          tmpcost -= 750.f;
        } else {
          tmpcost -= e->getDanger();
        }
      }

      if(tmpcost < cost){
        cost = tmpcost;
        setpos(target, e->position);
      }
    }

    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }

    // if no targets are available attack enemyshadowed metal spots until we find one
    for(MetalSpot* ms : graphManager->getUnownedSpots()){
      if(ms->enemyShadowed){
        if(waterTargetsOnly && !isWater(ms->getPos())) continue;
        float tmpcost = distance(ms->getPos(), origin);
        tmpcost /= 1+getThreat(ms->getPos());
        tmpcost /= (frame-ms->getLastSeen())/600;

        if(tmpcost < cost){
          setpos(target, ms->getPos());
          cost = tmpcost;
        }
      }
    }
    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }
    return nullpos;
}

springai::AIFloat3 MilitaryManager::getArtyTarget(const springai::AIFloat3& origin, bool defend){
    springai::AIFloat3 target;
    float cost = 65535;

    // first check defense targets
    if(defend){
      // check for defense targets first
      for(DefenseTarget* d : defenseTargets){
        float tmpcost = distance(origin, d->position) - d->damage;

        if(tmpcost < cost){
          cost = tmpcost;
          setpos(target, d->position);
        }
      }
    }

    // then look for enemy mexes to kill, and see which is cheaper
    std::vector<MetalSpot*> spots = graphManager->getEnemySpots();
    for(MetalSpot* ms : spots){
      float tmpcost = (distance(ms->getPos(), origin)/4) - (500 * getThreat(ms->getPos()));

      if(tmpcost < cost){
        setpos(target, ms->getPos());
        cost = tmpcost;
      }
    }

    // then look for enemy units to kill, and see if any are better targets
    // then check for nearby enemies that aren't raiders.
    for(auto &[k,e] : targets){
      if((e->identified && e->ud->IsAbleToFly()) || (e->isRaider)){
        continue;
      }
      float tmpcost = distance(origin, e->position) - (500 * getThreat(e->position));

      if(e->isMajorCancer){
        tmpcost /= 4;
        tmpcost -= 1000;
      } else if(e->isMinorCancer){
        tmpcost /= 2;
        tmpcost -= 500;
      }

      if(graphManager->isAllyTerritory(e->position)){
        tmpcost /= 2;
        tmpcost -= 250;
      }

      if(tmpcost < cost){
        cost = tmpcost;
        setpos(target, e->position);
      }
    }

    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }

    // if no targets are available attack enemyshadowed metal spots until we find one
    spots = graphManager->getUnownedSpots();
    for(MetalSpot* ms : spots){
      if(ms->enemyShadowed){
        float tmpcost = distance(ms->getPos(), origin);
        tmpcost /= 1+getThreat(ms->getPos());
        tmpcost /= (frame-ms->getLastSeen())/600;

        if(tmpcost < cost){
          setpos(target, ms->getPos());
          cost = tmpcost;
        }
      }
    }
    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }
    return nullpos;
}

springai::AIFloat3 MilitaryManager::getAirTarget(springai::Unit* u, bool defend){
    springai::AIFloat3 origin(u->GetPos());
    springai::AIFloat3 target;
    float fthreat = getFriendlyThreat(origin);
    float cost = 65535;

    // first check defense targets
    if(defend){
      for(DefenseTarget* d : defenseTargets){
        if(getEffectiveAAThreat(d->position) > fthreat){
          continue;
        }
        float tmpcost = distance(origin, d->position) - d->damage;

        if(tmpcost < cost){
          cost = tmpcost;
          setpos(target, d->position);
        }
      }
    }

    // then look for enemy mexes to kill, and see which is cheaper
    for(MetalSpot* ms : graphManager->getEnemySpots()){
      if(getEffectiveAAThreat(ms->getPos()) > fthreat || !pathfinder->isAssaultReachable(u, ms->getPos(), fthreat)){
        continue;
      }
      float tmpcost = (distance(ms->getPos(), origin)/4.f) - 500.f;

      if(tmpcost < cost){
        setpos(target, ms->getPos());
        cost = tmpcost;
      }
    }

    // then look for enemy units to kill, and see if any are better targets
    for(auto &[k,e] : targets){
      bool allyTerritory = graphManager->isAllyTerritory(e->position);
      if(e->position != nullpos && e->identified && getEffectiveAAThreat(e->position) < fthreat){
        float tmpcost = distance(origin, e->position);
        if(!e->isAA){
          if(!e->isRaider && allyTerritory){
            tmpcost /= (1.f + std::max(0.f, getEffectiveThreat(e->position)));
          }
          tmpcost -= e->getDanger();
        }

        if(e->isMajorCancer){
          tmpcost /= 4;
        } else if(e->isMinorCancer){
          tmpcost /= 2;
        }

        tmpcost += 2000.f * getAAThreat(e->position);

        if(tmpcost < cost){
          cost = tmpcost;
          setpos(target, e->position);
        }
      }
    }

    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }

    // if no targets are available attack enemyshadowed metal spots until we find one
    for(MetalSpot* ms : graphManager->getUnownedSpots()){
      if(ms->enemyShadowed){
        float tmpcost = distance(ms->getPos(), origin);
        tmpcost /= 1.f + getThreat(ms->getPos());
        tmpcost /= (frame - ms->getLastSeen())/600;

        if(tmpcost < cost){
          setpos(target, ms->getPos());
          cost = tmpcost;
        }
      }
    }
    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }
    return nullpos;
}

springai::AIFloat3 MilitaryManager::getKrowTarget(const springai::AIFloat3& origin){
    springai::AIFloat3 target;
    float fthreat = getFriendlyThreat(origin);
    float cost = 65535;

    // then look for enemy mexes to kill, and see which is cheaper
    std::vector<MetalSpot*> spots = graphManager->getEnemySpots();
    for(MetalSpot* ms : spots){
      float tmpcost = distance(ms->getPos(), origin);
      tmpcost += 2000.f * getAAThreat(ms->getPos());
      if(tmpcost < cost && getEffectiveAAThreat(ms->getPos()) < fthreat){
        setpos(target, ms->getPos());
        cost = tmpcost;
      }
    }

    // then look for enemy units to kill, and see if any are better targets
    for(auto &[k,e] : targets){
      if(e->position != nullpos && e->identified && !e->ud->IsAbleToFly()){
        float tmpcost = distance(origin, e->position) - (!e->isAA ? 10.f * std::min(e->ud->GetCost(m), 1500.f) : e->ud->GetCost(m));
        tmpcost += 10000.f * getAAThreat(e->position);

        if(tmpcost < cost){
          cost = tmpcost;
          setpos(target, e->position);
        }
      }
    }

    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }

    // if no targets are available attack enemyshadowed metal spots until we find one
    spots = graphManager->getUnownedSpots();
    for(MetalSpot* ms : spots){
      if(ms->enemyShadowed){
        float tmpcost = distance(ms->getPos(), origin);
        tmpcost /= 1+getThreat(ms->getPos());
        tmpcost /= (frame-ms->getLastSeen())/600;

        if(tmpcost < cost){
          setpos(target, ms->getPos());
          cost = tmpcost;
        }
      }
    }
    if(target != nullpos){
      targetMarkers.push_back(TargetMarker(target, frame));
      return target;
    }
    return nullpos;
}

std::vector<Enemy*> MilitaryManager::getBomberTargets(){
    const springai::AIFloat3& ac = graphManager->getAllyCenter();
    auto cmp = [this,ac](Enemy* lhs, Enemy* rhs){
      float lhsthreat = (1.f + getAAThreat(lhs->position));
      float rhsthreat = (1.f + getAAThreat(rhs->position));
      float cost1 = distance(lhs->position, ac) * lhsthreat;
      cost1 -= lhs->value / lhsthreat;
      float cost2 = distance(rhs->position, ac) * rhsthreat;
      cost2 -= rhs->value / rhsthreat;
      return cost1 < cost2;
    };

    std::vector<Enemy*> result;
    for(auto &[k,t] : targets){
      if(!t->identified || t->ud == nullptr || (!t->isStatic && t->value < 700.f) || t->isNanoSpam) continue;
      result.push_back(t);
    }
    std::sort(result.begin(), result.end(), cmp);
    return result;
}

Enemy* MilitaryManager::getUltiTarget(const springai::AIFloat3& origin){
    // 74145: TODO: ultis are kinda defenseless now, be more choosy!
    Enemy* target = nullptr;
    float cost,tmpcost;
    for(auto &[k,e] : targets){
      if(!e->identified || e->ud == nullptr || e->ud->IsAbleToFly()) continue; // unknown/impossible
      if(e->ud->GetCost(m) < 600.f || e->ud->GetHealth() < 1000.f) continue; // not worth it
      if(!graphManager->isAllyTerritory(e->position) || getPorcThreat(e->position) > 0) continue; // too risky
      tmpcost = distance(origin, e->position) - e->getDanger();
      if(target==nullptr||tmpcost < cost){
        target=e; cost=tmpcost;
      }
    } return target;
}

springai::AIFloat3 MilitaryManager::getAATarget(Raider& r){
    springai::AIFloat3 origin(r.getPos());
    springai::AIFloat3 target(nullpos);
    float range = r.getUnit()->GetMaxRange();
    float cost = 65535;
    // first check defense targets
    // check for defense targets first
    for(DefenseTarget* d : airDefenseTargets){
      if(getTotalFriendlyThreat(d->position) == 0 || getPorcThreat(getDirectionalPoint(d->position, origin, range)) > 0){
        continue;
      }
      float tmpcost = distance(origin, d->position) - d->damage;
      if(tmpcost < cost){
        cost=tmpcost; setpos(target,d->position);
      }
    }

    // then look for enemy air units to kill, and see if any are better targets
    for(auto &[k,e] : targets){
      if(!e->identified
          || !e->ud->IsAbleToFly()
          || getEffectiveThreat(getDirectionalPoint(e->position, origin, range)) > 0
          || getPorcThreat(e->position) > 0){
        continue;
      }
      float tmpcost = distance(origin, e->position) - e->getDanger();
      if(tmpcost<cost){
        cost=tmpcost; setpos(target,e->position);
      }
    }
    if(target == nullpos){
      MetalSpot* spot = graphManager->getClosestFrontLineSpot(graphManager->getEnemyCenter());
      if(spot != nullptr){
        setpos(target, graphManager->getClosestAirHaven(spot->getPos()));
      }
    }
    if(target != nullpos){
      targetMarkers.push_back({target, frame});
      return target;
    }
    return nullpos;
}

springai::AIFloat3 MilitaryManager::getMerlinTarget(springai::Unit* u){
    // 74145: TODO: firing at large blobs of radar dots should be OK too!
    float maxRange = u->GetMaxRange();
    float cost = 65535;
    springai::AIFloat3 target(nullpos);
    for(auto &[k,e] : targets){
      if(!e->identified || e->ud == nullptr || e->ud->IsAbleToFly() || e->isRaider
          || !graphManager->isAllyTerritory(getDirectionalPoint(e->position, u->GetPos(), maxRange))) continue;
        float tmpcost = distance(e->position, u->GetPos());
        tmpcost += 250.f * e->ud->GetSpeed();
        tmpcost -= e->ud->GetCost(m) * getScytheThreat(e->position);
        if(tmpcost<cost){
          cost=tmpcost; setpos(target,e->position);
        }
    }
    return target;
}

springai::AIFloat3 MilitaryManager::getBerthaTarget(const springai::AIFloat3& origin){
    springai::AIFloat3 target(nullpos);
    float cost=0,maxrange=callback->GetUnitDefByName("staticheavyarty")->GetMaxWeaponRange();
    // for berthas the more expensive the target the better
    for(auto &[k,e] : targets){
      if(!e->identified || e->position == nullpos || e->isNanoSpam) continue;
      // berthas can't target things outside their range, and are not good vs air.
      if(distance(e->position, origin) > maxrange || e->ud->IsAbleToFly()) continue;
      std::string defName(e->ud->GetName());
      if((defName == "staticantinuke" && !hasNuke) || (e->ud->GetSpeed() > 0 && e->ud->GetBuildSpeed() > 8.f)){
        // we also ignore antinuke unless we actually have nukes.
        continue;
      }
      float tmpcost = (e->ud->GetCost(m) * getScytheThreat(e->position)) - ((frame - e->lastSeen)/2.f);
      if(defName.find("factory")!=std::string::npos || defName.find("hub")!=std::string::npos || defName.find("felon")!=std::string::npos){
        tmpcost+=800;
        if(defName.find("hub")!=std::string::npos) tmpcost*=2;
      }
      if(target==nullpos || tmpcost>cost){
        cost=tmpcost; setpos(target,e->position);
      }
    }
    if(target != nullpos){
      targetMarkers.push_back({target, frame});
      return target;
    } return nullpos;
}

springai::AIFloat3 MilitaryManager::getSuperWepTarget(springai::Unit* u, bool doCook){
    if(frame - lastValueRedrawFrame > 350){
      paintValueMap(u->GetDef()->GetName());
      lastValueRedrawFrame = frame;
    }
    springai::AIFloat3 origin(u->GetPos());
    float range = u->GetMaxRange();
    springai::AIFloat3 target(nullpos);
    float cost = -65535;

    for(auto &[k,e] : targets){
      if(!e->identified || e->position == nullpos
          || distance(e->position, origin) > range || e->ud->IsAbleToFly()
          || graphManager->isAllyTerritory(e->position)){
        // superweapons should avoid teamkilling.
        continue;
      }
      float tmpcost = getValue(e->position);
      if(tmpcost>cost){
        cost=tmpcost; setpos(target,e->position);
      }
    }
    if(target != nullpos){
      targetMarkers.push_back({target, frame});
      if(doCook){ cook(); }
      return target;
    }
    return nullpos;
}

void MilitaryManager::cook(){
    double rnd = kgb_rand();
    if(rnd > 0.8)      ai->say("Nice knowing you.");
    else if(rnd > 0.6) ai->say("It's cooking time!");
    else if(rnd > 0.4) ai->say("SHINY!");
    else if(rnd > 0.2) ai->say("R.I.P.");
    else               ai->say("Get rekt.");
}

Enemy* MilitaryManager::getClosestEnemyPorc(const springai::AIFloat3& position){
    Enemy* best = nullptr;
    float dist;
    for(auto &[k,e] : enemyPorcs){
      if(!e->identified || e->ud==nullptr || !e->isStatic || e->getDanger()==0 || e->isAA || e->isNanoSpam || e->position==nullpos){
        continue;
      }
      float tmpdist = distance(position, e->position) + e->ud->GetMaxWeaponRange();
      if(best==nullptr || tmpdist<dist){
        best=e; dist=tmpdist;
      }
    } return best;
}

springai::AIFloat3 MilitaryManager::getRallyPoint(const springai::AIFloat3& start){
    springai::AIFloat3 pos(start),target(nullpos);
    if(pos == nullpos){
      setpos(pos, graphManager->getAllyCenter());
      if(pos == nullpos){
        std::vector<springai::Unit*> units = callback->GetFriendlyUnits();
        if(units.empty()) return pos;
        setpos(pos,units.front()->GetPos());
      }
    }
    float cost,fthreat=getFriendlyThreat(pos);
    bool found=false;
    // check for defense targets first
    for(DefenseTarget* d : defenseTargets){
      if(getPorcThreat(d->position)>0 || getTacticalThreat(d->position)>availableMobileThreat || !graphManager->isAllyTerritory(d->position)){
        continue;
      }
      float tmpcost = distance(pos, d->position) - d->damage;
      if(!found || tmpcost<cost){
        found=true; cost=tmpcost; setpos(target,d->position);
      }
    }

    // then check for nearby enemies
    for(auto &[k,e] : targets){
      bool allyTerritory = graphManager->isAllyTerritory(e->position);
      float ethreat = getEffectiveThreat(e->position);
      float pthreat = getPorcThreat(e->position);
      if((ethreat > fthreat && (!allyTerritory || pthreat > 0))
          || getTacticalThreat(e->position) > availableMobileThreat
          || (e->identified && e->ud->IsAbleToFly())
          || ((!e->identified || e->isRaider) && distance(pos, e->position) > 800.f)
          || (!allyTerritory || pthreat > 0)){
        continue;
      }
      float tmpcost = distance(pos, e->position);
      if(!e->isAA){
        if(e->identified && !e->isRaider){
          tmpcost /= 2;
          tmpcost -= e->getDanger() * (1.f + getThreat(e->position));
        } else {
          tmpcost -= e->getDanger();
        }
      }
      if(!found || tmpcost<cost){
        found=true; cost=tmpcost; setpos(target,e->position);
      }
    }
    if(found) return target;

    //if there aren't any, then rally near the front line.
    springai::AIFloat3 position(getWorkerEscort(pos));
    if(position != nullpos) return position;
    MetalSpot* spot = graphManager->getClosestFrontLineSpot(pos);
    if(spot!=nullptr && spot->getPos()!=nullpos) return spot->getPos();

    setpos(position, graphManager->getClosestLeadingLink(pos));
    if(position!=nullpos) return position;

    //otherwise rally near ally center.
    setpos(position, graphManager->getClosestHaven(graphManager->getAllyCenter()));

    if(position != nullpos) return position;
    setpos(position, graphManager->getAllyCenter());
    if(position != nullpos) return position;

    ai->say("getRallyPoint is fukt.");
    spot = graphManager->getClosestSpot(pos);
    if(spot!=nullptr && spot->getPos()!=nullpos) return spot->getPos();
    return pos;
}

springai::AIFloat3 MilitaryManager::getAirRallyPoint(const springai::AIFloat3& start){
    springai::AIFloat3 pos(start),target(nullpos);
    if(pos == nullpos){
      setpos(pos, graphManager->getAllyCenter());
      if(pos == nullpos){
        std::vector<springai::Unit*> units = callback->GetFriendlyUnits();
        if(units.empty()) return pos;
        setpos(pos, units.front()->GetPos());
      }
    }
    float cost,tmpcost;
    bool found=false;
    // check for defense targets first
    for(DefenseTarget* d : defenseTargets){
      if(getEffectiveAAThreat(d->position) > getFriendlyThreat(pos) || getPorcThreat(d->position) > 0){
        continue;
      }
      tmpcost = distance(pos, d->position) - d->damage;
      tmpcost += 2000.f * getAAThreat(d->position);
      if(!found || tmpcost<cost){
        found=true; cost=tmpcost; setpos(target,d->position);
      }
    }
    // then check for nearby enemies
    for(auto &[k,e] : targets){
      if(getEffectiveAAThreat(e->position) > getFriendlyThreat(pos)
          || !graphManager->isAllyTerritory(e->position)
          || getPorcThreat(e->position) > 0){
        continue;
      }
      tmpcost = distance(pos, e->position);
      if(!e->isAA) tmpcost -= e->getDanger() * (1.f + std::max(0.f, getEffectiveThreat(e->position)));
      tmpcost += 2000.f * getAAThreat(e->position);
      if(e->isMajorCancer){
        tmpcost /= 4;
        tmpcost -= 1000;
      } else if(e->isMinorCancer){
        tmpcost /= 2;
        tmpcost -= 500;
      }
      if(!found || tmpcost<cost){
        found=true; cost=tmpcost; setpos(target,e->position);
      }
    }
    if(found) return target;

    //if there aren't any, then rally near the front line.
    springai::AIFloat3 position(getWorkerEscort(pos));
    if(position != nullpos) return position;

    MetalSpot* spot = graphManager->getClosestFrontLineSpot(pos);
    if(spot!=nullptr && spot->getPos()!=nullpos){
      return graphManager->getClosestAirHaven(spot->getPos());
    }

    setpos(position, graphManager->getClosestAirLeadingLink(pos));
    if(position != nullpos) return position;

    setpos(position, graphManager->getAllyCenter());
    if(position != nullpos) return position;
    spot = graphManager->getClosestSpot(pos);
    if(spot!=nullptr && spot->getPos()!=nullpos) return spot->getPos();
    return pos;
}

springai::AIFloat3 MilitaryManager::getRaiderRally(const springai::AIFloat3& start){
    springai::AIFloat3 pos(start),target(nullpos);
    if(pos == nullpos){
      setpos(pos, graphManager->getAllyCenter());
      if(pos == nullpos){
        std::vector<springai::Unit*> units = callback->GetFriendlyUnits();
        if(units.empty()) return pos;
        setpos(pos,units.front()->GetPos());
      }
    }
    float cost,tmpcost;
    bool found=false;
    // check for defense targets first
    for(DefenseTarget* d : defenseTargets){
      if((getEffectiveThreat(d->position) > getFriendlyThreat(pos)
          || getPorcThreat(d->position) > 0
          || getRiotThreat(d->position) > 0)
            && !squadHandler->fighters.empty()
            || !graphManager->isAllyTerritory(d->position))
        { continue; }
      tmpcost = distance(pos, d->position) - d->damage;
      if(!found || tmpcost<cost)
      { found=true; cost=tmpcost; setpos(target,d->position); }
    }

    // then check for nearby enemies
    for(auto &[k,e] : targets){
      float ethreat = (getThreat(e->position) * (1.f + getRiotThreat(e->position))) - getTotalFriendlyThreat(e->position);
      if(((ethreat > getFriendlyThreat(pos)
          || (e->identified && e->ud->IsAbleToFly())) && !squadHandler->fighters.empty())
          || (!graphManager->isAllyTerritory(e->position) || getPorcThreat(e->position) > 0)){
        continue;
      }
      tmpcost = distance(pos, e->position) - e->getDanger();
      if(e->isMajorCancer){
        tmpcost /= 4; tmpcost -= 1000;
      } else if(e->isMinorCancer || (e->isAA && std::string(e->ud->GetName()) != "turretaalaser")){
        tmpcost /= 2; tmpcost -= 500;
      }
      if(!found || tmpcost<cost){
        found=true; cost=tmpcost; setpos(target,e->position);
      }
    }

    if(found) return target;

    springai::AIFloat3 position(getWorkerEscort(pos));
    if(position != nullpos) return position;
    MetalSpot* spot = graphManager->getClosestFrontLineSpot(pos);
    if(spot!=nullptr && spot->getPos()!=nullpos) return spot->getPos();

    //if there aren't any, then rally near the front line.
    setpos(position,graphManager->getClosestLeadingLink(pos));
    if(position != nullpos) return position;

    //otherwise rally near ally center.
    setpos(position, graphManager->getClosestHaven(graphManager->getAllyCenter()));
    if(position != nullpos) return position;

    setpos(position, graphManager->getAllyCenter());
    if(position != nullpos) return position;
    spot = graphManager->getClosestSpot(pos);
    if(spot!=nullptr && spot->getPos()!=nullpos) return spot->getPos();
    return pos;
}

springai::AIFloat3 MilitaryManager::getWorkerEscort(const springai::AIFloat3& pos){
    springai::AIFloat3 target(nullpos);
    float mindist; bool found=false;
    Worker* nearestFac = ai->ecoManager->getNearestFac(pos);
    springai::AIFloat3 facpos((nearestFac != nullptr) ? nearestFac->getPos() : pos);
    springai::AIFloat3 enemyCenter(graphManager->getEnemyCenter());
    if(enemyCenter == nullpos) setpos(enemyCenter, pos);
    for(auto &[k,w] : ai->ecoManager->workers){
      if(getEffectiveThreat(w->getPos()) > availableMobileThreat || (w->isCom && ai->ecoManager->workers.size() > 1)) continue;
      float dist = distance(w->getPos(), enemyCenter) + (1000.f * (getTotalFriendlyThreat(w->getPos()) - getEffectiveThreat(w->getPos())));
      if(!found || dist<mindist){
        found=true; setpos(target,w->getPos()); mindist=dist;
      }
    }
    if(target==nullpos) return graphManager->getAllyCenter();
    return callback->GetMap()->FindClosestBuildSite(callback->GetUnitDefByName("mahlazer"), getAngularPoint(target, graphManager->getMapCenter(), 250.f), 600.f, 3, 0);
}

std::vector<Enemy*> MilitaryManager::getTargets(){
    std::vector<Enemy*> r; r.reserve(targets.size());
    for(auto &[k,v] : targets){ r.push_back(v); }
    return r;
}

static char ASCIIValue(int val, int max){
    static char vals[11] = {
      '.', '0','1','2','3','4','5','6','7','8','9'
    };
    float lv = (val==0) ? 0 : std::log((float)val);
    float lm = std::log((float)max);
    return vals[(int)(11 * lv / lm)];
}

void MilitaryManager::paintASCIIThreatMap(){
    std::ofstream myfile; myfile.open ("zkgbai_threatmap.txt");
    for(int y = 0; y < threatGraphics.height; y++){
      for(int x = 0; x < threatGraphics.width; x++){
        myfile << ASCIIValue(threatGraphics.getValue(x, y), 32767);
      } myfile << std::endl;
    } myfile.close();
}

void MilitaryManager::paintASCIIPorcMap(){
    std::ofstream myfile; myfile.open("zkgbai_porcmap.txt");
    for(int y = 0; y < enemyPorcGraphics.height; y++){
      for(int x = 0; x < enemyPorcGraphics.width; x++){
        myfile << ASCIIValue(enemyPorcGraphics.getValue(x, y), 32767);
      } myfile << std::endl;
    } myfile.close();
}

void MilitaryManager::paintASCIIRiotMap(){
    std::ofstream myfile; myfile.open("zkgbai_riotmap.txt");
    for(int y = 0; y < riotGraphics.height; y++){
      for(int x = 0; x < riotGraphics.width; x++){
        myfile << ASCIIValue(riotGraphics.getValue(x, y), 127);
      } myfile << std::endl;
    } myfile.close();
}

}
