#ifndef _ZKGBAI_UNITCLASSES_H
#define _ZKGBAI_UNITCLASSES_H

#include <string>
#include <set>
#include <vector>

namespace zkgbai {

class UnitClasses {
public:
    std::vector<std::string> smallRaiders;
    std::vector<std::string> mediumRaiders;
    std::vector<std::string> soloRaiders;
    std::vector<std::string> assaults;
    std::vector<std::string> arties;
    std::vector<std::string> striders;
    std::vector<std::string> airMobs;
    std::vector<std::string> shieldMobs;
    std::vector<std::string> mobSupports;
    std::vector<std::string> loners;
    std::vector<std::string> AAs;
    std::vector<std::string> planes;
    std::vector<std::string> bombers;
    std::vector<std::string> sappers;
    std::vector<std::string> heavies;

    std::vector<std::string> noRetreat;
    
    std::set<int> porcWeps;

    static UnitClasses* getInstance(){
      if(instance == nullptr){
        instance = new UnitClasses();
      }
      return instance;
    }

    static void add_user(){
      users++;
    }

    static void remove_user(){
      users--;
      if(users == 0){
        delete instance;
        instance = nullptr;
      }
    }

private:
    inline static int users = 0;
    inline static UnitClasses* instance = nullptr;
    UnitClasses();
};
}
#endif //_ZKGBAI_UNITCLASSES_H
