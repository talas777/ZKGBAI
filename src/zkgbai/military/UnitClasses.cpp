#include "UnitClasses.h"

namespace zkgbai {

UnitClasses::UnitClasses(){
    // raiders
    smallRaiders.push_back("cloakraid");
    smallRaiders.push_back("shieldraid");
    smallRaiders.push_back("amphraid");
    smallRaiders.push_back("amphbomb");
    smallRaiders.push_back("hoverraid");
    smallRaiders.push_back("shipscout");
    smallRaiders.push_back("shiptorpraider");

    mediumRaiders.push_back("vehraid");
    mediumRaiders.push_back("gunshipraid");
    mediumRaiders.push_back("cloakheavyraid");
    mediumRaiders.push_back("tankheavyraid");
    mediumRaiders.push_back("subraider");

    soloRaiders.push_back("spiderscout");
    soloRaiders.push_back("vehscout");
    soloRaiders.push_back("shieldscout");
    soloRaiders.push_back("gunshipbomb");

    // assaults; stuff that attacks in mobs
    assaults.push_back("cloakassault");
    assaults.push_back("cloakriot");
    assaults.push_back("cloakskirm");
    assaults.push_back("amphfloater");
    assaults.push_back("amphriot");
    assaults.push_back("amphimpulse");
    assaults.push_back("vehriot");
    assaults.push_back("vehassault");
    assaults.push_back("hoverskirm");
    assaults.push_back("hoverriot");
    assaults.push_back("hoverassault");
    assaults.push_back("hoverdepthcharge");
    assaults.push_back("spiderassault");
    assaults.push_back("spiderskirm");
    assaults.push_back("spideremp");
    assaults.push_back("spiderriot");
    assaults.push_back("tankriot");
    assaults.push_back("tankassault");
    assaults.push_back("shieldskirm");
    assaults.push_back("gunshipskirm");
    assaults.push_back("gunshipheavyskirm");
    assaults.push_back("gunshipassault");
    assaults.push_back("shipriot");
    assaults.push_back("shipskirm");
    assaults.push_back("shipassault");

    // Shield mobs
    shieldMobs.push_back("shieldassault");
    shieldMobs.push_back("shieldriot");
    shieldMobs.push_back("shieldarty");
    shieldMobs.push_back("shieldfelon");
    shieldMobs.push_back("vehcapture");
    shieldMobs.push_back("shieldshield");

    // mobSupport: things that increase the strength of mobs
    mobSupports.push_back("cloakjammer");

    // striders; stuff that can dgun
    striders.push_back("striderdante");
    striders.push_back("striderscorpion");
    striders.push_back("striderbantha");
    striders.push_back("striderdetriment");

    // loners; strider-like stuff that does better on its own than in mobs
    loners.push_back("vehsupport");
    loners.push_back("tankheavyassault");
    loners.push_back("spidercrabe");
    loners.push_back("amphassault");

    // arties; stuff that has long range and can attack high threat areas without suiciding
    arties.push_back("cloaksnipe");
    arties.push_back("cloakarty");
    arties.push_back("veharty");
    arties.push_back("vehheavyarty");
    arties.push_back("hoverarty");
    arties.push_back("tankarty");
    arties.push_back("tankheavyarty");
    arties.push_back("striderarty");
    arties.push_back("shiparty");
    arties.push_back("shipheavyarty");

    // Anti-air units
    AAs.push_back("cloakaa");
    AAs.push_back("gunshipaa");
    AAs.push_back("amphaa");
    AAs.push_back("hoveraa");
    AAs.push_back("jumpaa");
    //AAs.push_back("planeheavyfighter");
    //AAs.push_back("planefighter");
    AAs.push_back("shieldaa");
    AAs.push_back("shipaa");
    AAs.push_back("spideraa");
    AAs.push_back("tankaa");
    AAs.push_back("vehaa");

    // Planes
    planes.push_back("planefighter");
    planes.push_back("bomberdive");
    planes.push_back("bomberdisarm");
    planes.push_back("planeheavyfighter");
    planes.push_back("bomberprec");
    planes.push_back("bomberriot");
    planes.push_back("bomberheavy");
    planes.push_back("planescout");

    // Bombers
    bombers.push_back("bomberdive");
    bombers.push_back("bomberprec");
    bombers.push_back("bomberheavy");
    bombers.push_back("bomberriot");
    bombers.push_back("bomberdisarm");

    // Sappers
    sappers.push_back("cloakbomb");
    sappers.push_back("shieldbomb");
    sappers.push_back("jumpbomb");

    // Things that should not retreat
    noRetreat.push_back("spidercrabe");
    noRetreat.push_back("shieldshield");
    noRetreat.push_back("shieldfelon");
    noRetreat.push_back("bomberprec");
    noRetreat.push_back("gunshipbomb");
    noRetreat.push_back("planefighter");

    // List of things which can count higher towards fighter value.
    heavies.push_back("tankassault");
    heavies.push_back("tankheavyassault");
    heavies.push_back("spidercrabe");
    heavies.push_back("amphassault");
    heavies.push_back("shipassault");

    // porc weps are set in root.
}

}
