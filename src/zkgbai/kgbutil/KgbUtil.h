#ifndef _ZKGBAI_KGBUTIL_H
#define _ZKGBAI_KGBUTIL_H

// generated by the C++ Wrapper scripts
#include "Unit.h"
#include "AIFloat3.h"

#define CONTAINSP(a,b) (a.end()!=std::find_if(a.begin(),a.end(),[b](auto*c){return *b==*c;}))
#define CONTAINSS(a,b) (a.end()!=std::find(a.begin(),a.end(), std::string(b)))
#define CONTAINSI(a,b) (a.end()!=std::find(a.begin(),a.end(), b))
#define ERASEONE(c,x){auto mTMPit=std::find(c.begin(),c.end(),x);if(mTMPit!=c.end())(void)c.erase(mTMPit);}

namespace springai {

static bool operator<(const AIFloat3& lhs, const AIFloat3& rhs){
    return std::tie(lhs.x, lhs.z, lhs.y) < std::tie(rhs.x, rhs.z, rhs.y);
}

}

namespace zkgbai {

static void setpos(springai::AIFloat3& to, const springai::AIFloat3& from){
    to.x = from.x; to.y = from.y; to.z = from.z;
}

static double kgb_rand(){
    return static_cast<double>(rand()) / RAND_MAX;
}

const static springai::AIFloat3 nullpos(0,0,0);

static springai::AIFloat3 getRadialPoint(const springai::AIFloat3& position, float radius){
    // returns a random point lying on a circle around the given position.
    springai::AIFloat3 pos;
    double angle = kgb_rand() * 2 * M_PI;
    double vx = std::cos(angle);
    double vz = std::sin(angle);
    pos.x = static_cast<float>(position.x + radius*vx);
    pos.z = static_cast<float>(position.z + radius*vz);
    return pos;
}

static springai::AIFloat3 getDirectionalPoint(const springai::AIFloat3& start, const springai::AIFloat3& dest, float distance){
    springai::AIFloat3 dir;

    // First derive a normalized direction vector.
    float x = dest.x - start.x;
    float z = dest.z - start.z;
    float d = static_cast<float>(sqrt(x*x + z*z));
    x /= d;
    z /= d;

    // Then apply it relative to the start position with the given distance.
    dir.x = start.x + (x * distance);
    dir.z = start.z + (z * distance);
    return dir;
}

static springai::AIFloat3 getAngularPoint(const springai::AIFloat3& start, const springai::AIFloat3& dest, float radius){
    springai::AIFloat3 radir;
    float x = dest.x - start.x;
    float z = dest.z - start.z;
    double angle = std::atan2(z, x);
    if(kgb_rand() > 0.5){
      angle -= kgb_rand() * 0.25 * M_PI;
    } else {
      angle += kgb_rand() * 0.25 * M_PI;
    }

    double vx = std::cos(angle);
    double vz = std::sin(angle);
    radir.x = static_cast<float>(start.x + radius*vx);
    radir.z = static_cast<float>(start.z + radius*vz);
    return radir;
}

static springai::AIFloat3 getFormationPoint(const springai::AIFloat3& start, const springai::AIFloat3& compare, const springai::AIFloat3& dest){
    float x = start.x - compare.x;
    float z = start.z - compare.z;
    return springai::AIFloat3(dest.x + x, dest. y, dest.z + z);
}

static float getSpeed(springai::Unit* u){
    springai::AIFloat3 vel(u->GetVel());
    return static_cast<float>((sqrt((vel.x * vel.x) + (vel.z * vel.z)) * 30.0));
}

static float lerp(float start, float end, float scale){
    return (end * scale) + (start * (1.0f - scale));
}

static float distance(const springai::AIFloat3& v0, const springai::AIFloat3& v1){
    float dx = v0.x - v1.x;
    float dz = v0.z - v1.z;
    return static_cast<float>(sqrt(dx*dx+dz*dz));
}

}
#endif //_ZKGBAI_KGBUTIL_H
