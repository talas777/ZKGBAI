#ifndef _ZKGBAI_BYTEARRAYGRAPHICS_H
#define _ZKGBAI_BYTEARRAYGRAPHICS_H

namespace zkgbai {
class ByteArrayGraphics {
private:
    std::vector<char> data;

public:
    const int width;
    const int height;

ByteArrayGraphics(int w, int h) : width(w), height(h){
    data.resize(w*h);
    std::fill(data.begin(), data.end(), 0);
}

void clear(){
  std::fill(data.begin(), data.end(), 0);
}

void paintCircle(int cx, int cy, int radius, int intensity){
    if(intensity<=0) return;
    int beginX = std::max(cx - radius + 1, 0);
    int endX =   std::min(cx + radius, width);

    int beginY = std::max(cy - radius + 1, 0);
    int endY =   std::min(cy + radius, height);

    int radsq = radius * radius;

    for(int y = beginY; y < endY; y++){
      int dY = (cy - y);
      int dySq = dY*dY;
      int ypos = (y * width);

      for(int x = beginX; x < endX; x++){
        int dX = (cx - x);
        int dxSq = dX*dX;

        int sum = dxSq + dySq;

        if(sum <= radsq){
          int index = ypos + x;
          if(index < 0 || index >= (int)data.size()) continue;
          data[index] += intensity;
          if(data[index] < 0){
            data[index] = 127;
          }
        }
      }
    }
}

void unpaintCircle(int cx, int cy, int radius, int intensity){
    if(intensity<=0) return;
    int beginX = std::max(cx - radius + 1, 0);
    int endX =   std::min(cx + radius, width);

    int beginY = std::max(cy - radius + 1, 0);
    int endY =   std::min(cy + radius, height);

    int radsq = radius * radius;

    for(int y = beginY; y < endY; y++){
      int dY = (cy - y);
      int dySq = dY*dY;
      int ypos = (y * width);

      for(int x = beginX; x < endX; x++){
        int dX = (cx - x);
        int dxSq = dX*dX;

        int sum = dxSq + dySq;

        if(sum <= radsq){
          int index = ypos + x;
          if(index < 0 || index >= (int)data.size()) continue;
          data[index] = static_cast<char>(std::max(data[index] - intensity, 0));
        }
      }
    }
}

float getValue(int x, int y) const {
    if(x<0 || y<0) return 0;
    int idx = x + y*width;
    if(idx < 0 || idx >= (int)data.size() ) return 0;
    return data[idx];
}

};
}
#endif //_ZKGBAI_BYTEARRAYGRAPHICS_H
