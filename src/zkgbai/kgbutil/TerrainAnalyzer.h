#ifndef _ZKGBAI_TERRAINANALYSER_H
#define _ZKGBAI_TERRAINANALYSER_H

#include <set>

// generated by the C++ Wrapper scripts
#include "AIFloat3.h"
#include "OOAICallback.h"
#include "UnitDef.h"

namespace zkgbai {

class ZKGBAI;
class GraphManager;
class Pathfinder;
class PathResult;

class TerrainAnalyzer {
public:
    TerrainAnalyzer(ZKGBAI* ai);
    std::set<std::string> getInitialFacList();
    bool areSpidersViable();
    bool areBotsViable();
    bool areShipsViable();

private:
    ZKGBAI* ai;
    springai::OOAICallback* callback;
    GraphManager* graphManager;
    std::set<std::string> initialFacList;
    Pathfinder* path = nullptr;

    springai::UnitDef* vehPath = nullptr;
    springai::UnitDef* botPath = nullptr;
    springai::UnitDef* spiderPath = nullptr;
    springai::UnitDef* hoverPath = nullptr;
    springai::UnitDef* amphPath = nullptr;
    springai::UnitDef* boatPath = nullptr;
    bool spidersViable = false;
    bool botsViable = false;
    bool shipsViable = false;
    const std::string taMsg = "Terrain Analysis: ";

    void populateFacList();
    PathResult checkPathing(springai::UnitDef* pathType, float maxRelCost);
    void log(std::string s);
};
}
#endif //_ZKGBAI_TERRAINANALYSER_H
