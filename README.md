zkgbai
======

This is the "Zero-K Graph-Based AI", built upon the SpringRTS AI interface to play the Zero-K RTS. 

It uses a static graph derived from resource points on the game map as a major part in its decisions.

Compiling
=========

`$ git clone spring`  
`$ cd spring`  
`$ cd AI/Skirmish`  
`$ git clone ZKGBAI`  
`$ cd ../..` # back to spring folder  
`$ cmake .`  
`$ make ZKGBAI`  

Installing
==========
To install the AI, copy the files from the `AI/Skirmish/ZKGBAI/data/` directory to `$SpringData/AI/Skirmish/ZKGBAI/stable`.

An example location on linux would be `/home/user/.spring/AI/Skirmish/ZKGBAI/stable`  
With steam version, that could be `~/.steam/steam/steamapps/common/Zero-K/AI/Skirmish/ZKGBAI/stable`

You will need to enable developer options in ZK lobby to choose the AI.
